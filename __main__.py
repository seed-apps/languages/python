#!/usr/bin/python3

import sys,os
import xmlscript
DEBUG=False
#DEBUG=True

#xmlscript.import_all(os.environ["LOCAL_MAIN"],DEBUG=DEBUG)
xmlscript.import_all(os.environ["SEED_DIR"],DEBUG=DEBUG)
        
path=sys.argv[1]

if os.path.isdir(path)==True:
    path=os.path.realpath(path)
    filename=os.path.join(path,"__main__.xml")
    
elif os.path.isfile(path)==True:
    path=os.path.realpath(path)
    filename=path
    path="/"+os.path.join(*path.split("/")[:-1])
else:
    raise Exception("no file",path)
#print(path)
#print(filename)
#print(sys.argv)
args=list()
kwargs=dict()
if len(sys.argv)>2:
    for arg in sys.argv[2:]:

        if "=" in arg:
            k,v=arg.split("=")
            kwargs[k]=v
        else:
            args.append(arg)

node=xmlscript.tree_from_file(filename,file_path=path,**kwargs)
#node.show()


#setup
#-------------------------------

if "-setup" in args:
    r=node.query("setup")

elif "-query" in args:
    r=node.query(action=args[-1])

elif hasattr(node,"run"):
    r=node.query("run")
else:
    r=node.query("setup")


r.parent=node

#output
#-------------------------------

if "-xml" in args:

    sys.stdout.write(xmlscript.tree_to_string(node)+"\n")#.decode())
    sys.stdout.flush()
if "-msg" in args:

    sys.stdout.write(xmlscript.tree_to_string(r))#.decode())
    sys.stdout.flush()

#Debug
if "-debug" in args:
    #r.tree()
    r.show()

if "-tree" in args:
    node.tree(limit=4)


if "-show" in args:
    node.show()



