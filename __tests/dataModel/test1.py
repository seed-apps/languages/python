from dataModel import *

class A(Model):

    @node(Model)
    def test(self):
        "test"
        return



class B(Model):

    @node(A)
    def test_node(self):
        """
        retourne une instance de A
        """
        print("ok")
        return dict(name="aa",a=1)


    @frame(Model=A)
    def test_frame(self):
        """
        retourne une frame Frame avec 10 instances de A
        """
        print("ok")
        for i in range(10):
            yield dict(name="aa",a=i)


    @frame(Model=A)
    def test_merge(self,i,j):
        """
        retourne une frame Frame avec 10 instances de A
        """

        return self.test_iter(i,j)

    def test_iter(self,i,j):
        #i:level
        #j:size
        
        if i>0:
            for k in range(j):
                yield dict(name=str(i)+"-"+str(k))
                yield self.test_iter(i-1,j)




def print_line(string,level=0):
    s="    "
    print( s*level+string )

def print_doc(cls):

    help=cls.help(cls)
    #print(help)
    for clsname,data in help.items():
        print_line(clsname)
        #print(data["doc"])
        del data["doc"]
        for name,objs in data.items():
            print_line(name,level=1)
            for k,v in objs.items():
                print_line(k,level=2)

def print_frame(f):

    for elt in f:
        print(elt.__class__.__name__,elt["name"])
    print(len(f))

print_doc(B)

print(B().test_node())
print_frame(B().test_frame())

import time
print()
for i in range(7):
    for j in range(8):
        start=time.time()
        f=B().test_merge(i,j)
        stop=time.time()
        print(i,j,len(f),stop-start)





