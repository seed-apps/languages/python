from dataModel.models import *

def test():

    class MetaTest(metaclass=ModelMeta):

        A=Field()
        B=Field(default="gkqqsdfgjkldfg")

        F1=Function()
        F2=Function()
        F3=Function()

        def f1(self):
            return "ok"

        def f2(self):
            return "ko"



    class MetaTest2(Model,MetaTest):

        C=Field()
        D=Field()

        F21=Function()
        F22=Function()
        F23=Function()

        def f1(self):
            return "ok"

        def f2(self):
            return "ko"

    console.add(list(ALL_CLASSES.keys()))
    console.add(MetaTest._fields)
    console.add(MetaTest._functions)


    console.add(MetaTest2._fields)
    console.add(MetaTest2._functions)



    obj=MetaTest2()
    obj.a=1
    #obj.b=2
    print(obj.a)
    print(obj.b)
    print(obj.f1())
    print(obj.f2())
    #error print(obj.f3())

