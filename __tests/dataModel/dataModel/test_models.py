from dataModel.models import *
from dataModel.Store import Store

def make_model(cls):
    console.add(cls.models())    
    console.add(cls.getFields())    
    console.add(cls.getFunctions())   
    console.add(cls.__help__())   
    console.add(cls.help())  

def test():

    class Dummy:

        def __init__(self,**args):
            print(args)
  

    class Test(Model):
        """
        documentation string
        """

        #doc attr
        C=Field(record=False)
        D=Field()        #doc attr

        F21=Function(Decorator(a=1),Decorator(b=2),a=Field(),b=Field(default=23))
        F22=Function(ReturnFrame())
        F23=Function(ReturnNode(Class=Dummy))
        F24=Function(ReturnMerge(Class=Store))

        def f22(self):
            for i in range(10):
                yield Dummy(i=i)

        def f21(self,a=None,b=None):
            print("execution f21",a,b)


        def f23(self):
            return dict(a=1,b=2)


        def f24(self):

            return self.recursive(3)


        def recursive(self,level):

            if level>0:
                for i in range(3):
                    yield self.recursive(level-1)
            else:
                yield level



    class Test2(Test):
        """
        documentation string
        """

        #doc attr
        A=String()
        V=Integer()    
        VAR_1=Float()   
        VAR_2=Boolean()   
        VAR_3=List()   

        F1=Function()


    console.add(help())

    make_model(Model)    
    make_model(Test2)    

    print("creation")
    obj=Test2()
    #console.add(obj.help())   



    try:
        obj.f21()
    except:
        pass

    obj.f21(a=1)
    obj.f21(a=2,b=3)

    console.add(dict(obj))
    console.add(obj.get_records())
    console.add(obj.actions())

    print(obj.f23())
    print(obj.f22())
    print()
    print(obj.f24())


    obj.var_3="a,b,c"
    print( obj.var_3)

