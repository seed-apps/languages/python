from dataModel.Store import Store
import console


def test_list(lst):

    print("len",len(lst))
    console.add(lst)
    for i in range(len(lst)):
        a=lst[i]



def test():
    lst=list(range(10))
    store=Store(lst)
    test_list(lst)
    test_list(store)
    print("empty",store.empty())
    print("first",store.first())
    print("last",store.last())
    print("contains True",3 in store)
    print("contains False","a" in store)
    store.append(6)
    print("remove_doublons",store.remove_doublons())
    store.remove(3)
    store.extend(["a","b","c"])
    print("classes",store.classes())

    for k,v in store.by_classes():
        print(k,v)


    print("execute __str__",store.execute("__str__"))
    print("execute __len__",store.execute("__len__"))
    print("copy",store.copy())
    print("+",store+store.copy())
    print("-",store-store.copy())
    print("equality",store==store.copy())

    print("intersection",store.intersection([1,2,3]))
    print("union",store.union([1,2,3]))
    print("difference",store.difference([1,2,3]))
    same,add,remove=store.compare([1,2,3])
    print("same",same)
    print("add",add)
    print("remove",remove)

    print("symetric_difference",store.symetric_difference([1,2,3]))
    console.add("carthesian_product",store.carthesian_product([1,2,3]))

    store.clear()
    print("empty",store.empty())
    store.destroy()
