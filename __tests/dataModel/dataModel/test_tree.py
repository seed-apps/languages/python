from dataModel.structures.tree.Tree import Tree
from  dataModel.structures.tree.NamedTree import NamedTree

from dataModel.models import *


class MyTree(Model,Tree):

    pass

class MyNamedTree(Model,NamedTree):

    pass

def make_tree(cls,level,parent=None):

    parent=cls(parent=parent)

    if level>1:
        for i in range(3):
            make_tree(cls,level-1,parent=parent)

    return parent

def test_tree(cls,level):

    node=make_tree(cls,level)
    node.tree()

    print("children",len(node.children))
    print("leaves",len(node.leaves))
    print("descendants",len(node.descendants))
    print("deph",node.deph)


    for i,level in node.getLevels():
        print(i,len(level))


    for i,level in node.getLevels():
        print(i,len(level))

    for i in range(node.deph):
        print(i,len(node.getDephArea(i)),len(node.getRange(i-1,i)))



    if "name" in node.keys():
        for elt in node.descendants:
            print(elt.path(root="/"))

        for elt in node.descendants:
            print(elt.find(elt.path(root="/"))==elt)

        for elt in node.children:
            print(elt.find("./../"+elt.name)==elt)

        print("mirror")
        mirror_tree=MyNamedTree()
        for elt in node.descendants:
            mirror_tree.append(elt.path(root="/"),cls=MyNamedTree)
        mirror_tree.tree()

    else:
        for elt in node.descendants:
            print(len(elt.ancestors),len(elt.children),len(elt.descendants))




    node.clear()
    node.tree()
    node.destroy()


def test():
    test_tree(MyTree,4)
    test_tree(MyNamedTree,4)
