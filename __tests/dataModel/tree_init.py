
from dataModel import *
from dataModelAddons.tree import Tree,Tree_Initializer


class Test(Model,Tree,Tree_Initializer):
    def onPreSetup(self):print(self.path(),"onPreSetup")
    def onSetup(self):print(self.path(),"onSetup")
    def onPostSetup(self):print(self.path(),"onPostSetup")
    def onPreCleanup(self):print(self.path(),"onPreCleanup")
    def onCleanup(self):print(self.path(),"onCleanup")
    def onPostCleanup(self):print(self.path(),"onPostCleanup")


root=Test()
node0=Test(parent=root,name="1")
node1=Test(parent=root,name="2")

#premier clean, non
print("CLEANUP")
root.cleanup()
#setup ok
print("SETUP")
root.setup()
#cleanup ok
print("CLEANUP")
root.cleanup()


#repeté, non executé
print("SETUP")
root.setup()
print("SETUP")
root.setup()
print("CLEANUP")
root.cleanup()
print("CLEANUP")

root.cleanup()
print("RESET")
root.reset()

print("UNACTIVE CHILD")
node0.active=False

print("SETUP")
root.setup()

print("RESET")
root.reset()
print("RESET")
root.reset()

print("END")



