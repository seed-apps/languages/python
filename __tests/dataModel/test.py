# -*- coding: utf-8 -*-


#=======================================================================

class ClassTest():

#=======================================================================

    CLASS=None

    def setUp(self):
       self.obj=self.CLASS()
 
    # Cette méthode sera appelée après chaque test.
    def tearDown(self):
        self.obj.destroy()


#=======================================================================

class DictTest(ClassTest):

#=======================================================================



    def test_keys(self):

        """Node keys"""
        n=10
        for elt in range(n):
            self.obj[str(elt)]=elt
        self.assertEqual(len(self.obj.keys()),n)

    def test_values(self):

        """Node values"""

        n=10
        for elt in range(n):
            self.obj[str(elt)]=elt
        r=list(self.obj.values())
        r.sort()
        self.assertListEqual(r,list(range(n)))


    def test_items(self):

        """Node items"""
        n=10
        comp=dict()

        for elt in range(n):
            comp[str(elt)]=elt
            self.obj[str(elt)]=elt
        r=dict(self.obj.items())

        self.assertDictEqual(r,comp)

    def test_getitem(self):

        """Node getitem"""
        self.obj["test"]=1
        self.assertEqual(self.obj["test"],1)



    def test_setitem(self):

        """Node setitem"""
        self.obj["test"]=1
        self.assertDictEqual(dict(test=1),dict(self.obj))

#=======================================================================

class FrameTest(ClassTest):

#=======================================================================

    CLASS=None
    FRAME=None
    SIZE=10


    def setUp(self):
        self.frame=self.FRAME()
        for i in range(selfSIZE):
            self.frame.append(self.CLASS(name=i))

    # Cette méthode sera appelée après chaque test.
    def tearDown(self):
        self.frame.destroy()
#=======================================================================
#=======================================================================


