
from semanticpy import *



manager=Semantic_Manager()




proto=Semantic_Fullpath(parent=manager,name="http",schemes="http",default_port="80")
proto=Semantic_Fullpath(parent=manager,name="http",schemes="https",default_port="443")
proto=Semantic_Fullpath(parent=manager,name="file",schemes="file")
proto=Semantic_Fullpath(parent=manager,name="ssh",schemes="ssh",default_port="22")
proto=Semantic_Fullpath(parent=manager,name="ftp",schemes=["ftp","sftp"])
proto=Semantic_Fullpath(parent=manager,name="davs",schemes="davs")
gps=Semantic_Shortpath(parent=manager,name="gps",schemes="gps")
t=Semantic_Shortpath(parent=manager,name="time",schemes="time")
t=Semantic_Shortpath(parent=manager,name="tel",schemes="tel")
t=Semantic_Shortpath(parent=manager,name="apt",schemes="apt")


manager.setup()

import fileinput
 
done=list()
for string in fileinput.input():
    string=string.replace("\n","")
    if string != "":
        manager.get_url(string)
    
manager.tree()

