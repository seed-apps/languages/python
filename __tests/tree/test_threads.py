
#from core.program import *
import console
import time

from dataModel.models import *
from core.program import Thread,Run,ThreadPool

class MyRun(Run):
    #---------------------------------------------------
    def onPreSetup(self):
        "actions a realiser avant le setup"
        return "setup "+self.path()#pass
   #---------------------------------------------------

    def onCall(self):
        return "call "+self.path()


    def onPreCleanup(self):
        "overload to have a behavior"
        return "cleanup "+self.path()#pass
    
   #---------------------------------------------------


class MyThread(Thread):
    #---------------------------------------------------
    def onPreSetup(self):
        "actions a realiser avant le setup"
        return "setup "+self.path()#pass
   #---------------------------------------------------

    def onProcess(self):
        yield "call "+self.path()
        time.sleep(0.10)
        yield Thread.onProcess(self)
   #---------------------------------------------------


    def onPreCleanup(self):
        "overload to have a behavior"
        return "cleanup "+self.path()#pass
    
   #---------------------------------------------------
    def onJoin(self):
        yield "join "+self.path()
    def onStart(self):
        yield"start "+self.path()


def make_tree(cls,level,parent=None):

    parent=cls(parent=parent)

    if level>1:
        for i in range(5):
            make_tree(cls,level-1,parent=parent)

    return parent

def test_tree(cls,level):



    node=make_tree(cls,level)
    node.tree()
    return node

def test():

    objs=test_tree(MyThread,4)
    for i in objs.descendants:
        MyRun(parent=i)
        MyRun(parent=i)
    console.add(objs.stats())
    console.add(objs.run())
    console.add(objs.stats())
    exit()
    root=ThreadPool()
    for i in range(10):
        MyRun(parent=root)
    console.add(root.run())

