
#from core.program import *
import console
import random

from dataModel.models import *
from core.program import Object,Run,Select,Print,Code,Command


class MyObject(Object):
    #---------------------------------------------------
    I=Integer()
    def onPreSetup(self):
        self.i=random.randint(1, 10)

   #---------------------------------------------------



def make_tree(cls,level,parent=None):

    parent=cls(parent=parent)

    if level>1:
        for i in range(3):
            make_tree(cls,level-1,parent=parent)

    return parent

def test_tree(cls,level):



    node=make_tree(cls,level)
    node.tree()
    return node

def print_select(select,obj):
    lst=Select(parent=obj,select=select).get_selection()
    print(select,len(lst))

def call_select(select,obj,string):
    lst=Select(parent=obj,select=select)
    Print(parent=lst,text=string)
    Code(parent=lst,text="print('python',%(i)i)")
    Command(parent=lst,text="echo bash %(i)i")
    console.add(lst.run())


def test():

    obj=make_tree(MyObject,3)
    obj.setup()
    obj.tree()

    select=Select(parent=obj,select="/*/[MyObject]")

    for elt in select.get_selection():
        print(elt.path(),elt.i)

    print_select("/**",obj)    
    print_select("/*/*",obj)    
    print_select("/*/*/*",obj)  

    print_select("/**/[MyObject]",obj)  
    print_select("/**/[MyObject]/[+i]",obj)  
    print_select("/**/[MyObject]/[i=5]",obj)
    print_select("/**/[MyObject]/[i<5]",obj)
    print_select("/**/[MyObject]/[i>5]",obj)
    print_select("/**/[MyObject]/[i>3]/[i<7]",obj)

    print_select("/1:1/[MyObject]",obj)
    print_select("/2:2/[MyObject]",obj)
    print_select("/3:3/[MyObject]",obj)
    print_select("/4:4/[MyObject]",obj)
    print_select("/5:5/[MyObject]",obj)

    call_select("/**/[MyObject]/[i>3]",obj,"%(class_name)s %(name)s %(i)i")

    #console.add(help())

    cmd=Command(parent=obj,text="ls")
    cmd.setup()
    cmd.set_selection([obj,])
    console.add(cmd.call())
    #console.add(obj)

    cmd=Command(parent=obj,text="ps -aux")
    cmd.setup()
    cmd.set_selection([obj,])
    console.add(cmd.call())

