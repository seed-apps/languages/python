from core.data import *
from core.program import *
from dataModel.models import *
import console

def print_select(select,obj):
    lst=Select(parent=obj,select=select).get_selection()
    print(select,len(lst))



def test_tree(path):

    node=Dir(text=path)
    node.setup()
    node.tree()
    console.add(node)
    console.tree_stats(node)
    return node



def test():
    node = test_tree(".")
    print_select("/**",node)
    print_select("/**/[Dir]/__init__.py/..",node)
    print_select("/**/[File]",node)
    print_select("/**/[File]/[extention=py]",node)
    print_select("/**/[File]/[extention=py]/[size>1000]",node)
    print_select("/**/[File]/[extention=py]/[name!=__init__.py]",node)

    run=Reader(parent=node,select="/**/[File]")
    lst=run.run()
    #console.add(lst)
    print("READ",run.select,len(lst))

    choice=ReaderChooser(parent=node,select="/**/[File]/[name!=__init__.py]")
    Reader(parent=choice,extentions=["py",])
    Reader(parent=choice,extentions=["xml",])
    lst=choice.run()
    #console.add(lst)
    print("READ",choice.select,len(lst))


    #node.destroy()


