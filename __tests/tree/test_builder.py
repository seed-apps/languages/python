
#from core.program import *
import console
import random

from dataModel.models import *
from core.program import *
from core.data import *

class MyBuilder(Builder):

    def onBuild(self,obj):
        obj.tree()
        return "BUILD "+self.path()


def make_tree(cls,level,parent=None):

    parent=cls(parent=parent)

    if level>1:
        for i in range(3):
            make_tree(cls,level-1,parent=parent)

    return parent


def test():

    obj=make_tree(Data,3)
    obj.tree()
    run=MyBuilder(parent=obj,select="/*/[Data]")
    console.add(run.run())

    run=MyBuilder(parent=obj,select="/")
    MyBuilder(parent=run,select="*/*/[Data]")
    console.add(run.run())

    run=MyBuilder(parent=obj,select="/*/[Data]")
    MyBuilder(parent=run,select="")
    MyBuilder(parent=run,select="*")

    console.add(run.run())
    


