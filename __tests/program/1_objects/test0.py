from atom import Atom,TreeLogs
from dataModel import *

class Test(Atom):
    TEST   =Function(TreeLogs())
    def onSetup(self):print(self.path(),"setup")
    def onCleanup(self):print(self.path(),"cleanup")

    def test(self):

        print(self.path(),"test")
    

root=Test()
node0=Test(parent=root,name="1")
node1=Test(parent=root,name="2")

root.query(action="setup")
root.query(action="cleanup")
root.tree()
