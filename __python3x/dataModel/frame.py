# -*- coding: utf-8 -*-
"""


"""

from minimal.store import Store,output_list

#===============================================================================

def output_frame(function):

#===============================================================================

    def execute(self,*args,**kwargs):

        return Frame(function(self,*args,**kwargs))

    return execute

#===============================================================================

class Frame(Store):

#===============================================================================
    """
    les structures sont des ensembles d'éléments.
    elles  ont plusieurs roles :

       - construire les noeuds 
       - construire les relations
       - gèrer les attributs et les actions

    elles animent les noeuds.

    Les structures Frame ajoutent et modifient les attributs des noeuds
    suivant des règles différentes.

    Une structure se comporte comme une liste de noeuds.
    Elle posséde l'interface permetant de le manipuler comme une liste.

    Les operations de comparaison et de combinaison entre structures.
    """


        

    #SEARCH
    #==============================================================



    #--------------------------------------------------------------------------
    @output_frame
    def except_class(self,*args):
        classes=list()
        #references circulaires sinon
        from .model import getClass
        for elt in args:
            classes.append(getClass(elt))
        for node in self:
            ok=True
            for cls in classes:
                if node.is_instance(cls)==True:
                    ok=False
                    break
            if ok ==True:
                yield node


    #--------------------------------------------------------------------------
    @output_frame
    def by_class(self,cls,cls_except=None,strict=False):
        #references circulaires sinon
        from .model import getClass
        cls=getClass(cls)
        if cls_except:
            cls_except=getClass(cls_except)


        if strict == False:
            if cls_except is None:
                for elt in self:
                    if elt.is_instance(cls):
                        yield elt
            else:
                for elt in self:
                    if elt.is_instance(cls) and not elt.is_instance(cls_except):
                        yield elt
        else:
            for elt in self:
                if elt.is_instance(cls,strict=True):
                    yield elt

    #--------------------------------------------------------------------------

    def attributes(self):
        """
        renvoie la liste des attributs des différents éléments
        """
        result=[]
        for node in self:
            for attr in node.keys():
                if attr not in result:
                    result.append(attr)

        return result

    #--------------------------------------------------------------------------
    @output_frame
    def by_attr(self,attr):
        """
        retourne la liste des éléments ayant l'attribut attr ::

           for elt in frame.by_attr("attribute"):
               elt["attribute"]
        """
        for elt in self:
            if attr in elt.keys():
                yield elt

    #--------------------------------------------------------------------------
    @output_frame
    def by_attr_value(self,attr,value):
        """
        retourne la liste des éléments ayant l'attribut attr ::

           for elt in frame.by_attr("attribute"):
               elt["attribute"]
        """
        for elt in self:
            if attr in elt.keys():
                if elt[attr] == value:
                    yield elt


    #--------------------------------------------------------------------------
    @output_list
    def get_attr(self,attr):
        """
        retourne une liste de tuples (valeur de l'attribut,element)
        """
        for elt in self:
            if attr in elt.keys():
                if elt[attr] is not None:
                
                    yield elt[attr],elt
        
          
    #--------------------------------------------------------------------------

    def sort(self,attr):
        """
        trie la liste d'attibuts et renvoie un tuple (attr,elt)
        """
        lst=self.get_attr(attr)
        lst.sort()
        return lst

    #--------------------------------------------------------------------------
    @output_frame
    def search(self,**args):
        """
        pour chaque élément test si les attributs de l'élément
        sont éqaux au dictionnaire args 
        """

        for elt in self:

            ok=True
            for k,v in args:
                if elt[k]!=v:
                    ok=False
                    break

            if ok == True:
                yield elt
          

    #--------------------------------------------------------------------------
    @output_frame
    def clone(self):
        """
        crée une nouvelle structure avec une copie des noeuds ::

            self.clone() == self -> False
        """
        return [elt.copy() for elt in self.__nodes]
    #---------------------------------------------------
    @output_frame
    def query(self,action=None,**args):
        for elt in self:
            yield elt.query(action,**args)
    #--------------------------------------------------------------------------

#=============================================================================


  
