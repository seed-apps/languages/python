"""
un dictionnaire contenant toutes les classes disponibles
"""
#import inspect
from ..fields import Field,Attribute
from ..functions import Function
#from .frame import Frame

ALL_CLASSES=dict()

#=============================================================================

def models():

#=============================================================================
    """
    renvoi la liste des classes ::

       for cls in pytree.models():
           ...

    renvoyer les champs ::

       for cls in pytree.models():
           for attr in self._fields.values():
               ...

    """

    return ALL_CLASSES.values()


#===============================================================

def concretes():

#===============================================================
    """
    retourne un dictionnaire de classes concrètes
    les classes concrètes héritent de Model
    """
    r=dict()
    for name,cls in ALL_CLASSES.items():
        if issubclass(cls,Model):
            r[name]=cls
    return r
#===============================================================

def abstracts():

#===============================================================
    """
    retourne un dictionnaire de classes abstraites
    les classes concrètes n'héritent pas de Model
    """
    from ..model import Model
    r=dict()
    for name,cls in ALL_CLASSES.items():
        if not issubclass(cls,Model):
            r[name]=cls
    return r

#===============================================================

def getClass(Class):

#===============================================================
    """
    renvoie la classe à partir du nom
    permet de tester et convertir une classe à la volée :
        cls1=getClass("name")
        cls2=getClass(cls1)
        print(cls1==cls2)

    renvoie une exception si la classe n'est pas trouvée
    """

    if type(Class) == str:

        if Class in ALL_CLASSES.keys():
            return ALL_CLASSES[Class]
        else:
            raise Exception("classe not found : %s"%Class)
    else:
        return Class


#=============================================================================

class ModelMeta(type):

#=============================================================================
    """
    ModelMeta est une meta classe, elle permet de construire les modèles.

    * une interface de programmation

    une classe ayant comme meta model ModelMeta, pourra être manipulée par
    un emsemble de commandes standards quelquesoit sa structure ainsi 
    que celle de ses instances.

    Des structures prédéfinies telles que Arbre et Réseau pourront être combinées.
    voir les Stuctures : "structures"
    voir la combinaison arbre réseau : "atom.model"

    elle collecte les attributs de classe héritant de Fields et Function 
    pour creer une interface de programmation dynamique pour chaque classe.

    * héritage de ModelMeta

    les différents modèles doivent déclarer ModelMeta comme metaclasse ::

       class MyModel(metaclass=ModelMeta):

            #déclaration des champs
            A=Field(int)
            B=Field(str)

            def method(self):
                "utilisation des champs"
                self.a=2
                print(self.a)

    les modèles sont des classes abstraites qui doivent etre combinées pour
    devenir utilisable.
    les modèle obligatoire est Model auquel on ajoute les autres modèles.

    le modèle MyModel doit etre utilisé avec la classe Model pour fonctionner ::

       class ConcreteClass(Model,MyModel):
           pass

    note sur l'écriture des variable :
       - variables en majuscule : les variables de classe avec Field
       - minuscule : les variables d'instance Attribute


    cette classe a été introduite pour gerer l'héritage multiple
    en python
    """

    #--------------------------------------------------------------------------

    CLASSES=list()

    #--------------------------------------------------------------------------
    def __init__(cls, name, bases, ns):
        """
        store the field definitions on the class as a dictionary

        mapping the field name to the Field instance.
        """

        if cls not in ALL_CLASSES.values():
            ALL_CLASSES[cls.__name__]=cls

        cls._fields = {}
        cls._functions = {}

        # loop through the namespace looking for Field instances
        for key, value in ns.items():

            if isinstance(value, Field):
                cls._fields[key.lower()] = value
                value.name=key.lower()

            elif isinstance(value, Function):
                cls._functions[key.lower()] = value
                value.name=key.lower()
            else:
                pass#print(key,value)


    #--------------------------------------------------------------------------

#=============================================================================
