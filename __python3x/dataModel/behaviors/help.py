
from .meta import ModelMeta
from ..fields import Field
from ..functions import Function
#=====================================================================

class Manage_Help(metaclass=ModelMeta):

#=====================================================================

    """
    fonctions pour lister l'aide associée l'objet
    """

    HELP=Function()
    HELP_CLS=Function()
    HELP_ATTRS=Function()
    ATTR_BY_CLS=Function()

    #---------------------------------------------------

    
    def help(self,cls_filter=None):

        """
        retourne une aide pour l'objet sous forme de dictionnaires
        imbriqués
        """

        cls=self.__class__

        models={}


        if cls_filter:
            for model in cls.models():
                if issubclass(model,cls_filter):
                    models[model.__name__]=self.help_cls(model=model)
        else:
            for model in cls.models():
                models[model.__name__]=self.help_cls(model=model)
            
        return models

    #---------------------------------------------------

    
    def help_cls(self,model=None):


        model=self.getClass(model)

        result={}

        result["doc"]=model.__doc__
        result["fields"]=self.help_attrs(model=model)
        result["functions"]=self.help_functs(model=model)


        return result

    #---------------------------------------------------

    
    def help_attrs(self,model=None):

        fields={}

        if hasattr(model,"_fields"):

            for k,v in model._fields.items():
                fields[ k ] = self.help_attr(k,v)
        return fields



    #---------------------------------------------------

    
    def help_functs(self,model=None):

        functions={}

        if hasattr(model,"_functions"):

            for k,v in model._functions.items():

                functions[ k ] = self.help_funct(k,v)

        return functions

    #---------------------------------------------------
    def help_funct(self,k,v):

        attrs=dict()
        url="/"+self.path()+"?action="+k
        for k_a,a in v.attributes.items():
            attrs[k_a]=self.help_attr(k_a,a,url=False)
            #url+="&"+k_a+"=%("+k_a+")s"

        return dict(attrs=attrs,doc=v.__doc__)


    #---------------------------------------------------

    def help_attr(self,k,v,url=True):

        if isinstance(v,Field):
            if v.cls is None:
                t=str(v.__class__.__name__)
            else:
                t=str(v.cls.__name__)
        else:
            t=str(type(v))

        if url==True:
            return dict(type=t,url="/"+self.path()+"?attr="+k)
        else:
            return dict(type=t)

    #--------------------------------------------------------------------------
    def attr_by_cls(self):
        cls=self.__class__
        
        models={}

        for model in cls.models():

            if hasattr(model,"_fields"):
                fields={}
                for k,v in model._fields.items():
                    if v.record == True:
                        fields[ k ] = self[k]

                models[model.__name__]=fields
            
        return models
    #--------------------------------------------------------------------------
