import os
from .meta import *

#==========================================================

class Manage_Query(metaclass=ModelMeta):
    
#==========================================================

    QUERY=Function()

    def parse_args(self,string,separator="&",**args):
        elts=string.split(separator)
        for elt in elts:
            k,v= elt.split("=")
            args[k]=v
        return args
    

    #-------------------------------------------------------
    def query(self,action=None,**args):

        if hasattr(self,action):
            f=getattr(self,action)
            return f(**args)

   #---------------------------------------------------

    def parse(self,addr,action=None,**args):
        attr=None

        if "?" in addr:
            path,query=addr.split("?")
            args=self.parse_args(query,**args)
        else:
            query=None
            path=addr
            
        if "#" in path:
            path,attr=path.split("#")
        
        if "action" in args.keys():
            action=args["action"]
            del args["action"]
            
        return path,attr,action,args
   #---------------------------------------------------
    #@result_or_error
    def queryX(self,path="",action=None,**args):
        #print(args)
 
        path,attr,action,args=self.parse(path,action=action,**args)
        current_node=self.__search(path=path)
        
        if attr is not None:  
            current_node=current_node[attr]
            
        if action is None:
            return current_node

        else:            
            return self.__execute(current_node,action=action,**args)
        
   #---------------------------------------------------
    def __execute(self,current_node,action="__help",**args):
        return getattr(current_node,action)(**args)

   #---------------------------------------------------
    def __help(self,**args):
        return "help"
              

   #---------------------------------------------------

