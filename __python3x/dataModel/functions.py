# -*- coding: utf-8 -*-

import inspect
#=============================================================================

class Function_Interface(object):

#=============================================================================
    """
    link between decorator and function
    create when function is assigned
    """
    def __init__(self,obj,function,node):
        self.obj=obj
        self.function=function
        self.node=node
        self.name=None

    def __call__(self,*args,**kwargs):
        #print("interf",self.obj.name,args,kwargs)
        return self.obj.exec_deco(self.node,self.function,*args,**kwargs)

#=============================================================================

class Decorator(object):

#=============================================================================
    """
    base class to make decorators
    """
    def __init__(self,**kwargs):
        self.kwargs=kwargs
        self.function=None


    def init(self,node,function):
        "function to verload to have behavior"
        self.function=function
        self.node=node

    def __call__(self,*args,**kwargs):
        #print("deco",self,args,kwargs)
        return self.onCall(self.function,*args,**kwargs)

    def onCall(self,function,*args,**kwargs):
        return function(*args,**kwargs)


#=============================================================================

class Function(object):

#=============================================================================

    """

    """

    #--------------------------------------------------------------------------

    def __init__(self,*decorators,**attributes):

        self.name=None
        self.decorators=decorators
        self.attributes=attributes
        self.function=None
    #--------------------------------------------------------------------------
    def setFunction(self,node):

        self.function=getattr(node,self.name)
        obj=Function_Interface(self,self.function,node)
        setattr(node,self.name,obj)



    #--------------------------------------------------------------------------
    def check_args(self,**args):

        "function to overload to have behavior"

        if len(self.attributes.items())>0:

            for name,attr in self.attributes.items():

                if name in args.keys():
                    kwargs[name]=attr.get_value(args[name])
                else:
                    kwargs[name]=attr.get_value(None)


    #--------------------------------------------------------------------------
    def exec_deco(self,node,function,*args,**kwargs):

        for decorator in self.decorators:

            decorator.init(node,function)
            function=decorator

        return function(*args,**kwargs)
                        
    #--------------------------------------------------------------------------

#=============================================================================

class BuilderInterface(object):

#=============================================================================

    def __init__(self,node,builder):

        self.node=node
        self.builder=builder

    #--------------------------------------------------------------------------
    def __call__(self):
        result = self.builder.onCall(selected_node=self.node,**self.builder.kwargs)

        if hasattr(result,"setup"):
            result.setup()
        return result

#=============================================================================

class Builder(Function):

#=============================================================================
    #--------------------------------------------------------------------------

    def __init__(self,**kwargs):
        self.name=None
        self.kwargs=kwargs
    #--------------------------------------------------------------------------
    def setFunction(self,node):

        function = BuilderInterface(node,self)
        setattr(node,self.name,function)

    #--------------------------------------------------------------------------
    def __call__(self,selected_node=None,**args):


        result = self.onCall(selected_node=selected_node,**self.kwargs)

        if hasattr(result,"setup"):
            result.setup()
        return result
    #--------------------------------------------------------------------------

    def onCall(self,**args):
        pass
    #--------------------------------------------------------------------------

#=============================================================================

