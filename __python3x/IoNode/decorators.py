#==========================================================

def process_response(response):

#==========================================================

    if hasattr(response,"__iter__") and not hasattr(response,"__len__"):

        for elt in response:
            if elt is not None:
                for subelt in process_response(elt):
                    yield subelt
    else:
        yield response
#==========================================================

def to_output(function,self,*args, **kwargs):
    
#==========================================================

    r= function(*args,**kwargs)
    if r is not None:
        for elt in process_response(r):
            self.logs(data=elt)

    return
 



#==========================================================

def logs():
    
#==========================================================

    #---------------------------------------------------

    def decorated(function):

        def wrapper(self,*args, **kwargs):

            r= function(self,*args,**kwargs)
            if r is not None:
                for elt in process_response(r):
                    self.logs(data=elt)

            return
        return wrapper

    return decorated
    #---------------------------------------------------

#==========================================================
