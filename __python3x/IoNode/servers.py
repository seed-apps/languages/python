from .node import *
from dataModel import *
from mod_program import *

#===================================================================

class IoServer_Request(Log_Message):

#===================================================================

    #-------------------------------------------------------
    def setup(self):
        pass

    #-------------------------------------------------------
    def call(self):
        return ""
    #-------------------------------------------------------

#===================================================================

class IoServer_Module(Module):

#===================================================================

    #-------------------------------------------------------
    def onTestRequest(self,request):
        return True
    #-------------------------------------------------------
    def onProcessRequest(self,request):
        pass
    #-------------------------------------------------------

#===================================================================

class IoServer(IoNode):

#===================================================================

    MODULE_PATH=String(default="/")
    REQUEST_CLS=IoServer_Request
    #-------------------------------------------------------
    def onDo(self):
    #-------------------------------------------------------
        pass
    #-------------------------------------------------------
    def onProcessRequest(self,**args):
    #-------------------------------------------------------
        request=self.__class__.REQUEST_CLS(parent=self,server=self,**args)
        request.setup()

        for module in self.find(self.module_path).get_firsts(cls=IoServer_Module):
            if module.onTestRequest(request)==True:
                module.onProcessRequest(request)

        return request.call()

#===================================================================
