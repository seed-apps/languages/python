from .node import IoNode
from dataModel import *
import subprocess,shlex,os,signal,time
from mod_program import execute
#==========================================================================

def clean_lines(lines):

#==========================================================================

    for line in lines:
        try:
            line=line.decode("utf-8")
            yield line.replace("\n","")
        except:
            yield line.replace(b"\n",b"")

#==========================================================================

class IoNode_Command(IoNode):

#==========================================================================
    TEXT=String()
    EXEC=Boolean(default=True)
    PID=Field()
    DELAY=Float(default=0.2)
    KILL=Function()

    #----------------------------------------------------------------

    def onStart(self):
        time.sleep(self.delay)
        if self.exec==True:
            cmd="exec "+self.text
        else:
            cmd=self.text
        self.proc = subprocess.Popen(cmd, shell=True,stderr=subprocess.PIPE,stdout=subprocess.PIPE, preexec_fn=os.setsid)
        #proc.wait()
        self.pid=self.proc.pid

    #----------------------------------------------------------------

    def onDo(self):
        #print("do")
        time.sleep(self.delay)
        if self.proc.returncode is None:
            #out=list(clean_lines(proc.stdout.readlines() ))
            #err=list(clean_lines(proc.stderr.readlines() ))
            #print(out)
            #print(err)
            self.read_lines()


        else:
            self.read_lines()
            print("stop")
            self.stop()
    #----------------------------------------------------------------
    def read_lines(self):

        line = self.proc.stdout.readline()
        #print("LINE",line)
        if line != b'':
            line=str(line).replace("\n","")
            self.logs(text=line,tag="out")
            #self.send_to_pipe(data=line,tag="out")
        line = self.proc.stderr.readline()
        if line != b'':
            line=str(line).replace("\n","")
            self.logs(text=line,tag="err")
            #self.send_to_pipe(data=line,tag="err")
    #----------------------------------------------------------------

    def onStop(self):
        print("off")

        outs, errs = self.proc.communicate()
        print(outs, errs )
        #r=execute("kill "+str(self.pid))
        #r.parent=self
        self.kill()
        #os.killpg(os.getpgid(self.proc.pid), signal.SIGTERM)
        self.proc.terminate()
        self.proc.kill()
        self.proc.wait()
        #self.read_lines()

    def kill(self):
        r=execute("kill "+str(self.pid))
        r.parent=self
    #----------------------------------------------------------------


