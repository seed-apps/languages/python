
import os
from .text import *
#========================================================================

class FilePath_Store(list):

#========================================================================
    """
    liste de chemins
    """


    def __init__(self,lst=None):
        if lst is not None:
            self.extend(lst)

    def sort(self):

        lst=list()
        for elt in self:
            lst.append((elt.name(),elt))
        lst.sort()
        for elt in lst:
            yield elt[1]   

    def names(self,**args):

        for elt in self:
            yield FilePath(elt).name(**args)


    def relative(self,start,**args):

        start=FilePath(start)

        for elt in self:
            yield FilePath(elt).relative(start.string,**args)


    def directories(self):

        for elt in self:
            if elt.isdir():
                yield elt

    def files(self):

        for elt in self:
            if elt.isfile():
                yield elt


    def extentions(self):
        done=[]
        for elt in self:
            ext = elt.extention()
            if ext not in done:
                done.append(ext)
                yield ext
        del done

    def filterdirectories(self,initfile):

        for elt in self:
            if elt.join(initfile).exists()==True:
                yield elt


    def filterfiles(self,*extentions):
        for elt in self:
            #print(elt.extention(),extentions)
            if elt.extention().lower() in extentions:
                yield elt

    def filtername(self,startswith=None,contains=None):
        for elt in self:
            if startwith and elt.get_filename().startswith(startswith):
                yield elt
            elif contains and contains in elt.get_filename():
                yield elt


    def filtersystem(self):

        for elt in self:

            if elt.get_filename().startswith("__")!=True:
                yield elt

#============================================================

class FilePath(DataSource):

#============================================================

    LS=Function(ReturnFrame(Class=FilePath_Store))
    FILTEFILES=Function(ReturnFrame(Class=FilePath_Store))
    DIRECTORIES=Function(ReturnFrame(Class=FilePath_Store))
    FILES=Function(ReturnFrame(Class=FilePath_Store))

    #----------------------------------------------------------
    def onSetup(self):
    #----------------------------------------------------------

        #if path is given
        if self.value is None:
            self.value="."

        if type(self.value) == str:
            self.value=os.path.abspath(self.value)

        elif isinstance(self.value,FilePath):
            self.value=self.value.value

        else:
            raise Exception("type incorrect : %s = %s"%(str(type(value)),str(value)))


        #self.value=unicode(self.value)
        #self.value=self.value.decode('utf-8')
        #self.value=self.value.encode('utf-8')

        #http://www.developpez.net/forums/d1148561/autres-langages/python-zope/general-python/ouvrir-repertoire-accent-path/
        #self.value=self.value.decode('utf-8')#.encode('cp1252')

    #----------------------------------------------------------
    def read(self):
        f=open(self.value,"r")
        content=f.read()
        f.close()
        return content

    #----------------------------------------------------------
    def ls(self,recursive=False,hidden=False,**args):
    #----------------------------------------------------------

        """
        * renvoie la liste de tous les fichiers dans un objet FileList
        * options :
           * recursive : si True recursif, sinon repertoire courant, par défaut False
           * hidden : si True inclue les fichiers cachés, par défaut False
        """



        try:
            result = [FilePath(value=self.join(elt)) for elt in os.listdir(self.value)]
        except:
            return


        if hidden==False:

            for elt in list(result):
                if elt.get_filename().startswith("."):
                    result.remove(elt)

        if recursive==True:

            for elt in list(result):
                if elt.isdir():
                    result.extend(elt.ls(recursive=True,hidden=hidden))


        return result

    #----------------------------------------------------------
    def directories(self,**args):
    #----------------------------------------------------------
        for elt in self.ls(recursive=False,**args):
            if elt.isdir():
                yield elt

    #----------------------------------------------------------
    def files(self,**args):
    #----------------------------------------------------------
        for elt in self.ls(recursive=False,**args):
            if elt.isfile():
                yield elt

    #----------------------------------------------------------

    def filterfiles(self,*extentions,**args):

    #----------------------------------------------------------
        """
        * filtre les extentions
        * renvoie la liste de tous les fichiers dans un objet FileList
        * options :
           * recursive : si True recursif, sinon repertoire courant, par défaut False
           * hidden : si True inclue les fichiers cachés, par défaut False
        """



        for elt in self.ls(**args).files():
            #print(elt.extention(),extentions)
            if elt.extention().lower() in extentions:

                yield elt

    #----------------------------------------------------------

    def filterdirectories(self,initfile,**args):

    #----------------------------------------------------------

        """
        * renvoie la liste de tous les répertoires dans un objet FileList
        * options :
           * recursive : si True recursif, sinon repertoire courant, par défaut False
           * hidden : si True inclue les fichiers cachés, par défaut False
        """


        for elt in self.directories():
            if elt.join(initfile).exists():
                yield elt

    #----------------------------------------------------------

    #----------------------------------------------------------------

    def split(self):

        path,name=os.path.split(self.value)
        name,ext=os.path.splitext(name)
        ext=ext[1:]#retirer le point
        return path,name,ext   

    #----------------------------------------------------------------

    def get_filename(self,extention=True):

        name=os.path.split(self.value)[1]
        if extention==True:
            return name
        else:
            return os.path.splitext(name)[0]   

    #----------------------------------------------------------------

    def get_parentdir(self):
        return FilePath(value=os.path.split(self.value)[0])

    #----------------------------------------------------------------

    def get_extention(self):
        ext=os.path.splitext(self.value)[1]#recuperer l'extention
        ext=ext[1:]#retirer le point
        return ext
        
    #----------------------------------------------------------------

    def exists(self):

        return os.path.exists(self.value)

    #----------------------------------------------------------------

    def join(self,*args):

        return FilePath(value=os.path.join(self.value,*args))

    #----------------------------------------------------------------
    def make_dirs(self):

        if self.exists()==False:

            if self.is_file()==True:

                self.get_parentdir().make_dirs()

            else:

                os.makedirs(self.value)

                

    #----------------------------------------------------------------
    def isfile(self):

        return os.path.isfile(self.value)


    #----------------------------------------------------------------
    def isdir(self):

        return os.path.isdir(self.value)

    #----------------------------------------------------------------
    def islink(self):

        return os.path.islink(self.value)
    #----------------------------------------------------------------
    def hidden(self):

        return self.get_filename().startswith(".")

    #----------------------------------------------------------------
    def  relative(self,start):

        return os.path.relpath(self.value, str(start))
    #----------------------------------------------------------------
    def  cut(self,path):

        return self.value.replace(os.path.abspath(path)+"/","")

    #----------------------------------------------------------------

    def infos(self):
        """
        renvoie les infos de la fonction os.stat


        - st_mode - protection bits,
        - st_ino - inode number,
        - st_dev - device,
        - st_nlink - number of hard links,
        - st_uid - user id of owner,
        - st_gid - group id of owner,
        - st_size - size of file, in bytes,
        - st_atime - time of most recent access,
        - st_mtime - time of most recent content modification,
        - st_ctime - platform dependent; time of most recent metadata change on Unix, or the time of creation on Windows)


        https://docs.python.org/2/library/os.html
        """

        headers=("mode", "ino", "dev", "nlink", "uid", "gid", "size", "atime", "mtime", "ctime")
        infos=os.stat(self.value)
        infos=dict(zip(headers,infos))
        infos["path"]=self.value
        return infos
#======================================================================


