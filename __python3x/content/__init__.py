"""
travailler avec des valeurs
"""

from .numeric import *
from .text import *
from .geom import *
from .color import *
from .vector import *
from .data import *
from .path import *
from .char import *
from .reader import *
from .FileReader import *
from .writer import *
from .mimetype import *

