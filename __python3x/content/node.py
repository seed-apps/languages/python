from dataModel import *

from mod_program import Object
#============================================================

class Content(Object):

#============================================================


    CLS_MODEL=String(default="Content")




    def onSet(self,value):

        return value

    def onGet(self,value):

        return value

    def onDel(self,value):
        self.destroy()
        return


    def add(self,cls=None,value=None,**args):

        if not cls:cls=self.getClass(self.cls_model)
        cls=self.getClass(cls)
        #print(self.path(),cls,value,args)
        node=cls(parent=self,**args)

        if value is not None:
            node.value=value
        elif self.default is not None:
            node.value=self.default
        #exit()
        return node
    
    def set_data(self,name=None,cls=None,value=None,error=False,**args):

        node=self.find(name,error=error)

        if node:
            if value is not None:
                node.value=value
            return node
        else:
            return self.add(cls=cls,value=value,name=name,**args)

#============================================================


