#model:Class.py
"""
module
---------------------------------------------------

description

"""

from dataModel import *
from .node import *
from .numeric import *
from .text import *
from .geom import *
from .color import *
from .vector import *

def setData(data=None,**args):

    t=type(data)

    if t is dict:
        node=Dict(**args)
        for k,v in data.items():
            setData(data=v,name=k,parent=node)

    elif t in (list,tuple):
        node=List(**args)
        for v in data:
            setData(data=v,parent=node)

    elif t is bool:
        node=Content(value=data,**args)

    elif t is str:
        node=Text(value=data,**args)

    elif t in (int,float):
        node=Number(value=data,**args)

    else:
        raise Exception(data)


    return node
#=====================================================================

class DataSet(Dict):

#=====================================================================


    def Dict(self,**args):return self.add(cls=DataSet,**args)
    def List(self,**args):return self.add(cls=List,**args)

    def Number(self,**args):return self.add(cls=Number,**args)
    def NumericVector(self,**args):return self.add(cls=NumericVector,**args)
    def Vector(self,**args):return self.add(cls=Vector,**args)
    def FixedVector(self,**args):return self.add(cls=FixedVector,**args)

    def RGB(self,**args):return self.add(cls=RGB,**args)
    def RGBA(self,**args):return self.add(cls=RGBA,**args)

    def Vector2d(self,**args):return self.add(cls=Vector2d,**args)
    def Vector3d(self,**args):return self.add(cls=Vector3d,**args)
    def Text(self,**args):return self.add(cls=Text,**args)
    def Url(self,**args):return self.add(cls=Url,**args)
    def Path(self,**args):return self.add(cls=Path,**args)
    def FilePath(self,**args):return self.add(cls=FilePath,**args)

#=====================================================================
