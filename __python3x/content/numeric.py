from mod_program import Object

from dataModel import *
from .vector import List
#============================================================

class Number(Object):

#============================================================

    MAX=Float(default=None) #limite maximum
    MIN=Float(default=None) #limite minimum

    def onSet(self,value):

        if self.max and value > self.max :
            return self.max 

        if self.min and value < self.min :
            return self.min 

        return value

#============================================================

class Vector(List):

#============================================================

    MAX=Float(default=None) 
    MIN=Float(default=None)

    def set_data(self,**args):
        return List.set_data(self,cls=Number,
                min=self.min,
                max=self.max,
                **args)


    def onSet(self,value):

        if type(value) in (list,tuple):

            for elt in value:
                
                self.set_data(value=elt)


#============================================================

class FixedVector(Vector):

#============================================================

    DEFAULT=Field(default=0.0)
    DIM=None#definition for classes
    SIZE=Integer(default=None)

    #---------------------------------------------------
    def onSetup(self):

        for i in range(self.getSize()):
            self.set_data(name=str(i),value=self.default)

    def onSet(self,value):

        if type(value) in (list,tuple):

            i=0
            for elt in value[:self.getSize()]:
                
                self.set_data(name=str(i),value=elt)
                i+=1

    def getSize(self):
        if self.size:
            n=self.size
        elif self.DIM:
            n=self.DIM
        return n

#============================================================

class Random(Number):

#============================================================

    def onSet(self,value):

        return

    def onGet(self,value):

        return
#============================================================
