
from .model import Atom
from dataModel import *

class Atom_Manager(Atom):


    MAKE_UPDATE=Function()

    def onSetup(self):
        self.set_defaults()
        #self.make_update()

 
    def make_update(self):
        for path,data in self.onUpdate():
            self.onAppend(path,data)

    def onUpdate(self):
        pass

    def onAppend(self,path,data):
        pass

    def set_defaults(self):
        pass

class Atom_Manager_List(Atom_Manager):

    CLS=ModelField(default=Atom)
    SEPARATOR=String()

    def set_defaults(self):
        if hasattr(self,"CLS_DEFAULT"):
            self.cls=getClass( getattr(self,"CLS_DEFAULT") )
        if hasattr(self,"SEPARATOR_DEFAULT"):
            self.separator= getattr(self,"SEPARATOR_DEFAULT") 

    def onUpdate(self):
        pass

    def onAppend(self,path,data):
        self.append(path,cls=self.cls,separator=self.separator,**data)


class Atom_Manager_Tree(Atom):

    BRANCH_CLS=ModelField(default=Atom)
    LEAF_CLS=ModelField(default=Atom)
    SEPARATOR=String(default="/")

    def set_defaults(self):
        if hasattr(self,"BRANCH_CLS_DEFAULT"):
            self.cls=getClass( getattr(self,"BRANCH_CLS_DEFAULT") )
        if hasattr(self,"LEAF_CLS_DEFAULT"):
            self.cls=getClass( getattr(self,"LEAF_CLS_DEFAULT") )
        if hasattr(self,"SEPARATOR_DEFAULT"):
            self.separator= getattr(self,"SEPARATOR_DEFAULT") 

    def onUpdate(self):
        pass

    def onAppend(self,path,data):

        lst=path.split(self.separator)
        if len(lst)>1:
            path=self.separator.join(*lst[:-1])
            parent=self.append(path,separator=self.separator,cls=self.branch_cls)
            name=lst[-1]
        else:
            parent=self
            name=lst[0]

        parent.append(name,cls=self.leaf_cls,**data)

