

import sys
#========================================================================

def import_module(mod):

#========================================================================
    """
    importer un module python
    
    donner le nom complet du module a.b.c
    renvoie le module
    """
    __import__(mod)
    return sys.modules[mod]

#========================================================================

def import_object(cls):

#========================================================================
    """
    importer un object python
    
    donner le nom complet de l'objet avec le module a.b.obj
    renvoie l'objet

    cela peut etre n'importe quel objet python du module :
       * un attribut
       * une fonction
       * une classe
       * ...etc
    """
    lst=cls.split(".")
    cls=lst[-1]
    mod=".".join(lst[:-1])
    return getattr(import_module(mod),cls)


#===============================================================

