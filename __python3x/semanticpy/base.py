from urllib.parse import urlparse,parse_qs,urlsplit
import os,sys

from mod_program import Object
from dataModel import *

class Semantic_Node(Object):

    pass

class Semantic_DataSet(Object):

    pass

class Semantic_Router(Object):

    CLS=String(default="Semantic_Node")
    KEY=String(default="path")
    SEPARATOR=String(default="/")
    REVERSE=Boolean(default=False)
    DEFAULT_VALUE=String(default=None)
    
    def next_node(self,data,node):
        if self.key not in data.keys() or data[self.key]==None:
            if self.default_value:
                path=self.default_value
                return node.append(path,cls=self.cls,separator=self.separator,reverse=self.reverse,url=data)
            else:
                return node
        else:
            path=data[self.key]
            return node.append(path,cls=self.cls,separator=self.separator,reverse=self.reverse,url=data)
            


class Semantic_Test(Object):

    KEY=String()
    VALUE=String()

    def test(self,url_data):
        return url_data[self.key]==self.value

class Semantic_And(Semantic_Test):

    def test(self,url_data):
        for elt in self.children.by_class(Semantic_Test):
            if elt.test(url_data)==True:
                return False
        return True

class Semantic_Or(Semantic_Test):
    SCHEMES=List()

    def test(self,url_data):
        for elt in self.children.by_class(Semantic_Test):
            if elt.test(url_data)==True:
                return True
        return False
            
class Semantic_Protocole(Object):

    def onSetup(self):
        if self.schemes:
            if type(self.schemes)!=str :
                if len(self.schemes)>1:
                    test=Semantic_Or(parent=self)
                    for elt in self.schemes:
                        Semantic_Test(parent=test,key="scheme",value=elt)
                else: 
                    Semantic_Test(parent=self,key="scheme",value= self.schemes[0])
            else:
                Semantic_Test(parent=self,key="scheme",value= self.schemes)
        self.define_path()
        
    def define_path(self):
        pass
        
    def test(self,url_data):
        for elt in self.children.by_class(Semantic_Test):
            if elt.test(url_data)!=True:
                return False
        return True
        
    def get_url(self,url_data,dataset=None):
        #print(self.path(),url_data)
        node=dataset
        for elt in self.children.by_class(Semantic_Router):
            node=elt.next_node(url_data,node)
        return node

class Semantic_Fullpath(Semantic_Protocole):

    DEFAULT_PORT=String(default='80')

    def define_path(self):
        Semantic_Router(parent=self,key="host",separator=".",reverse=True)
        Semantic_Router(parent=self,key="scheme")
        Semantic_Router(parent=self,key="port",default_value=self.default_port)
        Semantic_Router(parent=self,key="user")
        Semantic_Router(parent=self)
        Semantic_Router(parent=self,key="fragment")
        
class Semantic_Shortpath(Semantic_Protocole):

    def define_path(self):
        Semantic_Router(parent=self,key="scheme")
        Semantic_Router(parent=self)

class Semantic_Manager(Object):

    def onSetup(self):
        self.dataset=Semantic_DataSet(parent=self,name="dataset")


    def get_url(self,url):
        data= urlparse(url)
        r=dict()
        r["url"]=url
        r["scheme"]=data[0]
        r["netloc"]=data[1]
        r["path"]=data[2]
        r["query"]=data[4]
        r["fragment"]=data[5]
        
        if "@" in r["netloc"]:
            a,b=r["netloc"].split("@")
            r["user"]=a
            r["host"]=b
        else:
            r["user"]=None
            r["host"]=r["netloc"]

        if ":" in r["host"]:   
            a,b=r["host"].split(":")
            r["host"]=a
            r["port"]=b                 
        else:
            r["port"]=None                
             
        r["query"]=parse_qs(r["query"])
        for k,v in r.items():
            if v =="":
                r[k]=None
        #print(url)
        #print(r)
        #print(urlsplit(url))
        for elt in self.children.by_class(Semantic_Protocole):
            if elt.test(r)==True:
                return elt.get_url(r,self.dataset)
        

