########################################
# text
########################################

PIP3 "PyWavelets"
PIP3 "pyyaml"

# https://github.com/clips/pattern
# https://www.springboard.com/blog/data-mining-python-tutorial/
# https://analyticsindiamag.com/hands-on-guide-to-pattern-a-python-tool-for-effective-text-processing-and-data-mining/
PIP3 "pattern3"

# https://dataconomy.com/2015/01/python-packages-for-data-mining/
# https://code.tutsplus.com/tutorials/how-to-work-with-pdf-documents-using-python--cms-25726


