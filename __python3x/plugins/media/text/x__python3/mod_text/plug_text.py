from content import Read
#===============================================================

class Read_Text(Read):

#===============================================================
    mime_type="text"
    #-----------------------------------------------------

    def onRead(self,content=None,**args):
        for elt in content:
            yield dict(string=elt)

    #-----------------------------------------------------

    def onPreprocess(self,content=None,comment=None,**args):

        content=content.split("\n")
        result=[]

        for line in content:

            line=line.replace("\n","")
            result.append(line)

        return result


    #-----------------------------------------------------

#===============================================================

class Read_CleanText(Read_Text):

#===============================================================

    #-----------------------------------------------------

    def onPreprocess(self,content=None,comment=None,**args):

        content=content.split("\n")
        result=[]

        for line in content:

            line=line.replace("\n","")

            if comment is not None:
                i=line.find(comment)                
                if i>-1:
                    line=line[:i]

            line=line.strip()

            if line !="":
                result.append(line)

        return result


    #-----------------------------------------------------
#===============================================================
