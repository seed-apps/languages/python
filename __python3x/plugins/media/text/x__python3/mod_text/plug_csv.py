from content import Read,Write
from .plug_text import Read_CleanText
#import unicodecsv
from dataModel import *

#========================================================================

class Read_Csv(Read_CleanText):

#========================================================================
    DELIMITER=String(default=",")
    #-----------------------------------------------------

    def onPreprocess(self,content=None,comment=None,**args):

        content=content.split("\n")
        result=[]

        for line in content:

            line=line.replace("\n","")

            if comment is not None:
                i=line.find(comment)                
                if i>-1:
                    line=line[:i]

            line=line.strip()

            if line !="":
                result.append(line)

        return result


    #----------------------------------------------------------------

    def onRead(self,content=None):


        headers=content[0].split(self.delimiter)
        data = content[1:]

        for row in data:
            row=row.split(self.delimiter)
            yield dict(zip(headers,row))

    #----------------------------------------------------------------

#========================================================================

class Write_Csv(Write):

#========================================================================


    #----------------------------------------------------------------

    def onWrite(self,node=None,headers=None,delimiter=";",**args):


        yield delimiter.join(headers)+delimiter

        #prepare data
        for child in node.children:
            string=""
            for head in headers:
                string=string+unicode(child[head])+delimiter
            yield string

    #----------------------------------------------------------------
#========================================================================

class Read_Csv2(Read_CleanText):

#========================================================================
    """
    lecteur pour fichiers ne respectant pas le retour à la ligne
    """
    DELIMITER=String(default=",")
    
    #-----------------------------------------------------

    def onPreprocess(self,content=None,comment=None,**args):
        header=content[0:content.find("\n")]

        n=len(header.split(self.delimiter))#nombre de colonnes

        content=content.split("\n")
        result=[]


        i=0#compteur d'objets
        m=len(content)#nombre d'objets
        print(m)

        while i < m:
    
            if content[i].strip() != "": 

                data=content[i].split(self.delimiter)
                j=len(data)

                if j==n:
                    result.append(data)
                elif j>n:
                    raise Exception("bad len for csv 2")
                elif j <n:

                    while len(data) < n:

                        if data[-1].startswith("\""):
                            ok=True
                            while ok:
                                i+=1
                                if "\"" in content[i]:
                                    k=content[i].find("\n")
                                    data[-1]=data[-1]+content[i][0:k]
                                    data.extend(content[i][k:-1].split(self.delimiter))
                                    ok=False
                                else:
                                    data[-1]=data[-1]+content[i]
                        else:
                            i+=1
                            data2=content[i].split(self.delimiter)
                            data.extend(data2)

                    result.append(data)
            i+=1
        return result
    #----------------------------------------------------------------
    def onRead(self,content=None):


        headers=content[0]
        data = content[1:]

        for row in data:
            yield dict(zip(headers,row))

    #----------------------------------------------------------------
#========================================================================
