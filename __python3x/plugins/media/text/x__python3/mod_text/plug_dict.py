from content import Read,Write
from .plug_text import Read_CleanText
#========================================================================

class Read_Dict(Read_CleanText):

#========================================================================
    mime_type="dict"
    #----------------------------------------------------------------

    def onRead(self,content=None,delimiter="=",**args):

        result=dict()
        for line in content:
            k,v=line.split(delimiter)
            result[k]=v

        yield result

    #----------------------------------------------------------------

    def onPostprocess(self,result=None,**args):

        node=result.children[0]
        node.parent=None
        result.destroy()

        return node

    #----------------------------------------------------------------


#========================================================================

class Write_Dict(Write):

#========================================================================


    #----------------------------------------------------------------

    def onWrite(self,node=None,delimiter="=",**args):

        for k,v in node.items():
            yield k+delimiter+unicode(v)

    #----------------------------------------------------------------

#========================================================================
