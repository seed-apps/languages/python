

from content import Read,Write
from .plug_text import Read_CleanText

#========================================================================

class Read_Conf(Read_CleanText):

#========================================================================

    mime_type="conf"

    #----------------------------------------------------------------

    def onRead(self,content=None,delimiter="=",**args):


        result=None

        for line in content:

            if line.startswith("["):

                if result is not None:
                    yield result

                result=dict()
                i=line.find("[")
                j=line.find("]")
                result["name"]=line[i+1:j].strip()

            else:

                k,v=line.split(delimiter)
                result[k]=v

        if len(result)>0:
            yield result

    #----------------------------------------------------------------


#========================================================================

class Write_Conf(Write):

#========================================================================


    #----------------------------------------------------------------

    def onWrite(self,node=None,delimiter="=",**args):

        for child in node.children:
            yield "[%s]"%unicode(child.name)
            yield ""
            for k,v in child.items():
                if k != "name":
                    yield k+delimiter+unicode(v)
            yield ""
    #----------------------------------------------------------------

#========================================================================
