import threading


from pythonosc import dispatcher
from pythonosc import osc_server


from mod_program import *
from IoNode import *
from dataModel import *

def string_to_dict(string,separator=" ",equal="="):
    d=dict()
    if string=="msg=empty":
        return d
    for elt in string.split(separator):
        if elt.strip() !="":
            if equal in elt:
                k,v=elt.split(equal)
                d[k]=v

    return d    
#===============================================================================

class Osc_Server_Action(Action):

#===============================================================================
    ACTION=String()

    def onSetup(self):
        self.select_node()

    #@treelogs
    def onReceive(self,path,msg):#path,msg):
        #print(*args,**kwargs)
        d=string_to_dict(msg)
        if self.action is not None:
            self.selected_node.query(action=self.action,**d)

#===============================================================================

class Osc_Server(IoNode):

#===============================================================================

    IP=String(default="localhost")
    PORT=Integer()
    
    
    #---------------------------------------------------------------------------        
    def map(self,path,funct,*args):
        #print(path,funct,*args)        
        self.dispatcher.map(path,funct,*args)

    #---------------------------------------------------------------------------
    def onStart(self):
        self.dispatcher = dispatcher.Dispatcher()
        for elt in self.children.by_class(Osc_Server_Action):
            self.map("/"+elt.name,elt.onReceive)

        self.server = osc_server.ThreadingOSCUDPServer( (self.ip, self.port), self.dispatcher)
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server_thread.start()

    #---------------------------------------------------------------------------
    def onStop(self):
        self.server.shutdown()

    #----------------------------------------------------------------
    def onDoChildren(self):
        for elt in self.children.by_class("Code"):
            if not isinstance(elt,IoNode_Atom) and not isinstance(elt,Osc_Server_Action):
                yield elt.call()

    #---------------------------------------------------------------------------

#===============================================================================
