
from pythonosc import osc_message_builder
from pythonosc import udp_client


from IoNode import IoNode
from dataModel import *

#===============================================================================

class Osc_Client(IoNode):

#===============================================================================

    IP=String(default="localhost")
    PORT=Integer()
    
    
    #---------------------------------------------------------------------------
    def onStart(self):
        self.client = udp_client.SimpleUDPClient(self.ip, self.port)

    #----------------------------------------------------------------
    def onReceiveMessage(self,message):

        if "?" in message:
            path,message=message.split("?")
        else:
            path,message=message,"msg=empty"
        #print(path,message)
        self.client.send_message(path,message)
    #---------------------------------------------------------------------------
    def onStop(self):
        pass#print(dir(self.client))
    #---------------------------------------------------------------------------

#===============================================================================
