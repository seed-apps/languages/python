from mod_program import Object,Data,Action
from dataModel import *
import time
import numpy as np
import matplotlib.pyplot as plt
#=================================================================

class Curve_Object(Data,Network_Node):

#=================================================================
    pass
#=================================================================

class Curve(Curve_Object):

#=================================================================

    #------------------------------------------------------------
    def infos(self):
        return

    #------------------------------------------------------------
    def plot(self):

       pass

#=================================================================

class Curve_Axe(Curve_Object):

#=================================================================

    CONTENT=List()

    #------------------------------------------------------------
    def get_intervals(self):
        """
        appeler laa fonction pour generer la liste
        si deja faite, renvoie la liste
        """
        if hasattr(self,"intervals") !=True:

            self.make_intervals()
        return self.intervals

    #------------------------------------------------------------
    def make_intervals(self):

        self.intervals=list()
        dt0=self.content[0]
        for elt in self.content[1:]:

            self.intervals.append(elt-dt0)
            dt0=elt
    #------------------------------------------------------------
#=================================================================

class Curve2D_Add(Action):

#=================================================================
    X=String()
    Y=String()
    CONVERT=Boolean(default=True)
    CONVERSION=String(default="%Y-%m-%d %H:%M:%S")

    def get_curve(self):
        for elt in self.ancestors:
            if isinstance(elt,Curve):
                return elt

    #------------------------------------------------------------
    def onNextCall(self,node):
        curve=self.get_curve()

        if self.x in node.keys() and self.y in node.keys():
            curve.axe_x.content.append(float(self.convert(node[self.x])))
            curve.axe_y.content.append(float(node[self.y]))
    #------------------------------------------------------------
    def convert(self,string):
        elt= time.strptime(string, self.conversion)
        return time.strftime('%s',elt)

    #------------------------------------------------------------

#=================================================================

class Curve2D(Curve):

#=================================================================

    X=String(default="x")
    Y=String(default="y")

    #------------------------------------------------------------
    def onSetup(self):

        self.axe_x=self.append(self.x,cls=Curve_Axe,setup=True)
        self.axe_y=self.append(self.y,cls=Curve_Axe,setup=True)


    #------------------------------------------------------------
    def infos(self):
        return self.axe_x.content,self.axe_y.content


    #------------------------------------------------------------
    def plot(self):
        x,y=self.infos()
        plt.plot(x,y)
        plt.show()
    #------------------------------------------------------------

#=================================================================

class Curve_Group(Curve_Object):

#=================================================================
    pass
#=================================================================

class Curve_Sampler(Curve_Group):

#=================================================================
    SELECT=String(default="/")
    CLS_TARGET=String(default="Atom")
    DT=Float(default=200.0)
    CONVERSION=String(default="%Y-%m-%d %H:%M:%S")
    X=String()
    Y=String()
    #------------------------------------------------------------
    def onSetup(self):
        self.i=0
        self.t0=None
        self.node=self.find(self.select)
        self.cls_obj=self.getClass(self.cls_target)
        self.curve=self.append(str(self.i),cls=Curve,setup=True,node=self.node,cls_target=self.cls_target)
        for elt in self.node.all().by_class(self.cls_obj):
            self.build_data(elt)

    #------------------------------------------------------------
    def build_data(self,node):



        if self.x in node.keys() and self.y in node.keys():

            t=self.convert(node[self.x])

            #premier
            if self.t0 is None:
                self.t0=t
                self.curve.append_data(x=node[self.x],y=node[self.y])

            #autres
            else:
                #calcul de l'intervalle

                dt=t- self.t0

                #si plus petit que la ref, on ajoute à la courbe
                if dt< self.dt:
                    self.curve.append_data(x=node[self.x],y=node[self.y])

                #si plus grand, on cree une nouvele courbe
                else:
                    self.i+=1
                    self.curve=self.append(str(self.i),cls=Curve,setup=True,node=self.node,cls_target=self.cls_target)
                    self.curve.append_data(x=node[self.x],y=node[self.y])

                self.t0=t
    #------------------------------------------------------------
    def convert(self,string):
        elt= time.strptime(string, self.conversion)
        return int(time.strftime('%s',elt))

    #------------------------------------------------------------
#=================================================================

class Curve_AudioList(Curve_Object):

#=================================================================

    MIMETYPE=String()
    SELECT=String(default="/")
    CONVERT=Boolean(default=False)
    CONVERSION=String(default="%y%m%d-%H%M%S")

    def onSetup(self):

        self.content=[]
        self.node=self.find(self.select)

        for elt in self.node.all().by_class("Disc_File"):
            self.build_data(elt)
        
    def build_data(self,node):

        if node.mimetype()[0]==self.mimetype:
            #print(node)
            path=node.getPath()
            self.content.append(self.convert(path.name(extention=False) ) )


        
    def convert(self,string):
        #return string
        elt= time.strptime(string, self.conversion)
        return int(time.strftime('%s',elt))

#=================================================================


