from mod_program import Object,Action,Data
from dataModel import *
from atom import Atom,Atom_Link
import time
import numpy as np
from .curve import Curve_Object
import matplotlib.pyplot as plt
#=================================================================

class Histogram_Link(Atom_Link):

#=================================================================
    pass
#=================================================================

class Histogram_Cathegory(Curve_Object):

#=================================================================
    pass

#=================================================================

class Histogram_Add(Action):

#=================================================================
    ATTR=String()
    def get_curve(self):
        for elt in self.ancestors:
            if isinstance(elt,Histogram):
                return elt
    #------------------------------------------------------------
    def onNextCall(self,node):
        curve=self.get_curve()
        value=getattr(node,self.attr)
        print(value)
        cat=curve.find(str(value),cls=Histogram_Cathegory)
        Histogram_Link(source=cat,target=node)  

#=================================================================

class Histogram(Curve_Object):

#=================================================================

    LIMIT=Integer(default=1)

    #------------------------------------------------------------
    def infos(self):
        x,y=list(),list()
        for elt in self.children.by_class(Histogram_Cathegory):
            l=len(elt.outputs)
            if l>self.limit:
                x.append(elt.name)
                y.append(l)
        return x,y
    #------------------------------------------------------------
    def plot(self):
        x,y=self.infos()
        plt.bar(x,y)
        plt.show()
    #------------------------------------------------------------


