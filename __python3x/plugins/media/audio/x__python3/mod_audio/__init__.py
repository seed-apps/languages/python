__doc__="""
module audio pour manipuler les fichiers audio et les streams
"""

from .mixer import *
from .audio import *
