


from mod_program import *
from dataModel import *
import mido,time
from mido.ports import MultiPort
from IoNode import *
from mido import Message, MidiFile, MidiTrack

#===============================================================================

class Midi_Loop(IoNode):

#===============================================================================

    LOOP=Integer(default=10)


    #---------------------------------------------------------------------------
    def onDo(self):
        i=0
        while i < self.loop:   
            self.onDoMidi()
            i+=1
        if not self.checkMidi():
            self.stop()
    #---------------------------------------------------------------------------
    def onDoMidi(self):
        pass
    #---------------------------------------------------------------------------
    def checkMidi(self):
        return True
    #---------------------------------------------------------------------------
    def midi_message(self,action=None,**args):
        self.send_midi( Message(action,**args) )
    #---------------------------------------------------------------------------
    def system_message(self,data=None,**args):
        #data : list ok
        #reste? pas sur
        self.send_midi( Message('sysex', data=data) )
    #---------------------------------------------------------------------------
    def meta_message(self,action=None,**args):
        "https://mido.readthedocs.io/en/latest/meta_message_types.html"
        self.send_midi( mido.MetaMessage(action,**args) )
    #---------------------------------------------------------------------------
    def send_midi(self,message):
        #print(message)
        self.send_to_pipe(data=message)#.copy())
    #---------------------------------------------------------------------------

#===============================================================================

class Midi_Port(Midi_Loop):

#===============================================================================
    PORT_ID=Integer()
    #---------------------------------------------------------------------------
    def checkMidi(self):
        return not self.port.closed
    #---------------------------------------------------------------------------
    def onStop(self):
        self.port.close()

    #---------------------------------------------------------------------------

#===============================================================================

class Midi_Input(Midi_Port):

#===============================================================================

    VIRTUAL=Boolean(default=True)

    #---------------------------------------------------------------------------
    def onSetup(self):
  
         self.port=mido.open_input(self.port_id,client_name=self.name, virtual=self.virtual)
         Midi_Port.onSetup(self)
    #---------------------------------------------------------------------------
    def onDoMidi(self):

        for msg in self.port.iter_pending():
            #print(msg)
            self.send_midi(msg)


    #---------------------------------------------------------------------------


#===============================================================================

class Midi_MultiInput(Midi_Input):

#===============================================================================



    #---------------------------------------------------------------------------
    def onSetup(self):


        inports=list()

        for elt in mido.get_input_names():
            print(elt)
            inports.append(mido.open_input(elt))
            Atom(parent=self,name=elt)
        self.port = MultiPort(inports)
        Midi_Port.onSetup(self)
    #---------------------------------------------------------------------------

#===============================================================================

class Midi_Recorder(Midi_Input):

#===============================================================================
    """
    There are three types of MIDI files:

        type 0 (single track): all messages are saved in one track
        type 1 (synchronous): all tracks start at the same time
        type 2 (asynchronous): each track is independent of the others

    When creating a new file, you can select type by passing the type keyword argument, or by setting the type attribute:
    """
    
    MIDI_TYPE=Integer(default=0)
    I=Integer(default=0)
    FILE_PATH=String()

    #---------------------------------------------------------------------------
    def onSetup(self):
        Midi_Input.onSetup(self)


        self.midi = MidiFile(type=self.midi_type)
        self.track = MidiTrack()
        self.midi.tracks.append(self.track)
    #---------------------------------------------------------------------------
    def onReceiveMessage(self,message):

        if isinstance(message.data,Message):
            #msg.time=self.i
            self.track.append(message.data)
        self.i+=1

    #---------------------------------------------------------------------------
    def onDoMidiX(self):

        for msg in self.port.iter_pending():

            msg.time=self.i
            print(msg)
            self.track.append(msg)
        self.i+=1

    #---------------------------------------------------------------------------
    def onStop(self):
        self.midi.save(self.file_path)

    #---------------------------------------------------------------------------


#===============================================================================

class Midi_Input(Midi_Port):

#===============================================================================

    PORT_ID=Integer()


    #---------------------------------------------------------------------------
    def onStart(self):
  
         self.port=mido.open_input(self.name,client_name=self.name,virtual=True)

    #---------------------------------------------------------------------------
    def onDo(self):
        for message in self.port:
            if self.is_running()==True:
                self.send_to_pipe(data=message)
        
    #---------------------------------------------------------------------------

#===============================================================================

class Midi_Output(Midi_Port):

#===============================================================================

    PORT_ID=Integer()


    #---------------------------------------------------------------------------
    def onStart(self):
  
         self.port=mido.open_output(self.name,client_name=self.name,virtual=True)

    #---------------------------------------------------------------------------
    def onReceiveMessage(self,message):
        if isinstance(message.data,mido.Message):
            self.port.send(message.data)
        else:
            self.send_to_pipe(message=message)

    #---------------------------------------------------------------------------
    def midi_reset(self):
        """
        This will send "all notes off" and "reset all controllers" on every channel. 
        This is used to reset everything to the default state, 
        for example after playing back a song or messing around with controllers.
        """
        self.port.reset()
    #---------------------------------------------------------------------------
    def midi_panic(self):
        """
        Sometimes notes hang because a note_off has not been sent. To (abruptly) 
        stop all sounding notes, you can call it
        """
        self.port.panic()

    #---------------------------------------------------------------------------
    def onStop(self):
        self.midi_panic()
        self.midi_reset()
    #---------------------------------------------------------------------------
#===============================================================================

class Midi_MultiOutput(Midi_Output):

#===============================================================================



    #---------------------------------------------------------------------------
    def onStart(self):

        inports=list()

        for elt in mido.get_output_names():
            #print(elt)
            inports.append(mido.open_output(elt))

        self.port = MultiPort(inports)
        #self.root.stop()
        #exit()
    #---------------------------------------------------------------------------


#===============================================================================

class Midi_Player(Midi_Loop):

#===============================================================================

    FILE_PATH=String()
    REPEAT=Boolean(default=False)

    #---------------------------------------------------------------------------
    def onStart(self):
  
        self.f = MidiFile(self.file_path)
    #---------------------------------------------------------------------------
    def onDoMidi(self):
  

        for msg in self.f.play():
            if self.is_running()==True:
                self.send_midi(msg)
            else:
                break

        if self.repeat!=True:
            self.stop()
        
    #---------------------------------------------------------------------------

#===============================================================================


