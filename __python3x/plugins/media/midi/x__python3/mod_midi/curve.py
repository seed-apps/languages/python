
from atom import Atom
from workflow import Action
from dataModel import *
import mido,time
from mido.ports import MultiPort
from IoNode import *
from mido import Message, MidiFile, MidiTrack

#===============================================================================

class Midi_Curve(IoNode):

#===============================================================================

    SCALE=Float(default=1.0)
    SELECT=String(default="/")

    SEND_CTL=Function()
    #---------------------------------------------------------------------------
    def onSetup(self):
  
        self.node=self.find(self.select)
        self.i=0
        self.curves=self.node.all().by_class("Curve")
        IoNode.onSetup(self)


    def send_ctl(self):
        self.send_to_pipe(data=Message('control_change',channel=0))

    #---------------------------------------------------------------------------
    def onDo(self):

        if self.i==0:
            #print(Message('control_change',channel=1))
            self.send_to_pipe(data=Message('control_change',channel=0))

        if self.i < len(self.curves):
            print("#"*50,self.i)
            data=self.curves[self.i].get_data()

            dt=data[1][0]-data[0][0]

            self.send_note(data=data[0][1])
            time.sleep(dt)
            j=1

            for elt in data[1:]:
                self.send_note(data=elt[1])
                dt=data[j][0]-data[j-1][0]
                time.sleep(dt)
                j+=1
            self.i+=1
        else:
            self.i=0


    def send_note(self,data=None):
        #print(self.scale*data)
        note=int(self.scale*data)#int(data*127)
        if note>127:note=127
        if note<0:note=0
        self.send_to_pipe(data=Message('note_on',note=note,velocity=127,channel=0))
    #---------------------------------------------------------------------------

#===============================================================================
