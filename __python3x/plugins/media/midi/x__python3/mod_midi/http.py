
from .server import *
from mido.sockets import PortServer, connect


#===============================================================================

class Midi_Http(Midi_Loop):

#===============================================================================
    #TODO:multiclient : https://mido.readthedocs.io/en/latest/socket_ports.html
    IP=String()
    PORT=Integer()

#===============================================================================

class Midi_Http_In(Midi_Http):

#===============================================================================
    def onStart(self):
        self.port=PortServer(self.ip, self.port)

    #---------------------------------------------------------------------------
    def onDoMidi(self):
        for message in self.port:
            self.send_to_pipe(message)

    #---------------------------------------------------------------------------
    def onStop(self):
        pass

    #---------------------------------------------------------------------------
#===============================================================================

class Midi_Http_Out(Midi_Http):

#===============================================================================
    def onStart(self):
        self.port=connect(self.ip, self.port)

    #---------------------------------------------------------------------------
    def onReceiveMessage(self,message):
        if isinstance(message,mido.Message):
            self.port.send(message)
        else:
            self.send_to_pipe(message)

    #---------------------------------------------------------------------------
    def onStop(self):
        pass

    #---------------------------------------------------------------------------





