


from mod_program import *
from dataModel import *
import mido
from pythonosc import osc_message_builder
from pythonosc import udp_client
from IoNode import IoNode

#===============================================================================

class Midi_Server(IoNode):

#===============================================================================

    URL=String()
    IP=String(default="localhost")
    PORT=Integer()
    
    
    #---------------------------------------------------------------------------
    def onSetup(self):
        self.client = udp_client.SimpleUDPClient(self.ip, self.port)

    #---------------------------------------------------------------------------        
    def send(self,path,message):
        self.client.send_message(path,str(message))


    #---------------------------------------------------------------------------
    def onCall(self):

        inports=list()

        for elt in mido.get_input_names():
            inports.append(mido.open_input(elt))

        from mido.ports import MultiPort

        multi = MultiPort(inports)
        for msg in multi:
            self.send(self.url,str(msg))
        
    #---------------------------------------------------------------------------

#===============================================================================
