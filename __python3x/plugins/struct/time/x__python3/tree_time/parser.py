from minimal.parsers import AttributeParser
from datetime import datetime

#==============================================================

class TimeParser(AttributeParser):

#==============================================================

    #precision possibles
    #---------------------------------------------------------

    CALENDAR=("year","month","day")
    DAY=("hour","minute","second")
    CHRONO=("hour","minute","second","microsecond")
    DAILY_CHRONO=("hour","minute","second")
    DATE_HOURS=("year","month","day","hour","minute","second")
    FULL=("year","month","day","hour","minute","second","microsecond")

    #---------------------------------------------------------

    def __init__(self,precision="FULL"):

        self.precision=getattr(self,precision)
        AttributeParser.__init__(self,parameters=self.precision)

    #---------------------------------------------------------

    def now(self):

        return datetime.now()

    #---------------------------------------------------------


#==============================================================

