from minimal.path import filterfiles 
import os
#===============================================================

class Apps_Node(Atom):
    
#===============================================================
    SEPARATOR=String(default="#")

    READ_INIT=Boolean(default=False)
    INIT_FILE=String(default="__init__.xml")

    FILE_PATH=String(default=None)
    APP_PATH=String(default=None)

    DEFAULT_CLASS="Apps_Node"
    CLS=String(default=None)
    CLS_OBJ=Field(default=None)

    OPEN_NAUTILUS=Function()



    #-------------------------------------------------------------
    def onSetup(self):

        #print("apps:",self.get_path(),"XX",self.separator,"XX")
        
        if self.cls is None:
            self.cls_obj=getClass(self.__class__.DEFAULT_CLASS)
        else:
            self.cls_obj=getClass(self.cls)
            
        if self.read_init == True and self.file_path is not None and self.file_path.startswith("@"):
            obj,path=path.split(":")
            self.file_path=self.query(obj,action="render",filename=path)
        elif self.file_path is None:
            pass#self.file_path=self.get_path()
            
        if self.file_path is None:
            pass
            #raise Exception("app no file_path",self.name,self.path())
        
    #----------------------------------------------------------------

    def open_nautilus(self):
        if self.file_path is not None:
            #print("nautilus %(file_path)s"%self)
            command= "nautilus %s"%self.file_path
            return Commands_Command(text=command).run()

    #-------------------------------------------------------------
        
    def exists(self):
        if self.file_path is not None:
            if os.path.exists(self.get_path()) == True:
                return True

    #-------------------------------------------------------------    
    def has_element(self,name):
        if self.exists()==True:
            return os.path.exists(self.get_path(name))

    #-------------------------------------------------------------
    def get_element(self,name):
        return self.get_path(name)

    #-------------------------------------------------------------    
    def elements(self,*ext):
        return filterfiles(self.get_path(),*ext)
    

    #-------------------------------------------------------------
    def get_path(self,*args):
        
        if "file_path" in self.keys():
            file_path=self.file_path
        else:
            file_path=self.parent.get_path()
        return os.path.join(file_path,*args)

    #-------------------------------------------------------------
#===============================================================

class Apps_Package(Apps_Node):
    
#===============================================================
    DEFAULT_CLASS="Apps_Package"

    #-------------------------------------------------------------

    def onSetup(self):
        Apps_Node.onSetup(self)
        

    #-------------------------------------------------------------

#===============================================================

class Apps_Store(Apps_Node):
    
#===============================================================

    #-------------------------------------------------------------
    def append_structure(self,name=None,file_path=None,**args):
        #print(self.separator,name)
        
        if self.separator in name:
            path=name.split(self.separator)
            name=path[-1]
            path=self.separator.join(path[:-1])
            parent=self.append(path,cls=self.cls_obj,separator=self.separator)
        else:
            parent=self
            
        filename=os.path.join(file_path,self.init_file)

        if  self.read_init == True and os.path.exists(filename)==True:
            print("open :",filename)
            return xmlscript.tree_from_file(filename,parent=parent,name=name,file_path=file_path,**args)
        else:
            print("node :",self.cls_obj.__name__)
            return self.cls_obj(parent=parent,name=name,file_path=file_path,**args)

    #-------------------------------------------------------------

