from mod_program import *
from dataModel import *
from .Apps_Node import *
from .Apps_Plugin import *
#===============================================================

class Apps_Plugins(Object):
    
#===============================================================
    """
    gerer un ensemble de plugins
    """
    SELECT=String()
    FILENAME=String()
    #CLS=dataModel.String(default="Apps_Plugin")


    #---------------------------------------------------

    def onSetup(self):
        self.on_update()
        #pass

    #---------------------------------------------------

    def on_update(self):
        #print(self.name,"destroy")
        #self.children.by_class(Apps_Plugin).execute("destroy")
        print(self.name,"on_update plugs")
        self.onUpdatePlugins()
        self.children.by_class(Apps_Plugin).execute("setup")
        print(self.name,"on_update apps")
        self.onUpdateApps()
        #self.tree()

    #---------------------------------------------------

    def onUpdatePlugins(self):
        for app in self.find(self.select).all().by_class(Apps_Node):
            #print(app.name,app.path(),app.file_path)
            if app.file_path and app.has_element(self.filename) == True:
                self.make_plugin(app.name,app.get_element(self.filename))

    #---------------------------------------------------

    def make_plugin(self,plugin_name,filename,**args):

        print(plugin_name,filename)
        node=self.append(plugin_name,cls=Apps_Plugin,file_path=filename)
        node.setup().parent=self
    #---------------------------------------------------

    def onUpdateApps(self):
        for plugin in self.all().by_class(Apps_Plugin):
            for app in self.find(self.select).all().by_class(Object):
                if plugin.test(app)==True:
                    plugin.make_elt(app)
    #---------------------------------------------------

    def plugin_tree(self,app):

        root=Atom()
        for elt in self.all().by_class(Apps_Filter):

            if elt.test(app)==True:

                root.append(elt.name,separator=".",node=elt,cls=Atom,setup=False)


        for elt in root.all():
            if hasattr(elt,"node") != True:
                elt.node="None"

        return root

    #---------------------------------------------------

    def iterator_tree(self,app):

        root=Atom()

        for elt in self.all().by_class("Iterator"):
            if elt.test(app)==True:
                root.append(elt.name,separator=".",node=elt,cls=Atom,setup=False)

        for elt in root.all():
            if hasattr(elt,"node") != True:
                elt.node="None"

        return root


    #---------------------------------------------------

#===============================================================
         

