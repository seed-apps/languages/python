
from .Apps_Plugins import *
from dataModel import *
#===============================================================

class Apps_StorePlugins(Apps_Plugins):
    
#===============================================================


    def onUpdatePlugins(self):

        for subelt in self.find(self.select).all().by_class("Apps_Node",strict=False):

            if subelt.file_path and subelt.has_element(self.filename)==True:
                path=os.path.join(subelt.file_path,self.filename)
                self.make_plugin(subelt.parent.name+"/"+subelt.name,path)



#===============================================================

class Apps_DirPlugins(Apps_Plugins):
    
#===============================================================

    FILE_PATH=String()

    def onUpdatePlugins(self):

        for path in glob.glob(self.file_path+'/*.xml'):
            name=path.split("/")[-1].replace(".xml","")
            self.make_plugin(name,path)

         

#===============================================================

