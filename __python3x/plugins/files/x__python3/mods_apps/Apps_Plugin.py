from atom import Atom,Atom_Link
from dataModel import *
from dataModelAddons.tree import Tree
import xmlscript,dataModel
import os

from mod_program import Module,Object,Iterator
import glob
from .Apps_Method import *
#==================================================================

class Apps_Link(Atom_Link):
    
#==========================================================

    def test(self):

        return self.source.test(self.target)

    def getHandlers(self):

        return self.source.getHandlers(self.target)

#===============================================================

class Apps_Plugin(Object):
    
#===============================================================

    FILE_PATH=String(default=None)

    def onSetup(self):
        self.code=None
        if self.file_path and os.path.exists(self.file_path)==True :
            self.code=xmlscript.tree_from_file(self.file_path,name="__data__",parent=self)

    def getAllHandlers(self):

        return self.code.children.by_class(Handler)

    def getHandlers(self,app):

        if self.code is not None:
            for elt in self.getAllHandlers():
                if elt.test(app) ==True:
                    yield elt

    def test(self,node):

        if self.code is not None:

            for elt in self.getHandlers(node):
                return True
            return

    def make_elt(self,node,**args):      
        Apps_Link(parent=self,source=self,target=node,name=node.path().replace("/","."))
    
#===============================================================
  

