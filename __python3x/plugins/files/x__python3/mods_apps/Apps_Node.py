from mod_program import *
from dataModel import *
from minimal.path import filterfiles,filterdirectories,FilePath
import xmlscript
from tree_files import Disc_Directory
#===============================================================

def iterApps(path):

#===============================================================
    """
    lists applications in a directory.
    return an iterator (name,path)
    """

    #print("iter_apps",path)
    path=os.path.realpath(path)
    for elt in os.listdir(path):
        if not elt.startswith("__") and  not elt.startswith("."):
            elt_path=os.path.join(path,elt)
            if os.path.isdir(elt_path):
                #print(elt,elt_path)
                yield elt,elt_path
      
#===============================================================

class Apps_Node(Disc_Directory):
    
#===============================================================
    """
    Explorateur de fichier
    """


    APP_PATH=String(default=None)

    #-------------------------------------------------------------
    def onOpen(self):

        self.onOpenFiles()

    #-------------------------------------------------------------
    def onOpenFiles(self):

        for elt in os.listdir(self.file_path):

            self.openFile(elt)

    #-------------------------------------------------------------
    def openFile(self,name):

        if not name.startswith("__") and self.has_element(name)==True:
            filepath=self.get_element(name)
            #print(filepath)
            if filepath.isfile() == True and filepath.extention()=="xml":

                print("openFile",filepath)
                node=xmlscript.tree_from_file(filepath.string,
                                    name=filepath.name(extention=False),
                                    parent=self)

                if isinstance(node,getClass("Code")) ==True and isinstance(node,getClass("Log")) !=True:
                    node.setup()

     

    #----------------------------------------------------------------

    def onOpenOld(self):

        for name,elt in iterApps(self.get_path()):
            print(name,elt)
            self.load_app(  name=name,
                                    file_path=elt,
                                    read_init=self.read_init)

    #----------------------------------------------------------------

    def onClose(self):
        self.children.by_class(Object).execute("close")

    #----------------------------------------------------------------

    def new_app(self,name=None):
        path=os.path.join(self.file_path,name)
        os.mkdir(path)
        self.cls_obj(parent=self,file_path=path,name=name)


    #-------------------------------------------------------------
    def get_pathX(self,*args):
        
        if "file_path" in self.keys():
            file_path=self.file_path
        else:
            file_path=self.parent.get_path()
        return os.path.join(file_path,*args)

    #-------------------------------------------------------------
#===============================================================

