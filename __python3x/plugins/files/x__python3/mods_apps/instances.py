import atom
from dataModelAddons.tree import Tree
import xmlscript
import os
from dataModel import *

#===============================================================

class Apps_PluginControl(atom.Atom_Link):
    
#===============================================================

    def onSetup(self):

        atom.Atom_Link.onSetup(self)

        
        for element in self.source.make_elt(self.target,name=self.name):
            element.setup() 
            obj=atom.Atom_Link(source=self,target=element,parent=self)
            obj.setup()

    def onDestroy(self):
        for element in self.children.by_class(atom.Atom_Link):
            element.targe.destroy()

#===============================================================

class Apps_InstanceGroup(atom.Atom_Link):
    
#===============================================================

    def onSetup(self):
        atom.Atom.onSetup(self)

    def on_update(self):
        #self.clear()
        for app in self.target.all().by_class("Apps_Node"):
            if self.source.test(app)==True:
                #yield element.setup() 
                obj=Apps_PluginControl(name=app.path().replace("/","."),source=self.source,target=app,parent=self)
                obj.setup()

                
#===============================================================

class Apps_Instances(atom.Atom_Link):
    
#===============================================================

    ON_UPDATE=Function()

    def onSetup(self):
        atom.Atom_Link.onSetup(self)
        self.on_update()

    def on_update(self):
        self.clear()
        for plugin in self.source.all().by_class("Apps_Filter"):
            if plugin.app_path is not None:
                print("plugin:",plugin.path())
                group=Apps_InstanceGroup(parent=self,
                            name=plugin.name,source=plugin,target=self.target)
                r=group.setup()
                r.parent=group
                r.name="setup"
#===============================================================
