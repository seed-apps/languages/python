

from mod_program import Node_Condition
from dataModel import *
from .Disc_File import Disc_File
from .Disc_Element import Disc_Element
from .Disc_Directory import Disc_Directory
import os
#============================================================

class File_Condition(Node_Condition):

#============================================================
    FILE_PATH=String(default="%(file_path)s")
    #-------------------------------------------------
    def get_path(self,node):
        return self.file_path%node
    #-------------------------------------------------
    def onTestNode(self,node):
        path=self.get_path(node)
        return self.onTestDisc(path)
    #-------------------------------------------------
    def onTestDisc(self,path):

        return True
      
    #-------------------------------------------------

#============================================================

class FileExists(File_Condition):

#============================================================

    #-------------------------------------------------
    def onTestDisc(self,path):

        return os.path.exists(path)

    #-------------------------------------------------

#============================================================

class IsFile(File_Condition):

#============================================================

    def onTestDisc(self,path):
        return os.path.isfile(path)

#============================================================

class IsDir(File_Condition):

#============================================================

    def onTestDisc(self,path):
        return os.path.isfile(path)

#============================================================

class HasFile(File_Condition):

#============================================================
    TEXT=String()
    def onTestDisc(self,path):
        if self.text:
            return os.path.exists(os.path.join(path,self.text))


#============================================================

class HasFileType(File_Condition):

#============================================================
    TEXT=Field(default=None)
    CLS=Disc_Directory


    def onSetup(self):
        if self.text is None:
            self.extentions=[]
        elif type(self.text) == str and "," in self.text:
            self.extentions=self.text.split(",")
        else:
            self.extentions=[self.text,]
        
    def onTestDisc(self,node):
        if node.exists() == True:
            if len(node.get_elements(*self.extentions))>0:
                return True
            

#============================================================

