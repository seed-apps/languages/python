import os,shutil
from .Disc_Element import Disc_Element
from mod_program import command,execute
from dataModel import *
import mimetypes
#========================================================================

class Disc_File(Disc_Element):

#========================================================================

    #TODO: separation binaire/text
 
    READ=Function()
    WRITE=Function()
    EDIT=Function()
    #----------------------------------------------------------------
    @command
    def edit(self):
        return "gedit %(file_path)s"%self

    #----------------------------------------------------------------
    def create(self):

        #self.not_exists_or_exception()
        if not self.getPath().root().exists():
            os.makedirs(self.getPath().root().string)
            self.write('')


    #----------------------------------------------------------------

    def remove(self):

        if self.file_path.exists():
            os.remove(self.getPath().string)

    #----------------------------------------------------------------

    def copy(self,destination):

        shutil.copy2(self.getPath().string,str(destination))

    #----------------------------------------------------------------

    def write(self,data):
        with open(self.getPath().string,"w") as f:
            f.write(data)

    #----------------------------------------------------------------

    def read(self):
        with open(self.getPath().string,"r") as f:
            result= f.read()
        return result

    #----------------------------------------------------------------

#========================================================================
