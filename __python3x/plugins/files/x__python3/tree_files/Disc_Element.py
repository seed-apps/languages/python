# -*- coding: utf-8 -*-
#http://latarteauchips.free.fr/linux-programmer-arret-systeme.php
import os,mimetypes,datetime


from mod_program import Object
from dataModel import *
from minimal.path import FilePath
import os

#========================================================================

class Disc_Element(Object):

#========================================================================
    """
    represente un élément d'un disque
    """

    FILE_PATH=String(default=None)

    CREATE=Function()
    REMOVE=Function()
    COPY=Function()
    INFOS=Function()
    MIMETYPE=Function()
    #----------------------------------------------------------------
    def onSetup(self):

        if self.is_ok()==True: 
            """if self.file_path.startswith("./")==True:
                if "file_path" in self.parent.keys():
                    self.file_path=os.path.join(self.parent.file_path,self.file_path)
                else:
                    return
"""
            infos=self.infos()

            self.ctime = datetime.datetime.fromtimestamp( infos["ctime"] )
            self.mtime = datetime.datetime.fromtimestamp( infos["mtime"] )
            self.atime = datetime.datetime.fromtimestamp( infos["atime"] )

    #----------------------------------------------------------------
    def is_ok(self):
        if "file_path" in self.keys() and self.file_path is not None:
            return os.path.exists(self.file_path)
        return False
    #----------------------------------------------------------------
    def getPath(self):
        return FilePath(self.file_path)

    #----------------------------------------------------------------
    def getRelative(self,node=None,root=False):
        if not node:
            node=self.getRootPath()
        if node is None:
            if root==True:return "/"+self.file_path.split("/")[-1]
            return ""
        #print(self.file_path,node.file_path)
        if root==True:
            return  "/"+node.file_path.split("/")[-1]+self.file_path.replace(node.file_path,"")
        else:
            return  self.file_path.replace(node.file_path,"")
    #----------------------------------------------------------------
    def getRootPath(self):
        node=None
        for elt in self.ancestors:
            if isinstance(elt,Disc_Element):
                node= elt
            else:
                break

        return node
      
    #----------------------------------------------------------------
    def not_exists_or_exception(self):

        if not self.file_path.exists():
            raise Exception("path already exists"+self.file_path)
    #-------------------------------------------------------------
        
    def exists(self):
        if self.file_path is not None:
            return os.path.exists(self.get_path())
        return False

    #----------------------------------------------------------------
    def create(self):pass
    def remove(self):pass
    def copy(self,destination):pass

    #----------------------------------------------------------------

    def mimetype(self):
        if self.file_path is not None:
            return mimetypes.guess_type( self.file_path, strict=False)[0]

    #----------------------------------------------------------------
    def is_dir(self):
        return os.path.isdir(self.file_path)
    #----------------------------------------------------------------
    def is_file(self):
        return os.path.isfile(self.file_path)


    #----------------------------------------------------------------

    def is_image(self):
        t=self.getPath().extention()
        if t in ["jpg","jpeg","JPG","JPEG","gif","GIF","png","PNG","svg"]:
            return True
        return False
    #----------------------------------------------------------------

    def is_video(self):
        t=self.getPath().extention()
        if t in ["avi","AVI","mp4","mkv"]:
            return True
        return False
    #----------------------------------------------------------------

    def is_audio(self):
        t=self.getPath().extention()
        if t in ["wav","WAV","mp3"]:
            return True
        return False
    #----------------------------------------------------------------

    def infos(self):
        """
        renvoie les infos de la fonction os.stat


        - st_mode - protection bits,
        - st_ino - inode number,
        - st_dev - device,
        - st_nlink - number of hard links,
        - st_uid - user id of owner,
        - st_gid - group id of owner,
        - st_size - size of file, in bytes,
        - st_atime - time of most recent access,
        - st_mtime - time of most recent content modification,
        - st_ctime - platform dependent; time of most recent metadata change on Unix, or the time of creation on Windows)


        https://docs.python.org/2/library/os.html
        """

        headers=("mode", "ino", "dev", "nlink", "uid", "gid", "size", "atime", "mtime", "ctime")
        infos=os.stat(self.file_path)
        infos=dict(zip(headers,infos))
        infos["path"]=self.file_path
        return infos

    #----------------------------------------------------------------
    def join(self,*args):
        
        return os.path.join(self.file_path,*args)

    #-------------------------------------------------------------    
    def has_element(self,*args):
        return os.path.exists(self.join(*args))

    #-------------------------------------------------------------
    def get_element(self,*args):
        return self.join(*args)
    #----------------------------------------------------------------

#========================================================================

