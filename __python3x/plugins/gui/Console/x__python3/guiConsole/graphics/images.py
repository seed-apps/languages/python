from .areas import Console_Square
from dataModel import *
#========================================================================

class Console_Image(Console_Square):

#========================================================================

    FILE_PATH=String()

    def onSetup(self,**args):
        f=open(self.file_path,"r")
        self.lines=f.read().split("\n")
        f.close()
        return Console_Square.onSetup(self)

    def fill_matrix(self,matrix,**args):
        i=0
        for line in self.lines:
            matrix.setStringLine(i,line)
            i+=1
#========================================================================
