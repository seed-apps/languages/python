import os,time

from ..node import Console_Element
from ..matrix import TermRender_Matrix
from subprocess import call
from dataModel import Field,String,Integer,Float,Boolean



#========================================================================

class Console_Square(Console_Element):

#========================================================================

    CHAR=String(default=" ")
    BORDER=String(default=None)

    X=Integer(default=0)
    Y=Integer(default=0)
    LX=Integer(default=0)
    LY=Integer(default=0)

    def onRender(self,**args):
        return self.get_matrix().getLines()

    def get_matrix(self,**args):

        matrix=TermRender_Matrix((self.lx,self.ly),default=self.char)

        self.fill_matrix(matrix,**args)

        if self.border is not None:
            matrix.borders(self.border)

        return matrix

    def fill_matrix(self,matrix,**args):


        for area in self.children.by_class(Console_Square):
            sub_matrix=area.get_matrix()
            matrix.add_matrix(sub_matrix,(area.x,area.y))

        if self.border is None:
            i=0
        else:
            i=1
        for child in self.children.by_class(Console_Element):
            if not isinstance(child,Console_Square):
                for line in child.render():
                    matrix.setStringLine(i,line)
                    i+=1

#========================================================================


