from IoNode import IoNode
from atom import treelogs
from ..graphics import Console_Fullscreen,Console_Text,Console_Square,Console_Image
from dataModel import *
import time,os
from .model import Console_Screen

#===================================================================

class Console_IONode(Console_Screen):

#===================================================================

    I=Integer(default=0)    
    OBJS=List()

    #----------------------------------------------------------------
    def onDoLoop(self):


        for elt in self.objs:
            elt.destroy()
        del self.objs

        self.objs=list()

        self.i=0
        self.running=0


        for node in self.root.all().by_class(IoNode):
            self.addLine(node)
                
        #os.system("clear")
        #self.screen.tree()

        #print( self.running)
        if self.running==0:
            self.stop()

        if self.root.is_running()==True:
            self.set_status("... running %i threads ..."%self.running)
        else:
            self.set_status("... to be continued ...")
        os.system("clear")
        self.screen.render()
        time.sleep(self.dt)
    #----------------------------------------------------------------
    def onStop(self):
        self.onDo()


    #----------------------------------------------------------------
    def addLine(self,node):

        if node == self:
            return

        self.i+=1

        x=10+self.i*1
        y=10
        lx=1
        ly=int(50/4)

        elt=Console_Text(parent=self.screen,text="   * "+node.path(),x=x,y=y,lx=lx,ly=3*ly)
        self.objs.append(elt)
        if node.is_running()==True:
            elt=Console_Text(parent=self.screen,text="0",x=x,y=y+3*ly,lx=lx,ly=ly)
            self.running+=1
        else:
            elt=Console_Text(parent=self.screen,text="X",x=x,y=y+3*ly,lx=lx,ly=ly)
        self.objs.append(elt)
    #----------------------------------------------------------------

#===================================================================
