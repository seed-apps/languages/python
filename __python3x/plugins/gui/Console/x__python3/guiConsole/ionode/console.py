# -*- coding: utf-8 -*-


from ..graphics import Console_Fullscreen,Console_Text,Console_Square,Console_Image
from dataModel import *
from .model import Console_Screen

#===================================================================

class Console_Dialog(Console_Screen):

#===================================================================

    PROMPT=String(default="")

    def onDoLoop(self):

        self.screen.render()

        command=input(self.prompt)
        self.set_status("command -> "+command)
        if command in ["q"]:
            self.root.stop()

        #elif command not in [None,""]:
        #    self.query(comman)

    #----------------------------------------------------------------
    def onStop(self):
        self.set_status("... terminated ...")
        #self.screen.render()




#===================================================================
