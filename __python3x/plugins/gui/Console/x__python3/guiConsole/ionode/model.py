from dataModel import *
from mod_program import *
from IoNode import *
from atom import treelogs
from ..graphics import *
import time,os
#===================================================================

class Console_Add(Action,IoNode_Interface):

#===================================================================

    #----------------------------------------------------------------

    def onNextCall(self,node):
        parent=self.get_ionode()
        parent.msgs.add_element(Console_Text(text=str(node.text).replace("\n",""),lx=1))

#===================================================================

class Console_Screen(IoNode):

#===================================================================

    TITLE=String(default="")
    LOGO=String()
    DT=Float(default=0.10)
    MAXMSG=Integer(default=15)

    #----------------------------------------------------------------

    def onStart(self):

        self.screen=Console_Fullscreen( char=" ",border=".")
        if self.logo is not None:
            Console_Image( parent=self.screen,file_path=self.logo,x=5,y=5, lx=3, ly=8)
        Console_Text(parent=self.screen,text=" "*5+self.title,x=5,y=15, lx=3, ly=50,border=".",upper=True)

        self.status=Console_Text(parent=self.screen,x=1,y=3,lx=3,ly=50)
        self.set_status()

        self.msgs=Console_VScroll(parent=self.screen,maxmsg=self.maxmsg,border=".",x=5,y=70,lx=25,ly=100)
        return self.screen.setup()


    #----------------------------------------------------------------
    def onDo(self):
        self.screen.render()
        time.sleep(self.dt)

    #----------------------------------------------------------------

    def onDoChildrenX(self): 
        pass
    #----------------------------------------------------------------

    def set_status(self,status=""):
        self.status.text="status : "+status

    #----------------------------------------------------------------

#===================================================================
