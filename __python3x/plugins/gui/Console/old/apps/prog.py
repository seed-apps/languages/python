from console.screen.nodes import Screen
from tree import NodeScreen

#from core.program.execution.action import RunProgram
#==========================================================================

class DataViewer(Screen):

#==========================================================================

    #----------------------------------------------------------------
    def onShow(self,node=None,msg=None,uri=None,**args):

        loader=DataLoad(uri=uri,parent=self)
        loader.run(parent=msg)
        loader.node.name=uri
        self.select(loader.node)


        console=NodeScreen(name="nodes",select=self.node)
        msg=console.run(parent=msg)
        loader.node.destroy()
        return False

    #----------------------------------------------------------------
#==========================================================================



