from console.screen.nodes import Screen,ScreenSelector
from vie import settings,Element
from tree import ChildrenScreen,NodeScreen
from pynetwork.core.tree import TreeNode
import tkFileDialog,os,sys
from console.document.text import Text,Title
from console.document.part import DocumentSection

#==========================================================

class FullTree(Screen):

#==========================================================
    RefClass=Element

    FrameElement=Text

    #-------------------------------------------------------------

    def onRender(self,selected=None,area=None,result=None,**args):
        selected.root.show()
#==========================================================

class HelpFrame(Screen):

#==========================================================
    RefClass=Element

    FrameElement=Text

        
    #-------------------------------------------------------------

    def onRender(self,selected=None,area=None,**args):

        Title(parent=area,text=selected.name)
        root=DocumentSection(parent=area)
        for elt in settings.actions.getOptions(selected.__class__):
            
            Title(parent=root,text=elt.name)
            for child in elt.children:

                Text( text=child.name,parent=root)
            
    #-------------------------------------------------------------

    
#===============================================================================
        
class OpenNode(Screen):

#===============================================================================

    RefClass=Element

    #---------------------------------------------------------------------
    def onRender(self,selected=None,**args):

        filename=tkFileDialog.askopenfilename(initialdir=os.environ["VIE_ROOT"])
            
        if filename:
        
            script=settings.openFile(filename)
            script.update(filename=filename)
            self.select(script)
            script.reparentTo(selected)
            
#==========================================================================

class ClassActionSelector(Screen):

#==========================================================================

    def onShow(self,node=None,**args):

        lst=['quit','classes','actions',]

        
        name=self.menu.menu(lst)

        if name in ["quit",None]:
            return False

        elif name=="classes":
            screen=ChildrenScreen(select=settings.libs,parent=self)
            screen()
            node=screen.selected
            
        elif name=="actions":
            
            if "Class" in node.data.keys():
                cls=node.data["Class"]
            else:
                cls=node.__class__
                
            tree=TreeNode()
            for opt in settings.actions.getOptions(cls):
                opt.copy(parent=tree)
                
            #TreeScreen(select=tree)()
            screen=NodeScreen(select=tree,parent=self)
            screen()
            actionNode=screen.selected

            if node is not None and actionNode is not None:
                action=actionNode.data["Class"]()
                action.select(node)
                node=action()
            
        self.select(screen.selected)

        self.menu.strtitle=self.selected.path()
        screen.destroy()
        return True



