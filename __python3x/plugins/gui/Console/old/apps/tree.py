from console.screen.nodes import Screen,ScreenSelector
from vie import settings
from pytree import NamedTreeNode

#==========================================================================
class ActionScreen(Screen):

#==========================================================================

    RefClass=NamedTreeNode


    def onShow(self,node=None,**args):
        menu=settings.system.actions.getMenu(node.__class__)
        menu.show(data=True,**args)
        self.menu.enter_string("")
        return False

#==========================================================================
class TreeScreen(Screen):

#==========================================================================

    RefClass=NamedTreeNode


    def onShow(self,node=None,**args):
        #node.show(data=False,**args)
        settings.console.tree(node,data=False)
        print node.size()
        self.menu.enter_string("")
        return False

#==========================================================================

class DataScreen(Screen):

#==========================================================================

    RefClass=NamedTreeNode

    def onShow(self,node=None,**args):
        self.menu.dicScreen(node.data)
        self.menu.enter_string("")
        return False

#==========================================================================

class StatScreen(Screen):

#==========================================================================

    RefClass=NamedTreeNode

    def onShow(self,node=None,**args):

        results=dict()

        results['nodes']=node.size()
        for cls,lst in node.by_classes():
            results[cls.__name__]=len(lst)
        self.menu.dicScreen(results)
        self.menu.enter_string("")
        return False
#==========================================================================

class ChildrenScreen(Screen):

#==========================================================================

    RefClass=NamedTreeNode

    def onShow(self,node=None,**args):
        
        lst=['quit','..','.','/']
        lst.extend(node.keys())
        

        name=self.menu.menu(lst)

        if name in ["quit",None]:
            return False
        
        elif name=="..":
            if node.parent:
                self.select(node.parent)
    
        elif name==".":
            pass

        elif name=="/":
            self.select(node.root)


        elif name in node.keys():
            self.select(node.find(name))
            

        return True
#==========================================================================

class NodeScreen(Screen):

#==========================================================================

    RefClass=NamedTreeNode

    def onShow(self,node=None,**args):

        settings.console.title(node.path())
        settings.console.tree(node,ident=0,limit=2)

        lst=['quit','data','actions','nav','tree','fulltree','stats']

        settings.console.line()
        self.menu.numbered_list(lst)
        settings.console.line()

        name=self.menu.int_choice(":",lst)


        if name in ["quit",None]:
            return False

        if name=="data":
            screen=DataScreen(parent=self)

        elif name=="actions":
            screen=ActionScreen(parent=self)

        elif name=="nav":
            screen=ChildrenScreen(parent=self)

        elif name=="tree":
            screen=TreeScreen(parent=self,limit=3)
            
        elif name=="fulltree":
            screen=TreeScreen(parent=self,limit=None)
            
        elif name=="stats":
            screen=StatScreen(parent=self)


        screen.select(node)
        screen()
        self.select(screen.selected)
        self.menu.strtitle=self.selected.path()
        screen.destroy()
        return True



