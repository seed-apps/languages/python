from console.screen.nodes import Screen,ScreenSelector
from pynetwork import TreeNode

    
#==========================================================================
class NetworkScreen(Screen):

#==========================================================================

    RefClass=TreeNode


    def onShow(self,node=None,**args):
        node.show(data=False,**args)
        print node.size()
        self.menu.enter_string("")
        return False

#==========================================================================

class LinkDataScreen(Screen):

#==========================================================================

    RefClass=TreeNode

    def onShow(self,node=None,**args):
        self.menu.dicScreen(node.data)
        self.menu.enter_string("")
        return False

#==========================================================================

class LinkStatScreen(Screen):

#==========================================================================

    RefClass=TreeNode

    def onShow(self,node=None,**args):

        results=dict()

        results['nodes']=node.size()
        for cls,lst in node.by_classes():
            results[cls.__name__]=len(lst)
        self.menu.dicScreen(results)
        self.menu.enter_string("")
        return False
#==========================================================================

class LinkScreen(Screen):

#==========================================================================

    RefClass=TreeNode

    def onShow(self,node=None,**args):
        
        lst=['quit','..','.','/']
        lst.extend(node.keys())
        
        self.menu.strtitle=self.selected.path()

        name=self.menu.menu(lst)

        if name in ["quit",None]:
            return False
        
        elif name=="..":
            if node.parent:
                self.select(node.parent)
    
        elif name==".":
            pass

        elif name=="/":
            self.select(node.root)


        elif name in node.keys():
            self.select(node.find(name))
            

        return True
#==========================================================================

class NetworkNodeScreen(Screen):

#==========================================================================

    RefClass=TreeNode

    def onShow(self,node=None,**args):

        lst=['quit','data','nav','tree','fulltree','stats']

        
        name=self.menu.menu(lst)

        if name in ["quit",None]:
            return False

        if name=="data":
            screen=DataScreen(parent=self)

        elif name=="nav":
            screen=ChildrenScreen(parent=self)

        elif name=="tree":
            screen=TreeScreen(parent=self,limit=3)
            
        elif name=="fulltree":
            screen=TreeScreen(parent=self,limit=None)
            
        elif name=="stats":
            screen=StatScreen(parent=self)


        screen.select(node)
        screen()
        self.select(screen.selected)
        self.menu.strtitle=self.selected.path()
        screen.destroy()
        return True



