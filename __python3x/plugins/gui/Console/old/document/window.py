

from console import RenderNode



#=====================================================================

class Window(RenderNode):

#=====================================================================

    def onRender(self,**args):
        
        yield self.separator()
        yield self.onFormat("")
        yield self.onFormat(self.name.upper())
        yield self.onFormat("")
        yield self.separator()
        yield self.onFormat("")
        
        for subwindow in self.by_class(Window,direct=True):
            for line in subwindow.render(): 
                yield self.onFormat(line)
                
        yield self.onFormat("")
        yield self.separator()              
        
    def separator(self):
        return " "+"_"*(self.console_size()-2)+" "
              
    def onFormat(self,string):
        
        space=self.console_space()
        size=self.console_size()
        separator=" "*space
        
        if len(string)<size:
            string=string+" "*(self.console_size()-len(string))
        elif len(string)>size:
            string=string[size]
            
        return "|"+separator[1:]+string+separator[1:]+"|"
                        
    def console_level(self):
        return self.parent.console_level()+1

    def console_size(self):

        return self.parent.console_size()-2*self.console_space()

    def console_space(self):
        return self.parent.console_space()

#=====================================================================



        