

from console import RenderNode



#=====================================================================

class DocumentSection(RenderNode):

#=====================================================================
    """
    increase level for children
    no render
    """

    def console_level(self):
        return self.parent.console_level()+1

    def console_size(self):

        return self.parent.console_size()-self.console_space()

    def console_space(self):
        return self.parent.console_space()*self.console_level()

#=====================================================================

