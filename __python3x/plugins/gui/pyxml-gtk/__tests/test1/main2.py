#!/usr/bin/env python3
# coding: utf-8
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk
from gi.repository.WebKit2 import WebView, Settings




def when_button_is_clicked(button):
    '''
    Quand le bouton est cliqué
    '''
    button.set_label('Hello world!')
    
def on_play(button):
    print("play")
    
def on_stop(button):
    print("stop")
    
def on_pause(button):
    print("pause")
    

builder = Gtk.Builder()

builder.add_from_file('./main2.glade')  # Rentrez évidemment votre fichier, pas le miens!
window = builder.get_object('main_window')
window.set_title("blackboard")
browser = builder.get_object('web-browser')
browser.load_uri("http://localhost")
# Peut se faire dans Glade mais je préfère le faire ici, à vous de voir
window.connect('delete-event', Gtk.main_quit)


tree_node = builder.get_object('tree')
tree=Gtk.TreeStore(str)
for i in range(5):
    node=tree.append(None,("toto"+str(i),))
    for j in range(4):
        tree.append(node,("toto"+str(j),))
        
tree_node.set_model(tree)

for i, column_title in enumerate(["name",]):
    renderer = Gtk.CellRendererText()
    column = Gtk.TreeViewColumn(column_title, renderer, text=i)
    column.set_sort_column_id(i)
    tree_node.append_column(column)


select=tree_node.get_selection()

def on_select(selection):
    print("pause",selection)
    model, treeiter = select.get_selected()
    if treeiter is not None:
        print("You selected", model[treeiter][0])

select.connect("changed", on_select)


handler = {'on_clicked': when_button_is_clicked,
            'on_play': on_play,
            'on_stop': on_stop,
            'on_pause': on_pause,
            }
builder.connect_signals(handler)

window.show_all()
Gtk.main()





