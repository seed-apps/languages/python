#!/usr/bin/env python3
# coding: utf-8
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def when_button_is_clicked(button):
    '''
    Quand le bouton est cliqué
    '''
    button.set_label('Hello world!')


builder = Gtk.Builder()
print("ok")
builder.add_from_file('./main.glade')  # Rentrez évidemment votre fichier, pas le miens!
print("ok")
window = builder.get_object('main_window')

# Peut se faire dans Glade mais je préfère le faire ici, à vous de voir
window.connect('delete-event', Gtk.main_quit)

# Le handler
handler = {'on_clicked': when_button_is_clicked}
builder.connect_signals(handler)

window.show_all()
Gtk.main()

