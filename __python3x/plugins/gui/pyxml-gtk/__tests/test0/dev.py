# APT gir1.2-gudev-1.0
import gi
gi.require_version('GUdev', '1.0')
from gi.repository import GUdev,Gtk
from gi.repository.GdkPixbuf import Pixbuf

#help(gi.repository)
#exit()

from mod_program import *
from dataModel import *



class Gtk_Widget(Object):

    CLASS=None
    TERMINAL=False
    NODE=Field()
    
    def onSetup(self):
        self.node=self.onBuild(self.__class__.CLASS)
        
    def onPostSetup(self):  
        if self.__class__.TERMINAL !=True:
            for elt in self.children.by_class(Gtk_Widget):
                if elt.node:
                    self.node.add(elt.node)
            self.node.show_all()
        
    def onBuild(self,cls):
        return
        
    def onCleanup(self):
        pass
        

  
class Gtk_Window(Gtk_Widget):

    CLASS=Gtk.Window  
    TITLE=String()
              
    def onBuild(self,cls):
        node= cls(title=self.title)
        node.set_default_size(self.width,self.height)
        return node
        

class Gtk_VBox(Gtk_Widget):

    CLASS=Gtk.VBox  
              
    def onBuild(self,cls):
        return cls()
      

class Gtk_HBox(Gtk_Widget):

    CLASS=Gtk.HBox  
              
    def onBuild(self,cls):
        return cls()

class Gtk_Label(Gtk_Widget):

    CLASS=Gtk.Label 
    TERMINAL=True 
    LABEL=String()
    MARGIN=Integer()
                  
    def onBuild(self,cls):
        return cls(label=self.label, margin=self.margin)




    
   
class Image_Container(Object):

    
    def get_data(self):
        return 
    
     
class Icon(Image_Container):

    ICON=String()
    LABEL=String(default=None)
    
    def get_data(self):
        return Gtk.IconTheme.get_default().load_icon(self.icon, 64, 0)
    
class Gtk_IconBar(Gtk_Widget):

    CLASS=Gtk.IconView  
    TERMINAL=True 
    ICON=String()
    LABEL=String(default="")
    
    def onBuild(self,cls):
        lst = Gtk.ListStore(Pixbuf,str)
        node=cls(model=lst)
        node.set_pixbuf_column(0)
        node.set_text_column(1)

        for elt in self.children.by_class(Icon):
            lst.append(elt.get_icon_data())
        return node
        

class Gtk_Icon(Gtk_Widget):

    CLASS=Gtk.IconView  
    TERMINAL=True 
    ICON=String()
    LABEL=String(default="")
    
    def onBuild(self,cls):
        lst = Gtk.ListStore(Pixbuf,str)
        node=cls(model=lst)
        node.set_pixbuf_column(0)
        node.set_text_column(1)
        elt= Gtk.IconTheme.get_default().load_icon(self.icon, 64, 0)
        print(elt)
        lst.append([elt,self.label])
        return node
       

          

def make_Disks():
    store = Gtk.ListStore(str, int, str,str,str,str,str,str)
    c = GUdev.Client() 
    for dev in c.query_by_subsystem("block"):

        #get_device_file_symlinks 'get_sysfs_path', 'get_tags'
        treeiter = store.append( [dev.get_name(),
                                    dev.get_device_number(),
                                    dev.get_devtype(),
                                    dev.get_device_file(),
                                    dev.get_driver() ,
                                    str(dev.get_tags()),
                                    str(dev.get_device_file_symlinks()[1]),
                                    str(dev.get_device_file_symlinks())
                                    ]   )
        #print( dev.get_property_keys() )
        #print( dir(dev))

    tree = Gtk.TreeView(model=store)
    renderer = Gtk.CellRendererText()
    column = Gtk.TreeViewColumn("name", renderer, text=0, weight=1)
    tree.append_column(column)

    renderer = Gtk.CellRendererText()
    column = Gtk.TreeViewColumn("id", renderer, text=1, weight=1)
    tree.append_column(column)

    column = Gtk.TreeViewColumn("type", renderer, text=2, weight=1)
    tree.append_column(column)

    column = Gtk.TreeViewColumn("path", renderer, text=3, weight=1)
    tree.append_column(column)

    column = Gtk.TreeViewColumn("driver", renderer, text=4, weight=1)
    tree.append_column(column)

    column = Gtk.TreeViewColumn("tags", renderer, text=5, weight=1)
    tree.append_column(column)

    column = Gtk.TreeViewColumn("fs", renderer, text=6, weight=1)
    tree.append_column(column)

    return tree
    

window = Gtk_Window(title="test",width=400,height=600)
box = Gtk_VBox(parent=window)
Gtk_Label(parent=box,label="welcome",margin=30)
iconbar=Gtk_IconBar(parent=box)

c = GUdev.Client() 
for dev in c.query_by_subsystem("block"):
    #data=Gtk_HBox(parent=box)
    Icon(parent=iconbar,icon="media-optical-symbolic",label=dev.get_name())
    #Gtk_Label(parent=data,label=dev.get_name(),margin=0)
    
#tree=make_Disks()
window.setup()
#box.node.add(tree)
#tree.show()

window.node.present()
#window.show()

Gtk.main()
