

import os,sys

from settings import settings


if __name__ == "__main__":
    from app import Application
    app = Application()
    app.run(sys.argv)
