

from  mod_program import Code,Object
from dataModel import *
from xmlscript import tree_from_file

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gio
from .base import *
from .elements import *

#====================================================

class Black_Class(Object):

#====================================================


    CLS=String()

    #-------------------------------------------------
    def get_list(self):
        yield self.cls

    #-------------------------------------------------
    def get_item(self,**args):
        "make interface"
        return getClass(self.cls)(**args)

    #-------------------------------------------------

#====================================================

class Black_Properties(Black_Item):

#====================================================



    #-------------------------------------------------
    def onPreMake(self,data):
        if self.gtk_object is None:
            self.gtk_object = Gtk.VBox()
        self.clear()

        #Black_Label(parent=self,text="properties")

        root=Black_List(parent=self)
        for k,v in data.getRecordableDict():
            child=Black_Container(parent=root,h=True,name=k,position="start",expand=True)
            Black_Label(parent=child,text=k,position="start",expand=True)
            Black_Entry(parent=child,text=str(v),position="end")

#====================================================

class Black_Editor(Black_Item):

#====================================================
    FILENAME=String(default="./__init__.xml")
    W=Integer(default="900")
    H=Integer(default="900")
    #-------------------------------------------------        
    def run(self):
        self.setup()
        self.data=self.onLoad()
        self.make(self.data)
    
    #-------------------------------------------------        
    def onPreMake(self,data):
        self.gtk_object = Gtk.Window()
        self.gtk_object.set_default_size(self.w,self.h)
        self.gtk_object.set_border_width(10)
        self.gtk_object.connect("delete-event", Gtk.main_quit)
        
    #-------------------------------------------------
        
    def onPostMake(self,data):
        
        self.gtk_object.show_all()
        Gtk.main()
        
    #-------------------------------------------------                
    def get_list(self):
    
        lst=[]
        for elt in self.children.by_class(Black_Class):
            for elt2 in elt.get_list():
                lst.append(elt2)
        return lst
        
    #-------------------------------------------------
    def onLoad(self):
        node = tree_from_file(self.filename,parent=self)
        node.setup()
        return node
        
    #-------------------------------------------------        
    def onSave(self):
        self.data
    #-------------------------------------------------        
    def add(self,node):
        if isinstance(node,Black_HeaderBar):
            pass            
        else:
            Black_Item.add(self,node)
            
    #-------------------------------------------------
    
#====================================================


