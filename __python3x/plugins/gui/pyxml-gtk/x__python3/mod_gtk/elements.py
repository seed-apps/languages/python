from  mod_program import Code,Object
from dataModel import *
from xmlscript import tree_from_file

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk,Gio
from gi.repository.WebKit2 import WebView, Settings
from .base import *


#====================================================

class Black_ScrollV(Black_Item):

#====================================================

    H=Boolean(default=False)

    #-------------------------------------------------
    def onPreMake(self,data):
        self.gtk_object = Gtk.ScrolledWindow()
        self.fill=True
        self.expand=True
    #-------------------------------------------------
    def add(self,node):
        self.gtk_object.add_with_viewport(node.gtk_object)

#====================================================

class Black_Label(Black_Item):

#====================================================

    TEXT=String()

    #-------------------------------------------------
    def onPreMake(self,data):
        self.gtk_object = Gtk.Label(self.text)

    #-------------------------------------------------
#====================================================

class Black_Entry(Black_Item):

#====================================================

    #icon_name = "system-search-symbolic"
    ICON=String()
    TEXT=String()

    #-------------------------------------------------
    def onPreMake(self,data):

        self.gtk_object = Gtk.Entry()
        if self.text is not None:
            self.gtk_object.set_text(self.text)
        
        if self.icon is not None:
            self.gtk_object.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, self.icon)        

    #-------------------------------------------------


#====================================================
class ListBoxRowWithData(Gtk.ListBoxRow):
    def __init__(self, data):
        super().__init__()
        self.data = data
        #self.add(Gtk.Label(label=data))
#====================================================

class Black_List(Black_Container):

#====================================================
    SHOW_ON=String(default="/")
    #-------------------------------------------------
    def onPreMake(self,data):

        self.gtk_object = Gtk.ListBox()
        self.win=data.find(self.show_on)
        
    #-------------------------------------------------
    def onPostMake(self,data):
        self.gtk_object.connect("row-activated",self.onRowActivated)
        
    #-------------------------------------------------        
    def add(self,node):
        if isinstance(node,Black_Label):
            group=ListBoxRowWithData(node)
        else:
            group=Gtk.ListBoxRow()
        group.add(node.gtk_object)
        self.gtk_object.add(group)
    #-------------------------------------------------
    def onRowActivated(self,lst,row,*args): 
    
        if isinstance(row,ListBoxRowWithData):
            #print(args)       
            print("click on","/"+row.data.node.path())    
            self.win.make(row.data.node) 
            self.root.gtk_object.show_all()               
    #-------------------------------------------------

#====================================================

class Black_Button(Black_Item):

#====================================================
    TEXT=String(default=None)
    ICON=String(default=None)

    
    #-------------------------------------------------
    def onPreMake(self,data):

        
        if self.icon is not None:
            self.gtk_object = Gtk.Button()
            icon = Gio.ThemedIcon(name=self.icon)
            image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
            self.gtk_object.add(image)
        else:
            self.gtk_object = Gtk.Button(self.text)
    #-------------------------------------------------        
#====================================================

class Black_ToggleBar(Black_Item):

#====================================================

    SELECT=String()

    #-------------------------------------------------
    def make(self,data):
        self.gtk_object = Gtk.Box(spacing=6)
        self.datatree=data
        self.buttons=list()
        self.content=None
        
        for elt in self.children.by_class(Black_Item):
            button = Gtk.ToggleButton(label=elt.name)
            self.buttons.append(button)
            button.connect("toggled", self.onSelect, elt.name)
            self.gtk_object.pack_start(button, False, False, 0)

        self.parent.add(self)

    #-------------------------------------------------        
    def onSelect(self, button, name,*args):

        if button.get_active():
        
            if self.content is not None:
                self.content.destroy()
        
            print("select",name)
            for elt in self.buttons:
                if elt != button:
                    elt.set_active(False)

            button.set_active(True)
            content=self.find(self.select)
            data=self.find(name)
            data.make(self.datatree)
            content.add(data)
            content.gtk_object.show_all()
            self.content=data.gtk_object
        else:
            print("unselect",name)  
    #-------------------------------------------------        

      
#====================================================

class Black_HeaderBar(Black_List):

#====================================================

    #-------------------------------------------------
    def onPreMake(self,data):
        self.gtk_object = Gtk.HeaderBar()
        self.gtk_object.set_show_close_button(True)
        self.gtk_object.props.title = self.root.filename

    #-------------------------------------------------        
    def onPostMake(self,data):
        self.parent.gtk_object.set_titlebar(self.gtk_object)
    #-------------------------------------------------        
    def add(self,node):
        if isinstance(node,Black_Button):
            if node.position == "start":
                self.gtk_object.pack_start(node.gtk_object)
            elif node.position == "end":
                self.gtk_object.pack_end(node.gtk_object)                
        else:
            Black_List.add(self,node)

#======================================================

class Black_Tree(Black_Item):

#======================================================

    SELECT=String(default=".")
    SHOW_ON=String(default="/")

    #-------------------------------------------------
    def onPostMake(self,data):
        pass
    #-----------------------------------------------------     
    def onPreMake(self,data):
        self.datatree=data
        self.win=data.find(self.show_on)

        self.gtk_object=Gtk.TreeView()
        tree=Gtk.TreeStore(str,str)
        self.build_node(tree,None,self.datatree.find(self.select))
        self.gtk_object.set_model(tree)
        
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("name", renderer, text=0)
        column.set_sort_column_id(0)
        self.gtk_object.append_column(column)
                
        self.selection=self.gtk_object.get_selection()
        self.selection.connect("changed", self.onRowActivated)
        
    #-----------------------------------------------------             
    def build_node(self,tree,node,data_node):
        
        #print(data_node.name)
        for child in data_node.children:
            subnode=tree.append(node,(child.name,"/"+child.path()))
            self.build_node(tree,subnode,child)

    #-------------------------------------------------
    def onRowActivated(self,*args): 
        print(args)
        model, treeiter = self.selection.get_selected()
        node=model[treeiter][1]
        print(node)
        self.win.make(self.datatree.find(node))


#====================================================

class Black_Tree2(Black_List):

#====================================================

    LEVEL=Integer(default=5)
    CONTENT=Field(record=False)
    SELECT=String(default="/")
    TAB=Integer()

    #-------------------------------------------------
    def onPreMake(self,data):
    
        if self.content is not None:
            self.content.destroy()
            self.content=None

        self.gtk_object = Gtk.ListBox()
        self.win=self.find(self.show_on)
        node=data.find(self.select)
    
        self.content=Black_List(parent=self.content,show_on="/"+self.win.path())
        self.onMakeElement(self.content,node,self.level)
        self.content.make(data)
        #self.gtk_object=self.content.gtk_object
    #-------------------------------------------------        
    def onPostMake(self,data):
        #l=self.level-2
        l=0
        for label in self.content.all().by_class(Black_Label):
            i=label.level-l
            label.gtk_object.set_justify(Gtk.Justification.LEFT)
            label.gtk_object.set_xalign(0)
            label.gtk_object.set_margin_start(i*self.tab)
        self.add(self.content)

    #-------------------------------------------------        
    def onMakeElement(self,parent,data,level):

        if level >0:
            label=Black_Label(parent=parent,text=data.name,node=data,padding=10)
            container=Black_List(parent=parent,show_on="/"+self.win.path())
            for elt in data.children.by_class("Object"):
                self.onMakeElement(container,elt,level-1)
            
        
             
    #-------------------------------------------------
    
         
#====================================================


