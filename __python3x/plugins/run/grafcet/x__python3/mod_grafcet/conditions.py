from .nodes import Grafcet_Condition
import time

class Grafcet_Counter(Grafcet_Condition):


    def onSetup(self,**args):
        self.i=0


    def onCleanup(self):pass
    
    def onTest(self,n=1,**args):


        if self.i<n:
            self.i+=1

            return False
        else:
            self.i=0
            return True
            
        return True

class Grafcet_Timer(Grafcet_Condition):

    def onTest(self,dt=0,**args):
        time.sleep(dt)

        return True
        
        
class Grafcet_Door(Grafcet_Condition):

    def onTest(self,value=False,**args):
        return bool(value)

    
