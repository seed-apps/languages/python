from atom import Atom,Atom_Link
from dataModel import *
from mod_program import Action,Code,Loop,Data
import time,random
from atom import TreeLogs

#==============================================================

class Timeline_Atom(Data):

#==============================================================
    pass
#==============================================================

class Timeline_Element(metaclass=ModelMeta):

#==============================================================

    EXECUTE=Function(TreeLogs())
    URL=String()
    ACTION=String()

    #---------------------------------------------------------
    def onSetup(self):
        if self.url:Atom_Link(name=self.action,source=self,target=self.find(self.url))

    #---------------------------------------------------------
    def reset(self):
        self.onReset()
        for child in self.children.by_class(Timeline_Element):
            child.reset()

    #---------------------------------------------------------
    def onReset(self):
        pass

    #---------------------------------------------------------
    def is_on(self,position=None):

        return True
    #---------------------------------------------------------
    def execute(self,position=None):
        if self.is_on(position=position)==True:
            return self.onExecute(position=position)

    #---------------------------------------------------------
    def onExecute(self,position=None):

        if self.url and self.action:
            #print(self.path(),self.url)
            node=self.find(self.url)
            node.query(action=self.action)
        
        for elt in self.children.by_class("Code"):
            if isinstance(elt,Timeline_Element) != True:
                elt.query("call")

        for t in self.children.by_class(Timeline_Element):
            t.execute(position=position)

    #---------------------------------------------------------
    def get_position(self):
        #print(self.path(),self.parent.get_position()+self.position)

        return self.parent.get_position()

    #---------------------------------------------------------


#==============================================================

class Timeline_Key(Timeline_Atom,Timeline_Element):

#==============================================================
    """
    a keyframe is a moment to execute children
    """
    POSITION=Integer(default=None)#,callback="onModifyposition")
    #---------------------------------------------------------
    def onSetup(self):
        if self.url:Atom_Link(name=self.action,source=self,target=self.find(self.url))
    #---------------------------------------------------------
    def get_position(self):
        #print(self.path(),self.parent.get_position()+self.position)

        return self.parent.get_position()+self.position

    #---------------------------------------------------------
    def upper(self,position=None):

        return self.get_position()>position
    #---------------------------------------------------------
    def equal(self,position=None):

            return self.get_position()==position

    #---------------------------------------------------------
    def lower(self,position=None):

        return self.get_position()<position

    #---------------------------------------------------------
    def is_on(self,position=None):

        return self.equal(position)

    #---------------------------------------------------------
    def is_inside(self,begin=None,end=None):

        return self.upper(begin) and self.lower(end)

    #---------------------------------------------------------

#==============================================================

class Timeline_Random(Timeline_Key):

#==============================================================
    """
    execute every dt
    """
    LOOP=Integer(default=0)
    DT=Integer(default=1)
    NEXT=Integer(default=0)
    #---------------------------------------------------------
    def onReset(self):
        self.next=int(random.random()*self.dt)
        self.loop=0
    #---------------------------------------------------------
    def is_on(self,position=None):

        result=False
        if self.loop==self.next:

            result=True
            self.reset()
        self.loop+=1
        return result


    #---------------------------------------------------------


#==============================================================

class Timeline_Cycle(Timeline_Atom,Timeline_Element):

#==============================================================
    """
    execute every dt
    """
    LOOP=Integer(default=0)
    DT=Integer(default=1)
    NEXT=Integer(default=0)
    #---------------------------------------------------------
    def onReset(self):
        self.next=0
        self.loop=0
    #---------------------------------------------------------
    def is_on(self,position=None):

        result=False
        if self.loop==self.next:

            result=True
            self.next=self.loop+self.dt
        self.loop+=1
        return result


    #---------------------------------------------------------
#==============================================================

class Timeline_Loop(Timeline_Atom,Timeline_Element):

#==============================================================
    """
    reset every dt
    """
    LOOP=Integer(default=0)
    DT=Integer(default=1)
    RESET_AFTER=Boolean(default=False)

    #---------------------------------------------------------
    def is_on(self,position=None):

        #print(self.loop,self.dt)
        if self.loop==self.dt-1:
            self.reset_after=True

        return True
    #---------------------------------------------------------
    def onReset(self):
        self.reset_after=False
        self.loop=0

    #---------------------------------------------------------
    def onExecute(self,position=None):

        yield Timeline_Element.onExecute(self,position=self.loop)

        self.loop+=1
        if self.reset_after==True:
            self.reset()

#==============================================================

class Timeline_Track(Atom_Link,Timeline_Element):

#==============================================================
    #recursive behavior
    POSITION=Integer(default=None)
    DURATION=Integer(default=None)
    #---------------------------------------------------------
    def onSetup(self):
        print("setup")
        self.source=Timeline_Key(parent=self,name="start",position=0)
        self.target=Timeline_Key(parent=self,name="end",position=self.duration)


    #---------------------------------------------------------
    def get_position(self):
        #print(self.path(),self.__class__,self.position)
        return self.parent.get_position()+self.position


    #---------------------------------------------------------
    def get_inside(self,position=None,recursive=False):

        if self.is_on(position=position)==True:
            yield self
            for elt in self.traks(recursive=recursive):
                for subelt in elt.get_inside(position=position,recursive=recursive):
                    yield subelt

            if node:
                return node
            else:
                return self   
        else:
            return None

    #---------------------------------------------------------
    def is_on(self,position=None):

        return self.source.lower(position=position+1) and self.target.upper(position=position)
    #---------------------------------------------------------
    def get_bounds(self):
        return (self.source.get_position(),self.target.get_position())
    #---------------------------------------------------------
    def get_min(self):
        return self.source.get_position()

    #---------------------------------------------------------
    def get_max(self):
        return self.target.get_position()

    #---------------------------------------------------------
    def upper(self,position=None):

        return self.source.upper(position)

    #---------------------------------------------------------
    def lower(self,position=None):

        return self.target.lower(position)

    #---------------------------------------------------------
    def equal(self,position=None):

        return self.target.upper(position) and self.source.lower(position)


    #---------------------------------------------------------

#==============================================================

class Timeline(Loop,Timeline_Element):

#==============================================================
    BPM=Integer(default=None)
    DELAY=Float(default=None)

    #---------------------------------------------------------
    def onPostSetup(self):
        self.vmin,self.vmax=None,None
        self.reset()
    #---------------------------------------------------
    def reset(self):
        self.loop_count=0
        self.vmin,self.vmax=self.get_bounds()

    #---------------------------------------------------
    def hasNext(self):
        return self.loop_count < self.vmax+1

    #---------------------------------------------------
    def onNext(self):

        #if self.bpm and self.bpm < 1:self.bpm=1
        self.execute(position=self.loop_count)
        #if self.delay:
        #    time.sleep(self.delay)
        #if self.bpm:
        #    time.sleep(60.0/self.bpm)


    #---------------------------------------------------------
    def get_position(self):
        return 0

    #---------------------------------------------------------
    def get_bounds(self):
        vmin=0
        vmax=0
        for elt in self.children.by_class(Timeline_Track):
            emin,emax=elt.get_bounds()
            if emax>vmax:vmax=emax
            if emin<vmin:vmin=emin
        return (vmin,vmax)


    #---------------------------------------------------------

#==============================================================
