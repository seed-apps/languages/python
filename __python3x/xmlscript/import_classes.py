import sys
import os

#========================================================

def import_lib(path,root_path,DEBUG=False):

#========================================================


    lib=path.replace(root_path+"/","")
    lib=lib.replace(".py","")
    lib=lib.replace("/",".")
    try:
        __import__(lib)
        if DEBUG==True:print("         ",lib)
    except:
        if DEBUG==True:print("    X    ",lib)
            
#========================================================

def import_package(path,root_path,DEBUG=False):

#========================================================

    if os.path.isdir(path):
        init_path= os.path.join(path,"__init__.py")
        
        if os.path.isfile(init_path)==True:
            
            import_lib(path,root_path)
            
            for lib in os.listdir(path):
                lib_path=os.path.join(path,lib)
                if lib == "__init__.py":
                    pass
                elif lib.endswith(".py"):
                    import_lib(lib_path,root_path,DEBUG=DEBUG)
                elif os.path.isdir(lib_path) :
                    import_package(lib_path,root_path,DEBUG=DEBUG)
     
#========================================================

def import_all(path,DEBUG=False):

#========================================================

    done=[]

    for subpath in sys.path:

        if os.path.exists(subpath) and subpath.startswith(path+"/") and subpath not in done:

            for lib in os.listdir(subpath):
                import_package(os.path.join(subpath,lib),subpath,DEBUG=DEBUG)

        done.append(subpath)

#========================================================
    
