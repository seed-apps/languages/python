from .entity import *
import datetime,time
from mod_program import *
#=====================================================================

class Recorder(Entity_Cycle):

#=====================================================================
    """
    receiving
    """
    SOURCE=String(default="..")

    #-----------------------------------------------------------------
    def onSetup(self):
        self.result=list()
        self.node=self.find(self.source)

    #-----------------------------------------------------------------
    def onCall(self,message):
        self.result.append(  (  time , dict( self.onGetData() ) ) )

    #-----------------------------------------------------------------
    def onGetData(self):
        for record in self.children.by_class(Record):
            yield record.name,record.get_data(self.node)


#=====================================================================

class Record(Object):

#=====================================================================
    #-----------------------------------------------------------------
    def get_data(self,node):
        return dict(self.onGetData(node))

    #-----------------------------------------------------------------
    def onGetData(self,node):
        for record in self.children.by_class(Record):
            yield record.name,record.get_data(node)

#=====================================================================

class RecordChildren(Record):

#=====================================================================
    CLS=ModelField(default="Object")
    LISTATTR=String(default="children")
    RECURSIVE=Boolean(default=False)

    #-----------------------------------------------------------------
    def onGetData(self,node):

        if recursive == True:
            lst=node.all()
        else:
            lst=getattr(node,self.listattr)

        for elt in lst.by_class(self.cls):
            yield "/"+elt.path(),dict(Record.onGetData(self,elt))

    #-----------------------------------------------------------------
#=====================================================================

class RecordListLenght(Record):

#=====================================================================

    CLS=ModelField(default="Object")
    LISTATTR=String(default="children")
    RECURSIVE=Boolean(default=False)

    #-----------------------------------------------------------------
    def get_data(self,node):

        if recursive == True:
            lst=node.all()
        else:
            lst=getattr(node,self.listattr)

        lst=lst.by_class(self.cls)
        return lst.__len__()

    #-----------------------------------------------------------------

#=====================================================================
