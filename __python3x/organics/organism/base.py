from atom import *
from dataModel import *
from IoNode import *
import time
from mod_program import *
from ..entity import *
#=====================================================================

class Adn_Base(Entity_Event):

#=====================================================================

    #----------------------------------------------------------------
    def onCall(self,message):
        print("BASE",message.text)
    #----------------------------------------------------------------

#=====================================================================

class Body_Message(Message):

#=====================================================================
    """
    transporte l'information
    """
    TEXT=String()
    

    #-----------------------------------------------------------------
    def onExecute(self,node):
        #print("DO",node.path())
        for elt in node.children.by_class(Adn_Base):
            elt.selected_node=message
            #print(elt.selected_node)
            return elt.call(self)
    #-----------------------------------------------------------------

#=====================================================================

class Body_Pipe(Relation):

#=====================================================================

    CLASS=Body_Message

#=====================================================================

class Body_Base(Entity):

#=====================================================================

    #-----------------------------------------------------------------
    def onReceiveMessageX(self,message):
        if isinstance(message,Body_Message):
            message.execute(self)

    #-----------------------------------------------------------------


