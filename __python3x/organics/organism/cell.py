from .base import *
from atom import *
from dataModel import *
from IoNode import *
import time
from mod_program import *

#=====================================================================

class Adn(Adn_Base):

#=====================================================================
    """
    l'adn est constitué d'éléments Adn_Base 
    c'est une combinaison d'operations simples
    qui va générer de nouvelles structures
    """
    #----------------------------------------------------------------
    def onSetupX(self):

        pass

    #----------------------------------------------------------------
 
    def onTestX(self,message):
        return True
    #----------------------------------------------------------------
    def onCallX(self,message):
        print("ADN",message.text)
    #----------------------------------------------------------------
#=====================================================================

class Chromosom(IoNode):

#=====================================================================
    """
    contient le code pour traiter un message
    chaque constituant d'adn teste le message 
    et execute son code sur le message si ok
    """
    def onCallX(self,message):
        #message.show()
        print(message.text)
        message.tree()

#=====================================================================

class Cell(Body_Base):

#=====================================================================
    """
    thread executant des ordres venant des chromosomes
    """
    pass

#=====================================================================
