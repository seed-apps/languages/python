
from dataModel import *
from mod_program import *
from IoNode import *
import time
DEBUG=True
#=====================================================================

class Entity_Node(Object):

#=====================================================================

    pass

#=====================================================================

class Entity_Cycle(Entity_Node):

#=====================================================================
    TAG=String(default=None)
    COUNT=Integer(default=0)
    N=Integer(default=1)

    def next(self):
        self.count+=1
        if self.count<self.n:
            return

        self.count=0
        return True

    def call(self,message):
        if message.tag == self.tag:

            if self.next() == True:
                if DEBUG==True:print("CYCLE",self.path())
                return self.onCall(message)

    def onCall(self,message):
        pass
#=====================================================================

class Entity_Event(Entity_Node):

#=====================================================================

    CLS=ModelField(default="Message")
    TAG=String(default=None)
    SOURCE=String(default="..")

    #----------------------------------------------------------------
    def call(self,message):
        if self.onTest(message)==True:
            if DEBUG==True:print("EVENT",self.path())
            self.onCall(message)

    #----------------------------------------------------------------
 
    def onCall(self,message):
        pass

    #----------------------------------------------------------------
 
    def onTest(self,message):
        if isinstance(message,self.cls)==True:
            if not self.tag:
                return True
            if self.tag and message.tag:
                if message.tag==self.tag:
                    return True

        return False
#=====================================================================

class SendToPipe(Entity_Event):

#=====================================================================

    #----------------------------------------------------------------
 
    def onCall(self,message):
        if DEBUG==True:print("PIPE",self.path())
        self.parent.send_to_pipe(message)

    #----------------------------------------------------------------
#=====================================================================

class Entity_Send(Entity_Cycle):

#=====================================================================
    CLS=ModelField(default="Message")
    DESTINATION=String(default=None)
    DIRECTION=String(default="down")
    #----------------------------------------------------------------
    def onCall(self,message):
        if DEBUG==True:print("SEND",self.tag,self.path(),self.cls)
        self.parent.new_message(tag=self.destination,direction=self.direction,cls=self.cls)
    #----------------------------------------------------------------

#=====================================================================

class Entity_Network(Entity_Node):

#=====================================================================

    TAG=String(default=None)

    SOURCE=String(default="..")
    TARGET=String(default="..")

    NODE_CLS=ModelField(default="Entity")
    LINK_CLS=ModelField(default="Relation")

    RECURSIVE=Boolean(default=False)
    REVERSE=Boolean(default=False)

    #----------------------------------------------------------------
    def onSetup(self):
        source=self.find(self.source)
        target=self.find(self.target)

        if self.recursive ==True:
            self.make_node_recursive(source,target)
        else:
            self.make_node(source,target)
        
    #----------------------------------------------------------------
    def make_node_recursive(self,source,target):

            self.make_node(source,target)
            for elt in target.children.by_class(self.node_cls):
                self.make_node_recursive(elt,elt)

    #----------------------------------------------------------------
    def make_node(self,source,target):

        for elt in target.children.by_class(self.node_cls):
            if elt !=source:
                if self.reverse==True:
                    self.link_cls(parent=self.parent,source=elt,target=source,tag=self.tag)
                else:
                    self.link_cls(parent=self.parent,source=source,target=elt,tag=self.tag)

    #----------------------------------------------------------------

#=====================================================================

