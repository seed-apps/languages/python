
from dataModel import *
from IoNode import *
import time
from .Action import *
from .Message import *
from .Relation import *
#=====================================================================

class Entity(IoNode):

#=====================================================================
    def onDoChildren(self):
        return #self.children.by_class(Entity_Cycle).execute("call")
        #print(r)

    #-----------------------------------------------------------------
    def onReceiveMessage(self,message):
        print("RECEIVE",message)
        message.execute(self)

    #----------------------------------------------------------------
    def send_to_root(self,message):
        "envoyer un message à tous les pipes sortants"

        for pipe in self.inputs.by_class(Relation):
            pipe.send_message_to_root(message)

    #----------------------------------------------------------------
    def send_to_pipe(self,message):
        "envoyer un message à tous les pipes sortants"

        for pipe in self.outputs.by_class(Relation):
            pipe.send_message(message)
    #----------------------------------------------------------------
    def new_message(self,cls=Message,**args):
        self.send( message=cls(parent=self,**args) )
    #----------------------------------------------------------------
#=====================================================================


