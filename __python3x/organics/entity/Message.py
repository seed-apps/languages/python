from mod_program import *
from dataModel import *
from IoNode import *
import time
from .Action import *

DEBUG=True
#=====================================================================

class Message(Object):

#=====================================================================

    TAG=String(default=None)
    DIRECTION=String(default="down")

    #-----------------------------------------------------------------
    def is_done(self):

        if "stop_time" in self.keys():
            for elt in self.children.by_class(Message):
                if elt.is_done()!=True:
                    return False
            return True
        return False

    #-----------------------------------------------------------------
    def new_instance(self,node):
        message=self.__class__(parent=self,node="/"+node.path(),name=node.name,tag=self.tag,direction=self.direction)
        node.send(message=message)


    #-----------------------------------------------------------------
    def execute(self,node):
        #print("MESSAGE",node.path())
        self.start()
        result = self.onExecute(node)
        self["result"]=result
        self.stop()
        self.onFinish(node)


    #-----------------------------------------------------------------
    def onExecute(self,node):
        for elt in node.children.by_class(Entity_Cycle):
            #print(elt)
            elt.call(self)
        for elt in node.children.by_class(Entity_Event):
            elt.call(self)
    #-----------------------------------------------------------------
    def onFinish(self,node):
        if self.direction == "down":
            node.send_to_pipe(self)
        elif self.direction == "up":
            node.send_to_root(self)

    #-----------------------------------------------------------------
    def start(self):
        self["start_time"]=time.time()
    #-----------------------------------------------------------------
    def stop(self):
        self["stop_time"]=time.time()
    #-----------------------------------------------------------------

#=====================================================================

