
from mod_program import *
from dataModel import *
from IoNode import *
import time

#=====================================================================

class Relation(Object):

#=====================================================================

    CLS=ModelField(default="Message")
    TAG=String(default=None)


    #----------------------------------------------------------------
 
    def onTest(self,message):
        if type(self.cls) == str:
            self.cls=getClass(self.cls)
        #self.show()
        if isinstance(message,self.cls)==True:
            if not self.tag:
                return True
            if self.tag and message.tag:
                if message.tag==self.tag:
                    return True

        return False

    #----------------------------------------------------------------
 
    def send_message(self,message):
        "add a query to the query box"
        if self.onTest(message)==True:
            message.new_instance(self.target)

    #----------------------------------------------------------------
 
    def send_message_to_root(self,message):
        "add a query to the query box"
        if self.onTest(message)==True:
            message.new_instance(self.source)

    #----------------------------------------------------------------
 
#=====================================================================

