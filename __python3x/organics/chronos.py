from .entity import *
import datetime

#=====================================================================

class Planet(Entity):

#=====================================================================
    STAGE=String()

#=====================================================================

class Heart(Entity):

#=====================================================================
    DT=Float(default=1.0)
    TAG=String(default="ping")
    #----------------------------------------------------------------

    def onSetup(self):
        self.i=0
        self.t0=time.time()
        Entity.onSetup(self)

    #----------------------------------------------------------------

    def onDo(self):
        #print("ok","/"+self.path())
        if self.i==0 or time.time()>self.t0+self.dt:
            #print("ok","/"+self.path(),self.i)
            self.i+=1
            self.t0=time.time()
            node=Message(parent=self,tag=self.tag)
            node.execute(self)



#=====================================================================

class Logs(Message):

#=====================================================================
    #-----------------------------------------------------------------
    def onExecute(self,node):

        if node != self.parent:
            for elt in node.children.by_class(Message):
                self.process_message(elt)

    #-----------------------------------------------------------------
    def process_message(self,message):

        message["source"]=message.parent.path()
        message.parent=self
    #-----------------------------------------------------------------

#=====================================================================

class Gather_Logs(Entity_Cycle):

#=====================================================================
    """
    receiving
    """
    DESTINATION=String(default=None)
    #-----------------------------------------------------------------
    def onCall(self,message):
        node=Message(parent=self.parent,tag=self.destination)
        for elt in self.parent.children.by_class(Logs):
            if elt.is_done() ==True:
                elt.parent=node
                elt["source"]=self.parent.path()

        node.execute(self.parent)

#=====================================================================

class Logs_Event(Entity_Event):

#=====================================================================
    def onCall(self,message):
        self.process_message(message.parent)

    #-----------------------------------------------------------------
    def process_message(self,message):

        self.onProcessMessage(message)
        for elt in message.all().by_class(Logs):
            self.process_message(elt)

    #-----------------------------------------------------------------
    def onProcessMessage(self,message):
        pass


#=====================================================================

class Parent_Logs(Logs_Event):

#=====================================================================

    #-----------------------------------------------------------------
    def onCall(self,message):

        message.parent.parent=self.parent


#=====================================================================

class Source_Logs(Logs_Event):

#=====================================================================

    #-----------------------------------------------------------------
    def onProcessMessage(self,message):

        if "source" in message.keys():
            #print(message.source)
            node=self.parent.append(message.source,cls=Atom)
            Atom_Link(source=node,target=message)
    #-----------------------------------------------------------------

#=====================================================================

class Time_Logs(Logs_Event):

#=====================================================================


    CALENDAR=("year","month","day")
    DAY=("hour","minute","second")
    CHRONO=("hour","minute","second","microsecond")
    DAILY_CHRONO=("hour","minute","second")
    DATE_HOURS=("year","month","day","hour","minute","second")
    FULL=("year","month","day","hour","minute","second","microsecond")

    DIVISION=String(default="DATE_HOURS")

    #----------------------------------------------------------------
    def onSetup(self):

        self.precision=getattr(self,self.division)
        Entity_Event.onSetup(self)

    #-----------------------------------------------------------------
    def onProcessMessage(self,message):

        if hasattr(message,"start_time")==True:
            self.set_message(message.start_time,message)
        if hasattr(message,"stop_time")==True:
            self.set_message(message.stop_time,message)

    #-----------------------------------------------------------------
    def set_message(self,t,msg):

        t=datetime.datetime.fromtimestamp(t)
        node=self.parent
        for elt in self.precision:
            data=getattr(t,elt)
            node=node.find(str(data),cls=Atom)
        Atom_Link(source=node,target=msg)

    #-----------------------------------------------------------------
