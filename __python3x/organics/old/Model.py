from mod_program import *
from dataModel import *
from xmlscript import tree_from_file
import glob
from pathlib import Path
#==========================================================

class ModelNode(Setup):
    
#==========================================================

    MODEL_TYPE=String(default="Model")

    def do(self):
        self.execute(self.parent)

    def execute(self,node):

        self.onDo(node)
        self.onDoChildren(node)
        return node

    def onDo(self,node):
        pass

    def onDoChildren(self,node):
        for model in self.children.by_class(Model):
            model.execute(node)

    def new_node(self,path,**args):
        elt=tree_from_file(filename=path,model_type=self.model_type,**args)
        return elt

#==========================================================

class ModelFile(ModelNode):
    
#==========================================================
    FILE_PATH=String()
    NODENAME=String(default=None)

    def onDo(self,node):
        self.new_node(self.file_path,parent=node,name=self.nodename)


#==========================================================

class ModelDir(ModelNode):
    
#==========================================================
    FILE_PATH=String()
    RECURSIVE=Boolean(default=False)

    def onDo(self,node):
        #if self.recursive ==True:
        for path in glob.glob(self.file_path+'/*.xml'):
            self.new_node(path,parent=node)

#==========================================================

class Population(ModelNode):
    
#==========================================================
    N=Integer(default=0)
    FILE_PATH=String()
    NODENAME=String(default=None)

    def onDo(self,node):
        name=""

        if self.nodename:
            name=self.nodename+"_"

        for i in range(self.n):
            self.new_node(self.file_path,parent=node,name=name+str(i))


