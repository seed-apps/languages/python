#model:Class.py
"""
module
---------------------------------------------------

description

"""
from atom import Atom
from dataModel import *
from .node import *
import time
#=====================================================================

class IoNode_Message(Atom):

#=====================================================================
    TEXT=String()
    #-----------------------------------------------------------------
    def execute(self,node):
        message=IoNode_Message(parent=self,node="/"+node.path())
        message.start()
        result = self.onExecute(node)
        message["result"]=result
        message.stop()
        return message


    #-----------------------------------------------------------------
    def onExecute(self,node):
        pass

    #-----------------------------------------------------------------
    def start(self):
        self["start_time"]=time.time()
    #-----------------------------------------------------------------
    def stop(self):
        self["stop_time"]=time.time()
    #-----------------------------------------------------------------
#=====================================================================

class IoNode_Action(IoNode_Message):

#=====================================================================

    #-----------------------------------------------------------------
    def onExecute(self,node):
        if hasattr(node,self.text):
            print(self.text,node.path())
            result=getattr(node,self.text)()
            return result
        return "ERROR :",self.text,node.path()
#=====================================================================

class IoNode_Command(IoNode_Message):

#=====================================================================

    TEXT=String()

    #-----------------------------------------------------------------
    def onExecute(self,node):

        command=self.text%node
        print("CMD ",command)
        return command

#=====================================================================

class IoNode_SystemPipe(IoNode_Pipe):

#=====================================================================

    #-----------------------------------------------------------------
    def onSetup(self):
        pass

    #-----------------------------------------------------------------
    def onCleanup(self):
        pass

    #-----------------------------------------------------------------

#=====================================================================

class IoNode_SystemNode(IoNode):

#=====================================================================
    def onPostSetup(self):
        for elt in self.children.by_class(IoNode_SystemNode):
            print(self.name,elt.name)
            IoNode_SystemPipe(source=self,target=elt)

    #-----------------------------------------------------------------
    def send_to_system(self,message):

        self.onSendSystem(message)
        for child in self.outputs.by_class(IoNode_SystemPipe):
            child.target.send_to_system(message)
        return message
    #-----------------------------------------------------------------
    def receive_from_system(self,message):
        self.onReceiveSystem(message)
        for child in self.inputs.by_class(IoNode_SystemPipe):
            child.source.receive_from_system(message)
        return message
    #-----------------------------------------------------------------
    def onSendSystem(self,message):
        message.execute(self)
    #-----------------------------------------------------------------
    def onReceiveSystem(self,message):
        pass
    #-----------------------------------------------------------------
    def log(self,text):
        return IoNode_Message(text=text)
    #-----------------------------------------------------------------
    def command(self,text):
        return IoNode_Command(text=text)
    #-----------------------------------------------------------------
    def action(self,text):
        return IoNode_Action(text=text)
#-----------------------------------------------------------------
#=====================================================================

class IoNode_Logs(IoNode_SystemNode):

#=====================================================================
    def onPostSetup(self):
        IoNode_SystemPipe(source=self,target=self.parent)

    #-----------------------------------------------------------------
    def onReceiveSystem(self,message):
        message.show()
#=====================================================================

class IoNode_Main(IoNode_SystemNode):

#=====================================================================

    #-----------------------------------------------------------------
    def onReceiveSystem(self,message):
        message.show()

    #----------------------------------------------------------------

    def onStart(self):
        r=self.send_to_system(self.action("is_running")) 
        self.receive_from_system(r)
        r=self.send_to_system(self.action("stop")) 
        self.receive_from_system(r)
    #----------------------------------------------------------------

    def onStop(self):
        r=self.send_to_system(self.action("is_running")) 
        self.receive_from_system(r)
        print("ok")
    #----------------------------------------------------------------
 
