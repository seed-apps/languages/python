
from .code import *
import sys,os
#===================================================================

class Main(Code):

#===================================================================

    #----------------------------------------------------------------
    def onSetup(self):

        self.DEBUG=False
        import xmlscript
        xmlscript.import_all(os.environ["BLACKBOARD_SRC"],DEBUG=self.DEBUG)

        if os.path.isdir(path)==True:
            path=os.path.realpath(path)
            filename=os.path.join(path,"__init__.xml")
            
        elif os.path.isfile(path)==True:
            path=os.path.realpath(path)
            filename=path
            path="/"+os.path.join(*path.split("/")[:-1])
        else:
            raise Exception("no file",path)

        args=list()
        kwargs=dict()
        if len(sys.argv)>2:
            for arg in sys.argv[2:]:

                if "=" in arg:
                    k,v=arg.split("=")
                    kwargs[k]=v
                else:
                    args.append(arg)

        self.node=xmlscript.tree_from_file(filename,file_path=path,parent=self,**kwargs)

    #----------------------------------------------------------------
    def onCleanup(self,**args):
        pass

    #----------------------------------------------------------------
    def onCall(self):
        for msg in self.onCallNodes():
            self.sendMessage(msg)

 #----------------------------------------------------------------
    def onCallNodes(self):

        for elt in self.children.by_class("IoNode"):
            node=Log_Message(name=elt.name+"_run")
            for msg in elt.children.by_class("Log_Message"):
                msg.parent=node
            yield node

 #----------------------------------------------------------------
    def sendMessage(self,msg):
        msg.show()



