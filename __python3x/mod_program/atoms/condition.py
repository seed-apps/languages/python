
from dataModel import *
from ..nodes import Code,Code_Block,Tree_Condition
from dataModelAddons.tree import Tree

#============================================================

class Node_Condition(Code_Block,Tree_Condition):

#============================================================

    #-------------------------------------------------
    def get_handler(self):

        for elt in self.ancestors:
            if isinstance(elt,Handler)==True:
                return elt


    #-------------------------------------------------
    def test(self):
        return self.onTestNode(self.parent.get_node())

    #-------------------------------------------------
    def onTestNode(self,node):
        return True


    #-------------------------------------------------

#============================================================

