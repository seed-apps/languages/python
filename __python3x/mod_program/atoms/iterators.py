from .iterator import Iterator
from dataModel import *
#===================================================================

class Iterator_Group(Iterator):
    
#===================================================================
    """
    regroup severals children iterators

    """

    #----------------------------------------------------------------

    def onIter(self,node):

        lst= list(self.mergeIterators(node))

        return self.onList(select=lst)

    #-------------------------------------------------

    def onList(self,select=None):
        return select

    #-------------------------------------------------



#===================================================================

class First(Iterator_Group):
    
#===================================================================
    """

    """
    N=Integer(default=1)
    #-------------------------------------------------

    def onList(self,select=None):
        for i in range(self.n):
            if i < len(select):
                yield select[i]


#===================================================================

class Last(Iterator_Group):
    
#===================================================================
    """
    if is instance of
    """
    N=Integer(default=1)
    #-------------------------------------------------

    def onList(self,select=None):
        for i in range(self.n):
            if len(select)-i >0:
                yield select[-i]

    #-------------------------------------------------  


#===================================================================

 
