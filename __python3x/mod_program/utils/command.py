# -*- coding: utf-8 -*-


import os
from ..atoms import Action
from .decorators import command,execute
import subprocess,shlex
from dataModel import Boolean,String

#===================================================================

class Command(Action):

#===================================================================
    TEXT=String(default="")
    #-------------------------------------------------
    def get_pre_command(self,node):
        if "command_prefix" in node.keys():
            return node.command_prefix
        for elt in node.ancestors:
            if "command_prefix" in elt.keys():
                return elt.command_prefix
        return ""

    #-------------------------------------------------
    def get_node(self,**args):
        try:
            return Action.get_node(self,**args)
        except:
            return self
    #----------------------------------------------------------------

    def onNextCall(self,node):


        text=self.text%dict(node)


        text = self.get_pre_command(node)+" "+text
        print(text)
        r=execute(text)
        #r.parent=self
        #r.show()
        return r

    #----------------------------------------------------------------

#===================================================================


