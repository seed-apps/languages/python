from functools import wraps

Notifier=list()

        
        
#===================================================================

def notify(text=None):

#==========================================================================

    def decorate(function):
        @wraps(function)
        def CommandFunction(self,*args,**kwargs):

            result=function(self,*args,**kwargs)
            Notifier.append((self,text,args,kwargs,result))
            return result

        return CommandFunction
    return decorate

#===================================================================

def show_notif():

#==========================================================================
    for elt in Notifier:
        print(elt[1],elt[0].path())



