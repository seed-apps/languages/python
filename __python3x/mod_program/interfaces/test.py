
from dataModel import *

#============================================================

class Tree_Condition(metaclass=ModelMeta):

#============================================================

    TEST=Function()

    def test(self):
        return self.onTest()


    def onTest(self):
        """
        return True
        """
        return True

#============================================================

class Node_Condition(metaclass=ModelMeta):

#============================================================

    TEST=Function()

    #-------------------------------------------------
    def test(self,node):
        return self.onTestNode(node)

    #-------------------------------------------------
    def onTestNode(self,node):
        return True


    #-------------------------------------------------

#============================================================

