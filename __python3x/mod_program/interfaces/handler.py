
from dataModel import *
#from atom import treelogs,Atom
#from ..nodes import Code,Code_Block,Tree_Condition
from dataModelAddons.tree import Tree

from .variables import VariableManager

#==========================================================

class Code_Initializer(VariableManager):
    
#==========================================================

    ENV_DONE=False
    TREE_DONE=Boolean(default=False)
    INITIALIZE=Function(TreeLogs())
    CLEANING=Function(TreeLogs())
    #-------------------------------------------------------------
    def initialize(self):


        if Code_Initializer.ENV_DONE == False:

            self.add_environ()
            self.add_command_line()
            Code_Initializer.ENV_DONE=True

        if self.tree_done == False:
            self.tree_done=True
            self.processTree()

            yield self.children.by_class("Global").query(action="do")
            self.processTree()
            yield self.children.by_class("Setup").query(action="do")

    #-------------------------------------------------------------
    def cleaning(self):
        yield self.children.by_class("Cleanup").query(action="do")




#============================================================

class Node_Handler(metaclass=ModelMeta):

#============================================================

    """
    default behavior : pass selected_node to children and process children
    """
    SELECT=String(default=None)
    SELECTED_NODE=Field(record=False,default=None)

    #-------------------------------------------------
    def set_node(self,node):
        self.selected_node=node

    #-------------------------------------------------
    def get_node(self,error=True):
        
        #with path, searching relatively to self
        if self.select is not None:
            self.selected_node = self.find(path=self.select)

        #return or error
        if self.selected_node is not None:
            return self.selected_node
            
        elif error ==True:
            raise Exception(self.__class__.__name__,self.path(),"no path",self.select)
    #-------------------------------------------------

#============================================================

class Action_Execution(Node_Handler):

#============================================================

    """
    interface pour l'execution

    1 - l'appel commence par le test du noeud selectionnné
        par les les enfants Node_Condition
    2 - si toutes les conditions sont passées
        on execute les enfants Action sur le noeud sélectionné
    """

    #-------------------------------------------------
    def onNextCall(self,node,**args):
        #print("call",self.path(),node.path())
        return self.onProcessElement(node,**args)                

    #-------------------------------------------------
    def onTestNode(self,node):
        """
        test le noeud sur touts les enfants Node_Condition
        """

        for test in self.children.by_class("Node_Condition"):
            if test.onTestNode(node) !=True:
                return False
        return True 
    #-------------------------------------------------
    def onProcessElement(self,node,**args):

        if self.onTestNode(node)==True:
            for child in self.children.by_class("Code"):

                if isinstance(child,Action):
                    child.set_node(node)
                    yield child.call()                
                else:
                    yield child.call()  
    #-------------------------------------------------


#============================================================



