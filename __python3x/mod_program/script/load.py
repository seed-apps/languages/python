from ..atoms import *
from xmlscript import tree_from_file
import glob
from pathlib import Path
import minimal.path
#==========================================================

class Import_Xml(Action):
    
#==========================================================


    FILE_PATH=String()
    NODENAME=String(default=None)
    DO_SETUP=Boolean(default=True)

    #-------------------------------------------------------
    def get_filepath(self,node):

    #-------------------------------------------------------
        if self.file_path:
            return self.file_path
        elif hasattr(node,"file_path"):
            return node.file_path

    #-------------------------------------------------------
    def new_node(self,filename,**args):

    #-------------------------------------------------------
        #print("IMPORT",filename)
        elt=tree_from_file(filename=filename,**args)
        return elt
    #-------------------------------------------------------
    def onNextCall(self,node):

    #-------------------------------------------------------
        path=self.get_filepath(node)

        if path :

            args=dict()
            if self.nodename is not None:
                args["name"]=self.nodename
            for elt in self.onNextLoad(node,path,**args):
               if self.do_setup==True:
                    yield elt.setup()
               yield self.onProcessElement(elt)
               #elt.tree()
        else:
            raise Exeception("no path",self.path())

    #-------------------------------------------------------
    def onNextLoad(self,node,path,**args):

    #-------------------------------------------------------

        yield self.new_node(path,parent=node,**args)
 


#==========================================================

class Import_XmlFile(Import_Xml):
    
#==========================================================

    def onNextLoad(self,node,path,**args):
        yield self.new_node(path,parent=node,**args)


#==========================================================

class Import_XmlDir(Import_Xml):
    
#==========================================================

    EXTENTION=String(default=".xml")

    def onNextLoad(self,node,path,**args):

        for elt in glob.glob(path+'/*'+self.extention):
            #self.log(file_path=elt)
            #print(elt)
            yield self.new_node(elt,parent=node,**args)
#==========================================================

class Import_XmlTree(Import_Xml):
    
#==========================================================

    EXTENTION=String(default=".xml")

    def onNextLoad(self,node,path,**args):

        print("TREE",path)

        for elt in minimal.path.filterfiles(path,self.extention):
            #print(elt)
  

            path=elt.string.replace(path+"/","")
            lst=path.split("/")
            root="/".join(lst[:-1])

            if root.strip() !="":
                root=node.append(root,cls="Object")
            else:
                root=node
            yield self.new_node(elt.string,parent=root,**args)


#==========================================================

class Import_XmlInit(Import_Xml):
    
#==========================================================

    FILENAME=String()

    def onNextLoad(self,node,path,**args):

        for elt in Path(path).rglob(self.filename):

            #if elt.split("/")[-1]==self.filename:
            yield self.new_node(elt,parent=node,**args)
#==========================================================

class Import_XmlDirs(Import_Xml):
    
#==========================================================

    FILENAME=String()
    EXTENTION=String(default=".xml")

    def onNextLoad(self,node,path,**args):
        for elt in Path(path).rglob(self.filename):
            #print(type(elt),elt)
            for elt2 in glob.glob(str(elt)+'/*'+self.extention):
                #self.log(file_path=elt)
                print(elt2)
                yield self.new_node(elt2,parent=node,**args)


#==========================================================

