from ..atoms import *
from ..code import *

import glob
from pathlib import Path
import minimal.path
import xml.etree.ElementTree as ET
#==========================================================

class Load_XmlInstance(Action):
    
#==========================================================


    FILE_PATH=String()
    CLASSES=String(default=None)

    #-------------------------------------------------------
    def get_filepath(self,node):

    #-------------------------------------------------------
        if self.file_path:
            return self.file_path%node

    #-------------------------------------------------------
    def onSetup(self):

    #-------------------------------------------------------
        self.classes_obj=self.find(self.classes)

    #-------------------------------------------------------
    def onNextCall(self,node):

    #-------------------------------------------------------
        path=self.get_filepath(node)
        print(path)
        if path :
            data= self.tree_from_xml(ET.parse(path).getroot(),parent=node)


    #-------------------------------------------------------

    def tree_from_xml(self,xmlnode,parent=None,**kwargs):

    #-------------------------------------------------------


        if hasattr( xmlnode , 'attrib'): 
            attrib=dict(xmlnode.attrib )
            #attrib=dict(convert_dict(xmlnode.attrib ))
        else:
            attrib=None

        #tag
        if hasattr( xmlnode , 'tag'): 
            tag=xmlnode.tag

        else:
            tag=None

        #text
        if hasattr( xmlnode , 'text'):

            if xmlnode.text is not None:
     
                text=xmlnode.text.strip()

                if text !="":
                    attrib["text"]=text

        #convert attributes
        attrib.update(kwargs)

        for k,v in attrib.items():
            try:
                attrib[k]=v#eval(v)
            except:
                pass

        #print(cls,attrib)
        node=self.classes_obj.get_instance(tag,parent=parent,**attrib)
 

        for xmlchild in xmlnode:
            self.tree_from_xml(xmlchild,parent=node)
        return node

#==========================================================
