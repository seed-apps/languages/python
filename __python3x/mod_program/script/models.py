from mod_program import *
from dataModel import *
from xmlscript import tree_from_file
import glob,os
from pathlib import Path
from .load import Import_Xml
#==========================================================

class Import_Model(Import_Xml):
    
#==========================================================

    MODEL=String()
    def onSetup(self):
        self.file_path=self.get_filepath()
        if os.path.exists(self.file_path)!=True:
            self.active==False
            print("no model",self.model,self.file_path)

    def get_filepath(self,*args):
        return os.environ["MAIN_MODELS"]+"/"+self.model


#==========================================================

class Import_ModelFile(Import_Model):
    
#==========================================================
    def get_filepath(self,*args):
        return os.environ["MAIN_MODELS"]+"/"+self.model+".xml"

    def onNextLoad(self,node,path,**args):
        yield self.new_node(path,parent=node,**args)


#==========================================================

class Import_ModelDir(Import_Model):
    
#==========================================================

    RECURSIVE=Boolean(default=False)

    def onNextLoad(self,node,path,**args):
        if self.recursive ==True:
            for path in glob.glob(path+'/*.xml'):
                yield self.new_node(path,parent=node,name=path.split("/")[-1].replace(".xml",""))
        else:
           for path in glob.glob(path+'/*.xml'):
                yield self.new_node(path,parent=node,name=path.split("/")[-1].replace(".xml",""))

#==========================================================

class Import_ModelPopulation(Import_Model):
    
#==========================================================
    N=Integer(default=0)

    def get_filepath(self,*args):
        return os.environ["MAIN_MODELS"]+"/"+self.model+".xml"


    def onNextLoad(self,node,path,name=None,**args):

        if name is None:
            name=self.name
        name=name+"_"

        for i in range(self.n):
            yield self.new_node(path,parent=node,name=name+str(i))


