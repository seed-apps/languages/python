#from atom import Atom,TreeLogs

from dataModel import *
from dataModelAddons.network import Network_Node
from dataModelAddons.tree import Tree
from .interfaces import Tree_Initializer,Code_Initializer,Code_Initializer
from .interfaces import Tree_Condition,Tree_Runner
#==========================================================

class Code_Block(Model,Tree,Network_Node):

#==========================================================
    """
    basic element to build a code tree
    """
    #---------------------------------------------------

    def onInit(self):
        """
        set an automatic name if no name when instanciate object
        """
        self.autoname()




#===============================================================

class Object(Model,Network_Node,Tree_Initializer,Code_Initializer):
    
#===============================================================
    #IS_OPENED=Boolean(default=False)
    OPEN=Function()
    CLOSE=Function()
    #---------------------------------------------------

    def onInit(self):
        """
        set an automatic name if no name when instanciate object
        """
        self.autoname() 
    #---------------------------------------------------

    def onPreSetup(self):
        """
        set an automatic name if no name when instanciate object
        """
        return self.initialize() 
    #---------------------------------------------------

    def onPostCleanup(self):
        """
        """
        return self.cleaning()

    #-------------------------------------------------------------
    def is_ok(self):

        return True

    #-------------------------------------------------------------
    def open(self):
        if self.is_ok()==True:
            self.is_opened=True
            self.onOpen()


    #-------------------------------------------------------------
    def close(self):

        if self.is_opened==True:
            self.is_opened=False
            self.onClose()

    #-------------------------------------------------------------
    def onSetup(self):pass
    def onOpen(self):
        self.children.by_class(Object).execute("open")

    def onClose(self):
        self.children.by_class(Object).execute("close")
    def onCleanup(self):pass
    def onDestroy(self):pass
    #-------------------------------------------------------------
#==================================================================

class Relation(Object,Link):
    
#==========================================================



    def onSetup(self):
        #print(self.source,type(self.source))
        if type(self.source) == str:
            self.source= self.find(self.source)


        if type(self.target) == str:
            self.target= self.find(self.target)


#===============================================================

class Code(Code_Block,Code_Initializer,Tree_Condition,Tree_Runner):
    
#===============================================================


    #-------------------------------------------------------------
    def onPreSetup(self):
        "code object can initialize global variables for all tree"
        return self.initialize()
    #---------------------------------------------------

    def onPostCleanup(self):
        """
        """
        return self.cleaning()
    #-------------------------------------------------------------
    def call(self):
        "test if ok before calling onCall"

        if  self.active==True and self.test()==True:
            return Tree_Runner.call(self)
    #-------------------------------------------------------------
    def onCall(self):
        "call children code"
        for elt in self.children.by_class(Code):
            yield elt.call()
    #-------------------------------------------------------------
    def onTest(self):
        """
        test all children conditions, return False for first wrong
        """
        for elt in self.children.by_class(Code_Condition):
            if elt.test() !=True:
                return False
        return True
    #-------------------------------------------------------------
#===============================================================

class Code_Condition(Code_Block,Tree_Condition):
    
#===============================================================
    COUNTER=Integer(default=0)
    #---------------------------------------------------
    def reset(self):
        "counter to zero"
        self.counter=0

    def onTest(self):
        """
        test all children conditions, return False for first wrong
        """
        for elt in self.children.by_class(Code_Condition):
            if elt.test() !=True:
                return False
        return True
    #---------------------------------------------------


#===============================================================

class Instruction(Code_Block):
    
#===============================================================
    DO   =Function(TreeLogs())

    def do(self):
        return self.onDo()

    def onDo(self):
        for elt in self.children.by_class(Code):
            yield elt.setup()
            if elt.is_instance("Node_Handler"):
                elt.set_node(self.parent)
            #print("SETUP",elt.path(),self.parent.path())
            yield elt.call()

#===============================================================

class Global(Instruction):
    
#===============================================================
    VALUE=String()


    def onDo(self):
        self.parent.setGlobal(self.name,self.value)


#===============================================================

class Setup(Instruction):
    
#===============================================================
    
    pass


#===============================================================

class Cleanup(Instruction):
    
#===============================================================

    pass




