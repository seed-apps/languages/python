import time
from ..nodes import *
from dataModel import *
#=============================================================

class Range(Code_Condition):

#=============================================================
    """
    renvoie vrai n fois
    """
    N=Integer(default=1)

    #---------------------------------------------------
    def onTest(self):

        if self.counter<self.n:
            self.counter+=1
            return True
        return False

    #---------------------------------------------------

#=============================================================

class Task(Code_Condition):

#=============================================================

    """
    executer une tache au bout de N0 fois et désactive
    """    

    N=Integer(default=0)



    #-------------------------------------------------
    def onTest(self):
            
        if self.counter < self.n:   
            self.counter+=1
            return False
            
        else:
            self.counter+=1           
            self.active=False
            return True
    #---------------------------------------------------------


#=============================================================

class Cycle(Code_Condition):

#=============================================================

    """
    executer une tache tous les DELTA fois
    """ 

    N=Integer(default=0)

    #-------------------------------------------------
    def onSetup(self):
        self.counter=0

    #-------------------------------------------------
    def onTest(self):

        if self.counter == 0:
            self.counter=1
            return True

        elif self.counter < self.n:    
            self.counter+=1
            return False

        elif self.counter == self.n:
            self.counter=1
            return True

    #---------------------------------------------------------                


#=============================================================


class Delay(Code_Condition):

#=============================================================
    """
    executer une tache un temps dt après le premier appel et désactive
    si le temps est dépassé, excute et arret
    """    
    T=Float(default=1.0)

    #-------------------------------------------------
    def onSetup(self):
        self.t0=None

    #-------------------------------------------------
    def onTest(self):

        t=time.time()
        if self.t0 is None:
            self.t0=t
            return False

        if t  >= self.t0+self.t:
            self.active=False
            return True
        else:
            return False

    #---------------------------------------------------------
        
#=============================================================

class Dt(Code_Condition):

#=============================================================

    """
    executer une tache tous les dt fois
    """ 

    DT=Float(default=0.0)
    T=Float(default=0)          # temps courant remi à jour chaque appel
    T0=Float(default=None)      # temps initial
    CYCLES=Integer(default=0)

    #-------------------------------------------------
    def onTest(self):
        t=time.time()
        if self.t0 is None:
            self.t0=t

        t1=self.next_time()
        if t < t1:
            #time.sleep(t1-t)
            return False
        else:
            print(self.cycles,t)
            self.cycles+=1
            return True

    #-------------------------------------------------
    def next_time(self):
        return self.t0+(self.cycles+1)*self.dt

    #---------------------------------------------------------                

#=============================================================



