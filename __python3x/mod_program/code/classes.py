
from mod_program import Node_Handler
from dataModel import *
import os
#TODO:decorator à changer

from ..nodes import *
from ..atoms import *
from dataModel import *
from dataModelAddons.tree.model import ChildrenFrame
DEBUG=False
#DEBUG=True
#===============================================================================

def store(function):

#===============================================================================
    """
    decorator to merge results of a function into a single list :
       @store
       def function(self,a=1):
           yield obj1
           yield obj2

       @store
       def function(self,a=1):
           yield obj1
           yield [obj2,obj3]

       @store
       def function(self,a=1):
           yield obj0
           yield [obj1,obj4]
           yield [obj2,obj3]
           yield obj5
    """

    def execute(self,*args,**kwargs):

        #create a list of node
        lst=Frame()

        #function returns list or iterator of lists and objects
        for elt in function(self,*args,**kwargs):

            #manage when function returns a list
            if isinstance(elt,Frame) or isinstance(elt,list):

                # merge with current list
                for subelt in elt:
                    if subelt is not None: 
                        lst.append(subelt)

                #elt.destroy()
            elif elt is not None:
                
                lst.append(elt)
        return lst

    return execute
#========================================================================

class InstanceOf(Relation):

#========================================================================
    CLS=String()
    #-------------------------------------------------------------
    def onSetup(self):
        if self.cls is not None:
            for elt in self.root.all().by_class(Class):
                if elt.name==self.cls:
                    self.source=self.parent
                    self.target=elt
                    if DEBUG==True:print("link",self.source.path(),self.target.path())
        Relation.onSetup(self)
    #-------------------------------------------------------------
#========================================================================

class SubClassOf(Relation):

#========================================================================
    CLS=String()

    #-------------------------------------------------------------
    def build(self):
        if self.cls is not None:
            for elt in self.root.all().by_class(Class):
                if elt.name==self.cls:
                    self.source=self.parent
                    self.target=elt
                    if DEBUG==True:print("subcls",self.source.path(),self.target.path())
        Relation.onSetup(self)
    #-------------------------------------------------------------
#========================================================================

class Class_Node(Object,Node_Handler):

#========================================================================

    #-------------------------------------------------------------

    def get_class(self,name):
        for cls in self.root.all().by_class(Class):
            if cls.name == name:
                return cls

#========================================================================

class Attribute(Class_Node):

#========================================================================

    DEFAULT=String()
    FORCE=Boolean(default=False)
    SELECT=String(default=None)

    def onNextCall(self,node):
        if self.default and self.force==True:
            node[self.name]=self.default%node
        elif self.name in node.keys() and node[self.name] is None and self.force==True:
            node[self.name]=node[self.name]%node
        elif self.default and self.select is not None:
            parent=node.find(self.select)
            #print(parent.path())
            #print(self.path(),self.default)
            node[self.name]=self.default%parent
        elif self.default and (not self.name in node.keys() or node[self.name] is None):
            #print(self.path(),self.default)
            node[self.name]=self.default%node

#========================================================================

class ChildAttribute(Class_Node):

#========================================================================

    CLS=String()

    def onNextCall(self,node):
        result=self.get_class(self.cls).get_instance(parent=node)
        for attr in self.children.by_class(Attribute):
            attr.onNextCall(result)

#========================================================================

class View(Class_Node):

#========================================================================
    RENDER=Function()
    def render(self):
        return

#========================================================================

class Method(Action):

#========================================================================
    pass

#========================================================================

class Class(Class_Node):

#========================================================================

    SUPERCLASSES=Function() 
    SUBCLASSES=Function() 
    INSTANCES=Function() 

    ALLSUPERCLASSES=Function() 
    ALLSUBCLASSES=Function() 
    ALLINSTANCES=Function() 

    CLS_METHODS=Function()
    CLS_VIEWS=Function()
    CLS_ATTRIBUTES=Function()

    #-------------------------------------------------------------
    def onSetup(self):
        pass

    #-------------------------------------------------------------
    def superclasses(self):
        return self.outputs.by_class(SubClassOf).objects()
    #-------------------------------------------------------------
    def subclasses(self):
        return self.inputs.by_class(SubClassOf).subjects()

    #-------------------------------------------------------------
    @store
    def allsuperclasses(self,first=True):
        if first==True:
            yield self
        for cls in self.superclasses():
            yield cls.allsuperclasses()
    #-------------------------------------------------------------
    @store
    def allsubclasses(self):
        for cls in self.subclasses():
            yield cls
            yield cls.allsubclasses()

    #-------------------------------------------------------------

    def instances(self):
        return self.inputs.by_class(InstanceOf).subjects()
    #-------------------------------------------------------------
    @store
    def allinstances(self):
        yield self.instances()
        for cls in self.allsubclasses():
            yield cls.instances()
    #-------------------------------------------------------------

    def get_instance(self,**args):
        if DEBUG==True:print("CLASS",self.name,args)
        node=Instance(**args)

        lst=list(self.allsuperclasses())
        lst.reverse()
        for cls in lst:
            if DEBUG==True:print(cls.name)
            cls.get_instance_attributes(node,False)



        for cls in self.allsuperclasses():
            if DEBUG==True:print(cls.name)
            cls.get_instance_attributes(node,True)



        node.setup()
        #node.show()
        self.get_instance_childattributes(node)
        for cls in self.allsuperclasses():
            cls.get_instance_childattributes(node)


        link=InstanceOf(source=node,target=self,parent=self)
        link.setup()
        return node

    #-------------------------------------------------------------

    def get_instance_childattributes(self,node):
        for elt in self.children.by_class(ChildAttribute):
            elt.onNextCall(node)

    #-------------------------------------------------------------

    def get_instance_attributes(self,node,force):

        for attr in self.cls_attributes():
            if attr.force==force:
                if DEBUG==True:print("ATTR",attr.name,attr.name)
                attr.onNextCall(node)

         
    #-------------------------------------------------------------
    @store
    def check_instance(self,node):
        for attr in self.attributes():
            yield attr.check_instance(node)

    #-------------------------------------------------------------

    def cls_methods(self):
        return self.children.by_class(Method)

    #-------------------------------------------------------------

    def cls_views(self):
        return self.children.by_class(Class_View)
    #-------------------------------------------------------------

    def cls_attributes(self):
        return self.children.by_class(Attribute)
    #-------------------------------------------------------------



#========================================================================

class Instance(Object):

#========================================================================
    CLASSES=Function()
    ALLCLASSES=Function()

    METHODS=Function()
    VIEWS=Function()
    ATTRIBUTES=Function()


    #-------------------------------------------------------------
    def onSetup(self):
        pass
    #-------------------------------------------------
    def is_instance(self,cls,strict=False):
        if strict==True:
            return cls == elt.name

        for elt in self.allclasses():
            if cls == elt.name:
                return True
 
    #-------------------------------------------------------------
    def classes(self):
        #for link in self.outputs.by_class(InstanceOf):
        #    print(link.__class__,link.parent.path())
        #    link.show()
        #    print("LINK",link.source.path(),link.target.path())
        return self.outputs.by_class(InstanceOf).objects()
    #-------------------------------------------------------------
    @store
    def allclasses(self):
        for cls in self.classes():
            yield cls
            yield cls.allsuperclasses()
    #-------------------------------------------------------------
    @store
    def methods(self):
        for cls in self.allclasses():
            yield cls.name,cls.cls_methods()

    #-------------------------------------------------------------
    def query(self,action=None,**args):
        #print("call method",self.path(),action)
        for cls,methods in self.methods():
            for method in methods:
                #print("call method",cls,method.name)
                if method.name == action:
                    #print("call method",method.path(),self.path())
                    #self.show()
                    method.set_node(self)
                    return method.call()
        print("no method",self.path(),action)
        return Object.query(self,action=action,**args)

    #-------------------------------------------------------------
    @store
    def views(self):
        for cls in self.allclasses():
            yield cls.name,cls.views()
    #-------------------------------------------------------------
    @store
    def attributes(self):
        for cls in self.allclasses():
            yield cls.name,cls.attributes()
    #-------------------------------------------------------------
    @store
    def check_instance(self):
        for cls in self.allclasses():
            yield cls.name,cls.check_instance(self)

    #-------------------------------------------------------------
#========================================================================

class Namespace(Class_Node):

#========================================================================
    CLASSES=Function()
    #-------------------------------------------------------------

    def classes(self):

        return self.children.by_class(Class)


         
    #-------------------------------------------------------------

    def get_class(self,name=None,cls=Class,**args):

        return self.append(name,cls=cls,**args)

#========================================================================

class Class_Module_Link(Relation):

#========================================================================

    #-------------------------------------------------------------
    def onSetup(self):
        pass
    #-------------------------------------------------------------

#============================================================

class Function(Object,Action_Execution):

#============================================================

    def onNextCall(self,node):
        print(self.path(),node.name)
        return self.onProcessElement(node)                

#============================================================

class Module(Object,Action_Execution):

#============================================================

    pass

#========================================================================

class Class_Tool(Class_Node):

#========================================================================

    #-------------------------------------------------------------

    def onSetup(self):

        for elt in self.root.all().by_class(Class):
            Class_Module_Link(parent=self,source=self,target=elt)
        for elt in self.root.all().by_class(SubClassOf):
            elt.build()
    #-------------------------------------------------------------

    def get_class(self,name):
        for cls in self.children.by_class(Class_Module_Link):
            if cls.target.name == name:
                return cls.target
        node = Class(parent=self,name=name)
        Class_Module_Link(parent=self,source=self,target=node)
        return node



    #-------------------------------------------------------------

    def get_instance(self,classname,**args):
        cls=self.get_class(classname)
        if cls is None:
            return
        return cls.get_instance(**args)

    #-------------------------------------------------------------

#========================================================================
                       
