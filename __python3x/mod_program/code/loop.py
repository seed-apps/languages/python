from .code import *
from dataModelAddons import treelogs
from dataModel import Integer

#=======================================================

class Loop(SimpleCommand):

#=======================================================
    """
    une boucle execute un cycle
    jusqu' à ce qu'il soit finit
    """

    LOOP_COUNT=Integer(default=0)

    #---------------------------------------------------
    def onCall(self):
        """
        boucle while jusqu'à épuisement des conditions
        """
        while self.hasNext()==True:
            result= self.next()
            result.name="loop_"+str(self.loop_count)
            self.loop_count+=1
            #print(self.loop_count)
            yield result

        self.reset()

    #---------------------------------------------------
    def reset(self):
        self.loop_count=0

    #---------------------------------------------------
    def hasNext(self):
        """
        """
        return self.test()

    #---------------------------------------------------
    @treelogs
    def next(self):
        return self.onNext()

    #---------------------------------------------------
    def onNext(self):
        """
        par default appele la fonction call des objets Code enfants
        """
        for elt in self.children.by_class("Code"):
            yield elt.call()
    #---------------------------------------------------

#=======================================================

class LoopSynchro(Loop):

#=======================================================
    """ 
    executer toutes les boucles enfants ensemble jusqu'au bout
    TODO: marche pas
    """

    #---------------------------------------------------
    def onCall(self):

        ok=True
        while ok == True:

            #ok à faut, si aucune action ne le met à True
            #fin des boucles, sortie 
            ok=False

            #pour toutes les boucles faire avancer d'un pas
            for action in self.children.by_class(Loop):

                if action.hasNext():
                    ok=True
                    yield action.next()

    #---------------------------------------------------

#=======================================================



