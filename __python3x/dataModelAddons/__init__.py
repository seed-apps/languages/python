"""
documentation des comportements génériques :

interfaces de programmation ajoutées à Model pour créer des comportements
"""
from .tree import *
from .network import *
from .messages import *
