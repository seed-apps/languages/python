from dataModel import *

#=======================================================================

class NodeStore(Frame):

#=======================================================================

    #STORE
    #=========================================================

    #----------------------------------------------------------

    def by_class(self,cls):
        return NodeStore(Store.by_class(self,cls))

    #----------------------------------------------------------

    def by_attribute(self,attribute):
        return NodeStore(Store.by_attribute(self,attribute))

    #----------------------------------------------------------

    #LINKS
    #=========================================================

    #GENERAL

    #----------------------------------------------------------

    def getLinks(self):
        return LinkStore(InputLinksIterator(selected=self))

    #----------------------------------------------------------

    def getInputLinks(self):
        return LinkStore(InputLinksIterator(selected=self))

    #----------------------------------------------------------

    def getOutputLinks(self):
        return LinkStore(OutputLinksIterator(selected=self))

    #----------------------------------------------------------

    def getBorderLinks(self):
        return LinkStore(InputLinksIterator(selected=self))

    #----------------------------------------------------------

    def getInternalLinks(self):
        return LinkStore(InternalLinksIterator(selected=self))

    #----------------------------------------------------------

    #=========================================================

#=======================================================================

class LinkStore(Frame):

#=======================================================================

    #STORE
    #=========================================================

    #----------------------------------------------------------

    def by_class(self,cls):
        return LinkStore(Store.by_class(self,cls))

    #----------------------------------------------------------

    def by_attribute(self,attribute):
        return LinkStore(Store.by_attribute(self,attribute))

    #----------------------------------------------------------


    #LINKS
    #=========================================================

    #----------------------------------------------------------

    def selfLoops(self):
        return LinkStore(SelfLoopIterator(selected=self))

    #----------------------------------------------------------

    #NODES
    #=========================================================

    #INTERNALS

    #----------------------------------------------------------

    def nodes(self):
        return NodeStore(AllNodesIterator(selected=self))

    #----------------------------------------------------------

    def sources(self):
        return NodeStore(SubjectListIterator(selected=self))

    #----------------------------------------------------------

    def targets(self):
        return NodeStore(ObjectListIterator(selected=self))


    #----------------------------------------------------------

    #INTERNALS FLOW

    #----------------------------------------------------------

    def upstreams(self):
        return NodeStore(UpstreamNodesIterator(selected=self))

    #----------------------------------------------------------

    def downstreams(self):
        return NodeStore(DownstreamNodesIterator(selected=self))

    #----------------------------------------------------------


    def borders(self):
        return NodeStore(BorderNodesIterator_FromLinks(selected=self))

    #----------------------------------------------------------

    def internals(self):
        return NodeStore(InternalNodesIterator(selected=self))

    #----------------------------------------------------------

    #EXTERNALS

    #----------------------------------------------------------

    def neighbors(self):
        return NodeStore(InternalNodesIterator(selected=self))


    #----------------------------------------------------------

    #=========================================================


#===============================================================================


