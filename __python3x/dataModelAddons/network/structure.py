# -*- coding: utf-8 -*-
from dataModelAddons.container import Container
from .model import Base_Node,Link

#=======================================================================

class Network(Container):

#=======================================================================

    #---------------------------------------------------
    # DOCUMENTATION
    #---------------------------------------------------

    """
    conteneur de noeuds Network.
    L'arbre Tree est un dictionnaire de chemin ::

       tree=Tree()
       tree['a']=dict()
       node=node['a']

       tree.keys()
       tree.values()
       tree.items()


    le noeud se comporte comme un dictionnaire de données.
    on combine les comportements de Base_Node et Tree_Node pour avoir accès aux données ::

       tree["a/b/c"].keys()
       tree["a/b/c"].values()
       tree["a/b/c"].items()


    """
    #---------------------------------------------------
    # INIT
    #---------------------------------------------------

    def __init__(self,NodeClass=Base_Node,LinkClass=Link,**args):
        """
        creer un arbre avec Tree ::

           from pynode import Tree
           tree=Tree()
        """
        pass #self.nodes=Tree(Class=NodeClass)


    #---------------------------------------------------
    # PATH DICT
    #---------------------------------------------------

    def keys(self):

        """ liste les chemins """
        pass

    #---------------------------------------------------

    def values(self):

        """ liste noeuds """
        pass

    #---------------------------------------------------

    def items(self):

        """ liste des tuples (chemins,noeud) """
        pass

    #---------------------------------------------------

    def __getitem__(self,key):

        """ 
        retourne le noeud correspondant au chemin.
        pour acceder au noeud, on recherche son chemin ::

           node = tree["a/b/c"]

        lire une donnée ::

           data = tree["a/b/c"]["a"]
        """
        pass

    #---------------------------------------------------

    def __setitem__(self,key,value):

        """ 
        creer le noeud correspondant au chemin key avec le dictionnaire value
        on cree un nouveau noeud avec son chemin
        et un dictionnaire de données ::

           tree["a/b/c"] = dict(a=1)

        mettre à jour le dictionnaire ::

           tree["a/b/c"] = dict(a=2,b=3)

        modifier une donnée ::

           tree["a/b/c"]["a"] = 2
        """
        pass
    #---------------------------------------------------

    def __delitem__(self,key,value):

        """ 
        pour supprimer un noeud ::

           del tree["a/b/c"]
         """
        pass

    #---------------------------------------------------

#=======================================================================

