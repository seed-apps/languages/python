from .messages import execute_log
from dataModel import Decorator
import os,time,sys,traceback



#=============================================================================

class TreeLogs(Decorator):

#=============================================================================
    def onCall(self,function,*args,**kwargs):
        return execute_log(function,*args,**kwargs)

#=============================================================================

class ResultOrError(Decorator):

#=============================================================================

    def onCall(self,function,*args,**kwargs):

        return result_or_error(function)(*args,**kwargs)


#==========================================================================

def result_or_error(function):

#==========================================================================


    def ResultLogFunction(*args,**kwargs):

        return execute_log(function,*args,**kwargs)
        
    return ResultLogFunction


#==========================================================================

def treelogs(function):

#==========================================================================

    def TreelogsFunction(*args,**kwargs):

        return execute_log(function,*args,**kwargs)

    return TreelogsFunction

#==========================================================================
