# -*- coding: utf-8 -*-

import time,inspect,sys,traceback

from dataModel import Field,Time,String,Integer,Float
from dataModel import Model,Field,Function_Interface
from dataModelAddons.tree import Tree
from dataModelAddons.network import Base_Node

#from program.settings import *

from dataModel import Function,Decorator,Frame,Boolean

DEBUG=False
DEBUG_ERROR=True
DEBUG_ERROR_STOP=False
#==========================================================

class Log_Message(Model,Tree,Base_Node):
    
#==========================================================
    I=0

    RESULT=Boolean(default=True)
    TIME_START=Float()
    TIME_STOP=Float(default=None)
    TEXT=String()

    SUCCESS=Function()
    ERROR=Function()
    ERRORS=Function()
    #---------------------------------------------------
    def errors(self):
        for elt in self.all().by_class(Log_Message):
            if elt.RESULT==False:
                yield elt

    #---------------------------------------------------

    def onInit(self):
        "set time"
        self.time_start=time.time()
        if self.name in [None,"None"]:
            self.name=self.__class__.__name__+"_"+str(Log_Message.I)
            Log_Message.I+=1


    #---------------------------------------------------
    def success(self):
        self.result=True
        self.time_stop=time.time()

    #---------------------------------------------------
    def error(self,tb=None, **args):

        self.result=False
        self.update(args)



        if tb is not None:
            for line in tb:
                elt=Log_Traceback(parent=self,filename=line[0],line=line[1],function_name=line[2],text=line[3])

        self.time_stop=time.time()

        if DEBUG_ERROR == True:self.print_on_screen()
    #---------------------------------------------------
    def print_on_screen(self):

            print("")
            print("",str(self.message),str(self.exception))
            print("")
            for elt in self.children.by_class(Log_Traceback):
                if "__python3" in elt.filename:
                    filename="store:/"+elt.filename.split("__python3")[1]
                else:
                    filename=elt.filename
                print("    * ",filename,elt.line,elt.function_name)
                print("             ",elt.text)
                print("")
#==========================================================================

class Log_Traceback(Log_Message):

#==========================================================================

    LINE=Integer()
    FILENAME=String()
    TEXT=String()
    FUNCTION_NAME=String()

#==========================================================================

def execute_log(function,*args,**kwargs):

#==========================================================================
    #print(function,*args,*kwargs)
    msg=new_message(function,*args,**kwargs)

    try:

        response = function(*args,**kwargs)            
        process_response(msg,response)

        msg.success()


    except Exception:

        exc_type, exc_value, exc_traceback = sys.exc_info()
        tb=traceback.extract_tb(exc_traceback )


        msg.error(tb=tb,    exc_type=exc_type,
                            message=exc_value,
                            exception=exc_type)



        if DEBUG_ERROR_STOP == True:exit()
    return msg




#==========================================================================

def new_message(function,*args,**kwargs):

#==========================================================================

    if DEBUG==True:print("XXXX",function,args,kwargs)
    name=function.__name__
    if isinstance(function,Function_Interface)==True:
        node=function.node
        args=args[1:]

        msg=Log_Message(name=name,target="/"+node.path(),
                            args=str(args),kwarg=str(kwargs))

    elif hasattr(function,"__self__"):# and isinstance(function.__self__,Decorator):
        #print(function,name,function.__self__.name)
        #print(function.__self__.name)
        #print(dir(function))
        #exit()
        node=""
        name=function.__self__.name+"_"+name
        msg=Log_Message(   name=name,
                            args=str(args),kwarg=str(kwargs))


    else:
        msg=Log_Message(   name=name,
                            args=str(args),kwarg=str(kwargs))


    #msg.show()
    return msg

#==========================================================================

def process_response(msg,response):

#==========================================================================

    if isinstance(response,Tree) :
        response.parent=msg


    elif isinstance(response,Frame):

        for elt in response:
            process_response(msg,elt)

    elif hasattr(response,"__iter__") and not hasattr(response,"__len__"):

        for elt in response:
            if elt is not None:

                process_response(msg,elt)

    elif response is not None:
        if type(response) in [str]:
            Log_Message(parent=msg,text=response)
        elif type(response)==dict:
            Log_Message(parent=msg,**response)
        else:
            Log_Message(parent=msg,response=response)



#==========================================================================

