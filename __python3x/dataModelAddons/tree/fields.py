from dataModel import *


#=============================================================================

class TreeFrame(Frame):

#=============================================================================

    def keys(self):
        for child in self:
            yield child.name


    #---------------------------------------------------

    def tree(self,recursive=False,cls=None,cls_except=None,separator=".",constructor=None):

        if constructor is None:
            cls=Tree
        else:
            constructor=getClass(constructor)

        if cls is None:
            cls=Tree
        else:
            cls=getClass(cls)

        if cls_except is not None:
            cls_except=getClass(cls_except)

        if recursive == True:
            lst=self.all()
        else:
            lst=self

        lst=lst.by_class(cls,cls_except=cls_except)
        root=constructor()
        for elt in lst:
            root.append(elt.name,separator=separator,node=elt,cls=constructor,setup=False)

        for elt in root.all():
            if hasattr(elt,"node") != True:
                elt.node="None"

        root.tree()
        return root
    #-----------------------------------------------------------
    @frame(Frame)
    def all(self):

        #yield self
        for child in self:
            yield child

            for elt in child.all():
                yield elt


    #---------------------------------------------------

#=============================================================================

class ChildrenFrame(TreeFrame):

#=============================================================================

    def keys(self):
        for child in self:
            yield child.name
    #-----------------------------------------------------------
    @frame(TreeFrame)
    def all(self):

        #yield self
        for child in self:
            yield child

            for elt in child.all():
                yield elt


    #---------------------------------------------------
#=============================================================================

class ParentAttribute(Attribute):

#=============================================================================


    #--------------------------------------------------------------------------

    def modify(self,value):

        "reparent current node"

        self.delete()

        # et node existe
        if  value is not None:
            #nouvel attachement
            value.children.append(self.node)
            self.value=value



    #--------------------------------------------------------------------------

    def delete(self):

        "fonction executée quand l'attibut est supprimé"

        # si attache, detacher
        if self.value is not None:
            self.value.children.remove(self.node)
            self.value=None

    #--------------------------------------------------------------------------

#=============================================================================

class Parent(Field):

#=============================================================================

    Attribute=ParentAttribute

#=============================================================================
