# -*- coding: utf-8 -*-




from .model import Tree
from .structure import Tree_Builder


#==========================================================

def tree(separator="/",reverse=False,Class=Tree):
    
#==========================================================

    #---------------------------------------------------

    def decorated(func):

        def wrapper(self,*args, **kwargs):

            tree=Tree_Builder(Class=Class,build=True,separator=separator,reverse=reverse,parent=self)
            for path,elt in func(self,*args,**kwargs):
                tree[path]=elt
            return 
    
        return wrapper

    return decorated
    #---------------------------------------------------

#==========================================================

def children(Group=Tree,Node=Tree):
    
#==========================================================

    #---------------------------------------------------
    def decorated(func):

        def wrapper(*args, **kwargs):

            tree=Group()

            for elt in func(*args,**kwargs):

                if isinstance(elt,Node):
                    elt.reparenTto(tree)
                elif type(elt) is dict:
                    Node(parent=tree,**elt)
                else:
                    raise Exception("not good type")
            return tree
    
        return wrapper

    return decorated
    #---------------------------------------------------

#==========================================================

def children(Group=Tree,Child=Tree,separator="/",reverse=False):
    
#==========================================================

    #---------------------------------------------------
    def decorated(func):

        def wrapper(*args, **kwargs):

            tree=Tree_Builder(Class=Group,build=True,separator=separator,reverse=reverse)
            child=Tree_Builder(Class=Child,build=True,separator=separator,reverse=reverse)
            

            for elt in func(*args,**kwargs):

                if isinstance(elt,Node):
                    elt.reparenTto(tree)
                elif type(elt) is dict:
                    Node(parent=tree,**elt)
                else:
                    raise Exception("not good type")
            return tree
    
        return wrapper

    return decorated
    #---------------------------------------------------
#==========================================================

