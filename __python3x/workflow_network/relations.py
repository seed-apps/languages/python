
from dataModel import Frame
from mod_program import Iterator,Node_Condition
from .node import *
#===============================================================================

class SelfLoop(Node_Condition):

#===============================================================================

    def onTest(self,node):

        return node.is_self_loop()

#===============================================================================
class TransitiveObjects(Node_Iterator):
#===============================================================================
    def onIter(self,node,uri=None,**args):

        return self.transitive_objects(node,uri)

    def transitive_objects(self,node,uri,done=None):
        
        if done is None:
            done=[]
        
        for elt in Links_Objects(uri,select=node.outputs):

            if not elt in done:
                yield elt
                done.append(elt)
                for elt2 in self.tansitive_objects(elt,uri,done=done):
                    yield elt2

#===============================================================================
class TransitiveSubjects(Node_Iterator):
#===============================================================================

    def onIter(self,node,uri=None,**args):

        return self.transitive_subjects(node,uri)

    def transitive_subjects(self,node,uri,done=None):
        
        if done is None:
            done=[]
        
        for elt in Links_Subjects(uri,select=node.inputs):

            if not elt in done:
                yield elt
                done.append(elt)
                for elt2 in self.tansitive_subjects(elt,uri,done=done):
                    yield elt2

#===============================================================================

class TransitiveNeighbors(Node_Iterator):

#===============================================================================

    def onIter(self,node,uri=None,**args):

        return self.transitive_neighbors(node,uri)

    #-------------------------------------------------------------
    def transitive_neighbors(self,node,uri,done=None):
        
        if done is None:
            done=[]
        
        for elt in Links_Neighbors(uri,selected_node=node.inputs):

            if not elt in done:
                yield elt
                done.append(elt)
                for elt2 in self.tansitive_subjects(elt,uri,done=done):
                    yield elt2
#===============================================================================

class Links_Iterator(Iterator):

#===============================================================================

    pass
    

#===============================================================================

class Triples(Iterator):

#===============================================================================

    def onIter(self,node):
        for link in node:
            yield ( link.source.name,link.name,link.target.name ) 



#NODES
#===============================================================================

class Links_SourceNodes(Links_Iterator):

#===============================================================================

    def onIter(self,node):

        Frame=[]
        for link in node:
            if not link.source in Frame:
                Frame.append(link.source)
                yield link.source

#===============================================================================

class Links_TargetNodes(Links_Iterator):

#===============================================================================

    def onIter(self,node):

        Frame=[]
        for link in node:
            if not link.target in Frame:
                Frame.append(link.target)
                yield link.target

  
  
#===============================================================================

class Links_AllNodes(Links_Iterator):

#===============================================================================

    sources=Links_SourceNodes()
    targets=Links_TargetNodes()

    def onIter(self,node):

        "tous les liens entrants et sortants du Framee"
        self.sources.select(node)
        self.targets.select(node)

        return Frame(self.sources).union(self.targets)
      
#===============================================================================

class Links_InternalNodes(Links_Iterator):

#===============================================================================

    sources=Links_SourceNodes()
    targets=Links_TargetNodes()

    def onIter(self,node):

        "tous les liens entrants et sortants du Framee"
        self.sources.select(node)
        self.targets.select(node)

        return Frame(self.sources).intersection(self.targets)

#===============================================================================

class Links_UpstreamNodes(Links_Iterator):

#===============================================================================

    sources=Links_SourceNodes()
    internals=Links_InternalNodes()

    def onIter(self,node):

        "tous les liens entrants et sortants du Framee"
        self.sources.select(node)
        self.internals.select(node)

        Frame= Frame(self.sources)
        Frame.delete(self.internals)
        return Frame.__iter__()

    

#===============================================================================

class Links_DownstreamNodes(Links_Iterator):

#===============================================================================

    targets=Links_TargetNodes()
    internals=Links_InternalNodes()

    def onIter(self,node):

        "tous les liens entrants et sortants du Framee"
        self.targets.select(node)
        self.internals.select(node)

        Frame= Frame(self.targets)
        Frame.delete(self.internals)
        return Frame.__iter__()

#===============================================================================

class Links_BorderNodes(Links_Iterator):

#===============================================================================

    up=Links_TargetNodes()
    down=Links_InternalNodes()

    def onIter(self,node):

        "tous les liens entrants et sortants du Framee"
        self.up.select(node)
        self.down.select(node)

        return Frame(self.up).union(self.down)

        


#SUBJECTS
#===============================================================================

class Links_Subjects(Links_Iterator):

#===============================================================================
    def onIter(self,node,uri=None,**args):

        for link in node:
            if link.name==uri:
                yield link.source

#===============================================================================

class Links_SubjectList(Links_Iterator):

#===============================================================================

    def onIter(self,node):
        done=[]
        for link in node:
            if link.name not in done:
                yield link.name,Node_Subjects(link.name,node=node)

#OBJECTS
#===============================================================================

class Links_Objects(Links_Iterator):

#===============================================================================

    def onIter(self,node,uri=None,**args):

        for link in node:
            if link.name==uri:
                yield link.target

#===============================================================================
class Links_ObjectList(Links_Iterator):
#===============================================================================

    def onIter(self,node):
        done=[]
        for link in node:
            if link.name not in done:
                yield link.name,Node_Objects(link.name,node=node)


#NEIGHBORS
#===============================================================================
class Links_Neighbors(Links_Iterator):
#===============================================================================


    def onIter(self,node,uri=None,**args):

        done=[]
        for link in node:
            if link.name==uri:
                if link.target not in done:
                    yield link.target
                    done.append(link.target)

                if link.source not in done:
                    yield link.source
                    done.append(link.source)

#===============================================================================
class Links_NeighborList(Links_Iterator):
#===============================================================================

    def onIter(self,node):
        done=[]
        for link in node:
            if link.name not in done:
                yield link.name,NeighborsIterator(link.name,node=node)


