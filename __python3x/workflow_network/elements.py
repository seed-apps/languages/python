
from .relations import *
from mod_program import Iterator,Iterator_Condition
#===============================================================================

class Nodes_Filter(Iterator_Condition):

#===============================================================================

    #RefClass=LinkStore

    #-------------------------------------------------

    def onList(self,select=None,**kwargs):
        return lst

    #-------------------------------------------------

#===============================================================================

class Nodes_InternalLinks(Nodes_Filter):

#===============================================================================

    def onList(self,select=None,**kwargs):

        #pour chaque noeud du groupe
        for node in select:
            #pour les liens qui sortent du noeud
            for link in node.outputs:
                #si la cible est dans le groupe
                if link.target in select:
                    #on ajoute le lien dans le groupe de liens
                    yield link


#===============================================================================

class Nodes_OutputLinks(Nodes_Filter):

#===============================================================================

    def onList(self,select=None,**kwargs):

        #pour chaque noeud du groupe
        for node in select:
            #pour les liens qui sortent du noeud
            for link in node.outputs:
                #si la cible est dans le groupe
                if not link.target in select:
                    #on ajoute le lien dans le groupe de liens
                    yield link

  
#===============================================================================

class Nodes_InputLinks(Nodes_Filter):

#===============================================================================

    def onList(self,select=None,**args):


        #pour chaque noeud du groupe
        for node in select:
            #pour les liens qui sortent du noeud
            for link in node.inputs:
                #si la cible est dans le groupe
                if not link.source in select:
                    #on ajoute le lien dans le groupe de liens
                    yield link

  

#===============================================================================

class Nodes_BorderLinks(Nodes_Filter):

#===============================================================================

    inputsIt=Nodes_InputLinks()
    outputsIt=Nodes_OutputLinks()

    def onList(self,select=None,**args):

        return Group(self.inputsIt(select=select)).union(self.outputsIt(select=select))
        
#===============================================================================

class Nodes_AllLinks(Nodes_Filter):

#===============================================================================

    borders=Nodes_BorderLinks()
    internals=Nodes_InternalLinks()

    def onList(self,select=None,**args):
        return Group(self.borders(select=select)).union(self.internals(select=select))

#===============================================================================

class Nodes_UpstreamlinksNodeStoreComparator(Nodes_Filter):

#===============================================================================

    group1=Nodes_OutputLinks()
    group2=Nodes_InputLinks()

    def onList(self,select=None,group=None,**args):

        self.group1.select(select)
        self.group2.select(group)
        return Group(self.group1).intersection(self.group2)
  
#===============================================================================

class Nodes_DownstreamlinksNodeStoreComparator(Nodes_Filter):

#===============================================================================

    group1=Nodes_InputLinks()
    group2=Nodes_OutputLinks()

    def onList(self,select=None,group=None,**args):

        self.group1.select(select)
        self.group2.select(group)
        return Group(self.group1).intersection(self.group2)

#===============================================================================

class Nodes_Comparator(Nodes_Filter):

#===============================================================================

    group1=Nodes_BorderLinks()
    group2=Nodes_BorderLinks()

    def onList(self,select=None,group=None,**args):
        self.group1.select(select)
        self.group2.select(group)
        return Group(self.group1).intersection(self.group2)

#===============================================================================
#===============================================================================

            #NODES

#===============================================================================
#===============================================================================



#===============================================================================
class Nodes_InputNodes(Nodes_Filter):
#===============================================================================


    def onList(self,select=None,**args):

        inputsIt=Nodes_InputLinks()
        targetsIt=Nodes_TargetNodes()
        self.inputsIt.select(select)
        self.targetsIt.select(self.inputsIt)
        return self.targetsIt

#===============================================================================
class Nodes_OutputNodes(Nodes_Filter):
#===============================================================================

    def onList(self,select=None,**args):


        outputsIt=Nodes_OutputLinks()
        sourcesIt=Nodes_SourceNodes()

        self.outputsIt.select(select)
        self.sourcesIt.select(self.outputsIt)
        return self.sourcesIt.__iter__()

#===============================================================================
class Nodes_BorderNodes(Nodes_Filter):
#===============================================================================

    def onList(self,select=None,**args):

        inputsIt=Nodes_InputNodes()
        outputsIt=Nodes_OutputNodes()


        self.inputsIt.select(select)
        self.outputsIt.select(select)
        return Group(self.inputsIt).union(self.outputsIt)

#===============================================================================
class Nodes_LevelIterator(Nodes_Filter):
#===============================================================================

    def onList(self,select=None,**args):

        inputsIt=Nodes_InputNodesIterator()
        outputsIt=Nodes_OutputNodesIterator()
        internalsIt=Nodes_InternalLinksIterator()

        upstreamsIt=Nodes_UpstreamNodesIterator()
        downstreamsIt=Nodes_DownstreamNodesIterator()
        allnodesIt=Nodes_AllNodesIterator()


        copy=Group(select)

        self.inputsIt.select(select)
        self.outputsIt.select(select)
        self.internalsIt.select(select)

        l_in=self.inputsIt.size()
        l_out=self.outputsIt.size()
        l_inter=self.internalsIt.size()

        if DEBUG==True:
            print( l_in)
            print( l_out)
            print( l_inter)
            exit()

        i=0

        #entrees
        if l_in>0:

            result=list(self.inputsIt)
            yield i,result
            i+=1
            copy.delete(result)

        #les noeuds internes
        if l_inter>0:

            if l_in>0:            
                while len(copy)>0:
                    self.internalsIt.select(copy)
                    self.allnodesIt.select(self.internalsIt)

            if l_out>0:
                pass


        if len(copy)>0:
            raise Exception("algo pourri")

    def get_firsts(self,group):
        self.inputs.select(group)
        result= list(group)
        group.delete(result)
        return result



