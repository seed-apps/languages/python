
from dataModel import *
from dataModelAddons import *
from .interfaces import TreeRunner


#===============================================================

class Code(Model,TreeRunner):
    
#===============================================================

    """
    basic element to build a code tree
    """
    #---------------------------------------------------
    def onInit(self):
        """
        set an automatic name if no name when instanciate object
        """
        self.autoname()

    #---------------------------------------------------
    def onPreSetup(self):
        "actions a realiser avant le setup"
        pass
    #---------------------------------------------------
    def onSetup(self):
        "overload to have a behavior"
        pass
   #---------------------------------------------------
    def onPostSetup(self):
        "overload to have a behavior"
        pass
   #---------------------------------------------------
    def onPreCleanup(self):
        "overload to have a behavior"
        pass
   #---------------------------------------------------
    def onCleanup(self):
        "overload to have a behavior"
        pass
   #---------------------------------------------------
    def onPostCleanup(self):
        "overload to have a behavior"
        pass
    #---------------------------------------------------
    def onCall(self):
        return TreeRunner.onCall(self)
   #---------------------------------------------------
#===============================================================

class Test(Code,Tree_Condition):
    
#===============================================================

    #---------------------------------------------------
    def onCall(self):
        if self.test() == True:
            return TreeRunner.onCall(self)
   #---------------------------------------------------

#===============================================================

class NodeAction(Code,Action_Execution):
    
#===============================================================

    #---------------------------------------------------
    def onCall(self):
        node=self.get_node()
        return Action_Execution.onNextCall(self,node)
   #---------------------------------------------------


#===============================================================

class NodeTest(NodeAction,Node_Condition):
    
#===============================================================

    #---------------------------------------------------
    def onCall(self):
        if self.test(node) == True:
            return TreeRunner.onCall(self)
   #---------------------------------------------------

#===============================================================

