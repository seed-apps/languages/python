from dataModel.models import *
from core.data import Data
from plugins.gui import ViewEvent,ViewObject


#==========================================================

class GtkObject(ViewObject):
    
#==========================================================
    """

    """

    VIEW_CLASS=None
    ATTACH_PARENT=True

    POSITION=String(default="add")#add,start,end in container
    FILL=Boolean(default=False)
    PADDING=Integer(default=0)

    EXPAND=Boolean(default=False)
    VEXPAND=Boolean()
    HEXPAND=Boolean()

    HEIGHT=Integer()

    #-------------------------------------------------        
    def add(self,node):

        if "height" in node.keys() and node.height:
            node.view_object.set_property("height-request",node.height)
        #print(self.path(),node)
        #print(node.class_name,node.position,node.fill,node.expand,node.path())
        if node.position == "start":
            self.view_object.pack_start(node.view_object,node.expand,node.fill,node.padding)
        elif node.position == "end":
            self.view_object.pack_end(node.view_object,node.expand,node.fill,node.padding)                
        elif node.position == "add":
            #self.view_object.set_vexpand(False)
            #self.view_object.set_hexpand(False)

            #print(node.class_name,self.view_object.get_hexpand(),self.view_object.get_vexpand())

            if self.vexpand:
                self.view_object.set_vexpand(self.vexpand)

            if self.hexpand:
                self.view_object.set_hexpand(self.hexpand)

            self.view_object.add(node.view_object)     



    #-------------------------------------------------        
    def onBuildView(self):

        return self.__class__.VIEW_CLASS()

    #-------------------------------------------------        
    def onBuildAttachements(self,view_object,node):

        if self.__class__.ATTACH_PARENT == True:
            if isinstance(self.parent,GtkObject) and self.parent.view_object and self.parent.view_object != self.view_object:
                yield "    GTK ATTACH "
                self.parent.add(self)

        for event in self.children.by_class(ViewEvent):
            yield "     GTK CONNECT "+event.name
            self.view_object.connect(event.name,event.do,"/"+node.path() )

    #-------------------------------------------------        
    def onBuildChildren(self):
        return self.children.by_class(GtkObject).execute("build")

    #-------------------------------------------------        
    def redraw(self):

        if self.parent and isinstance(self.parent,GtkObject):
            yield self.parent.redraw()
        else:

            yield "GTK REDRAW "+self.path()
            yield self.onRedraw()
            #self.view_object.show_all()

    #-------------------------------------------------        
    def onRedraw(self):
        if self.view_object:
            yield "GTK ON_REDRAW "+self.path()
            self.view_object.show_all()
            yield self.children.by_class(GtkObject).execute("onRedraw")        
    #-------------------------------------------------        
    def onDestroy(self):

        print("destroy",self.path())
        if self.view_object and hasattr(self.parent,"view_object"):
            self.parent.view_object.remove(self.view_object)
            print("done",self.path())
        else:
            print( "ERROR GTK REDRAW "+self.class_name +" - "+ self.path() )

    #-------------------------------------------------        

      

#====================================================







 

