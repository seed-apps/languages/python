from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .GtkCurve import GtkCurve

import matplotlib.pyplot as plt

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

#==========================================================

class GtkPlot2D(GtkCurve):
    
#==========================================================
    """

    """
    PLOT=String(default="plot")
    X=Integer()
    Y=Integer()
    LABELS=Boolean(default=False)
    AXES=Boolean(default=True)
    LEGEND=Boolean(default=False)

    #---------------------------------------------------     
    def onGetPlot(self,node,fig):
    #------------------------------------------------------------
        subplot = fig.add_subplot()
        records=node.infos()
        axes=node.get_axes()
        f=getattr(subplot,self.plot)

        if self.y is None:
            f(records[self.x])
        if  self.labels==True:
            f(records[self.y],labels=records[self.x])
        else:
            f(records[self.x],records[self.y])


        if self.axes==True:
            subplot.set_xlabel(axes[self.x].name)  # Add an x-label to the axes.
            subplot.set_ylabel(axes[self.y].name) 

        subplot.set_title(node.name)
        if self.legend:
            subplot.legend(bbox_to_anchor=(1.1, 1), loc='upper left', borderaxespad=0) 
        return subplot
   #---------------------------------------------------
#==========================================================
