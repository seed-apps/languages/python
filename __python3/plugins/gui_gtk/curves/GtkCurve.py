from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from ..GtkObject import GtkObject

from matplotlib.figure import Figure
from numpy import arange, pi, random, linspace
import matplotlib.cm as cm
#from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import matplotlib
matplotlib.use('GTK3Agg')  # or 'GTK3Cairo'
from matplotlib.backends.backend_gtk3 import (
    NavigationToolbar2GTK3 as NavigationToolbar)
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

#import matplotlib.pyplot as plt
#plt.style.use('ggplot')
#plt.style.use(['dark_background',])
#plt.style.use(['seaborn-dark-palette',])
#plt.style.use(['fivethirtyeight',])
#plt.style.use(['seaborn-whitegrid',])
#plt.style.use(['grayscale',])

#print(plt.style.available)

#todo https://matplotlib.org/stable/gallery/user_interfaces/web_application_server_sgskip.html
#==========================================================

class GtkCurve(GtkObject):
    
#==========================================================
    """

    """

    TOOLBAR=Boolean(default=True)

    #---------------------------------------------------     
    def onBuildView(self):
        view_object = Gtk.VBox()
        self.fill=True
        self.expand=True
        self.position="start"


        return view_object
    #-------------------------------------------------        
    def onBuild(self,view_object,node):


        fig = Figure(figsize=(5, 4), dpi=100)
        fig.patch.set_facecolor('lightgrey')
        #fig.patch.set_alpha(0.2)
        ax=self.onGetPlot(node,fig)

        ax.patch.set_facecolor('antiquewhite')
        #ax.patch.set_alpha(0.0510)

        canvas = FigureCanvas(fig)

        if self.toolbar==True:
            toolbar = NavigationToolbar(canvas, view_object)
            view_object.pack_start(toolbar, False, False, 0)


        view_object.pack_start(canvas, True, True, 0)



    #---------------------------------------------------     
    def onGetPlot(self,node,fig):

        ax = fig.add_subplot(111, projection='polar')

        N = 20
        theta = linspace(0.0, 2 * pi, N, endpoint=False)
        radii = 10 * random.rand(N)
        width = pi / 4 * random.rand(N)

        bars = ax.bar(theta, radii, width=width, bottom=0.0)

        for r, bar in zip(radii, bars):
            bar.set_facecolor(cm.jet(r / 10.))
            bar.set_alpha(0.5)

        #ax.plot()
        return ax

   #---------------------------------------------------
#==========================================================
