from dataModel.models import *
from ..GtkObject import GtkObject
from .GladeEvent import GladeEvent

from dataModel.Store import Store

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk
from gi.repository.WebKit2 import WebView, Settings


import os

GLADE_MODELS=os.path.dirname(__file__)

#==========================================================

class GladeFile(GtkObject):
    
#==========================================================
    """

    """
    SYSTEM_FILE=None
    TEXT=String()
    TITLE=String()
    #-----------------------------------------------------     
    def onBuildView(self):

        if self.__class__.SYSTEM_FILE:
            f=self.__class__.SYSTEM_FILE
        else:
            p=self.ancestors.by_class("ViewSystem")[0].model_path
            f=p+"/"+self.text

        self.builder = Gtk.Builder()
        self.builder.add_from_file(f)
        view_object = self.builder.get_object(self.name)

        #self.view_object.set_title(self.title)
        #view_object.connect('delete-event', Gtk.main_quit)
        
        events=dict()
        for event in self.descendants.by_class(GladeEvent):
            events[event.name]=event.call
        self.builder.connect_signals(events)

        return view_object

    #-----------------------------------------------------     
    def get_builder(self):
        return self.builder
    #----------------------------------------------------- 

#==========================================================
