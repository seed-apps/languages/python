from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#==========================================================

class GtkToggleBar(GtkObject):
    
#==========================================================
    """

    """

    #-------------------------------------------------
    def make(self,data,output,**args):
        self.view_object = Gtk.Box(spacing=6)
        self.buttons=list()
        self.content=None
        
        for elt in self.children.by_class(Gtk_Item):
            button = Gtk.ToggleButton(label=elt.name)
            self.buttons.append(button)
            button.connect("toggled", self.onSelect, elt.name)
            self.view_object.pack_start(button, False, False, 0)

        self.parent.add(self)

    #-------------------------------------------------        
    def onSelect(self, button, name,*args):

        if button.get_active():
                
            print("select",name)
            for elt in self.buttons:
                if elt != button:
                    elt.set_active(False)

            button.set_active(True)

            content=self.find(name)
            content.select(self.selected)
            content.make()
            self.add(content)
            self.view_object.show_all()
            #self.content=data.view_object
        else:
            print("unselect",name)  
    #------------------------------------------------
#==========================================================
