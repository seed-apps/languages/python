from dataModel.models import *
from core.program import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk
import console
#==========================================================

class GtkSearch(GtkObject):
    
#==========================================================
    """

    """
    PART=String()
    TEXT=String()

    #-------------------------------------------------
    def onBuildView(self):
        self.select=Select(parent=self)
        self.select.setup()
        view_object = Gtk.SearchEntry()
        #self.fill=True
        #self.expand=True
        view_object.connect("key-release-event", self._on_key_release)
        #view_object.show()
        return view_object

    #-------------------------------------------------
    def onBuild(self,view_object,node):
        
        self.select.set_selection([node,])

    #-------------------------------------------------
    def _on_key_release(self, widget, event):
        keyname = Gdk.keyval_name(event.keyval)
        text=self.view_object.get_text()
        #print(keyname)
        if keyname in ['slash','asterisk','Return',] or (keyname=='bracketright' and "[" in text.split("/")[-1]):

            self.select.select=text
            part=self.get_part(self.part)
            part.clear()

            i=0
            for elt in self.select.get_selection():
                self.event_message(result=self.call_view(self.text,elt,part))
                if i>30:break
                i+=1

            part.build()
            part.redraw()
        """
        keyname = Gdk.keyval_name(event.keyval)
        if keyname == 'Escape':
            self.searchbar.set_visible(True)
            self.searchbar.set_search_mode(True)
        if event.state and Gdk.ModifierType.CONTROL_MASK:
            if keyname == 'f':
                self.searchbar.set_search_mode(True)
"""
    #-------------------------------------------------        

#==========================================================

