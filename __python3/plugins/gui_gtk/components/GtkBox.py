from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#==========================================================

class GtkBox(GtkObject):
    
#==========================================================
    """

    """
    H=Boolean(default=False)

    #-------------------------------------------------
    def onBuildView(self):
        if self.h == True:
            view_object = Gtk.HBox()
        else:
            view_object = Gtk.VBox()
        return view_object
    #-------------------------------------------------        

#==========================================================
