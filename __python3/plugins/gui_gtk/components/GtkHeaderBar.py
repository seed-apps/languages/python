from dataModel.models import *
from ..GtkObject import GtkObject
from .GtkButton import GtkButton
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#==========================================================

class GtkHeaderBar(GtkObject):
    
#==========================================================
    """

    """
    VIEW_CLASS=Gtk.Label
    TEXT=String(default="ok")

    #-------------------------------------------------
    def onBuildView(self):

        view_object = Gtk.HeaderBar()
        view_object.set_show_close_button(True)
        return view_object

    #-------------------------------------------------        
    def onBuild(self,view_object,node):
        view_object.props.title = self.text

    #-------------------------------------------------        
    def add(self,node):
        if isinstance(node,GtkButton):
            if node.position == "start":
                self.view_object.pack_start(node.view_object)
            elif node.position == "end":
                self.view_object.pack_end(node.view_object)                
        else:
            GtkObject.add(self,node)
#==========================================================
