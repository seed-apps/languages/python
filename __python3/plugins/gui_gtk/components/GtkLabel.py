from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#==========================================================

class GtkLabel(GtkObject):
    
#==========================================================
    """

    """
    VIEW_CLASS=Gtk.Label
    TEXT=String()


    #-------------------------------------------------
    def onBuild(self,view_object,node):
        view_object.set_label(self.text%node)
    #-------------------------------------------------
#==========================================================
