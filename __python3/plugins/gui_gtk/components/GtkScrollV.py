from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#==========================================================

class GtkScrollV(GtkObject):
    
#==========================================================
    """

    """
    #-------------------------------------------------
    def onBuildView(self):
        view_object = Gtk.ScrolledWindow()
        self.fill=True
        self.expand=True
        view_object.set_vexpand(True)
        view_object.set_hexpand(True)
        return view_object
    #-------------------------------------------------
    def add(self,node):
        self.view_object.add_with_viewport(node.view_object) 

    #-------------------------------------------------        

#==========================================================
