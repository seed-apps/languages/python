from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk,Gio
from gi.repository.WebKit2 import WebView, Settings
import console
#==========================================================

class GtkNodeProperties(GtkObject):
    
#==========================================================
 
    VIEW_CLASS=Gtk.VBox

   #---------------------------------------------------
    def onBuild(self,view_object,node):


        doc=node.__class__.__help__()
        self.onBuildCls(view_object,node,node.__class__)
        for elt in doc["bases"]:
            self.onBuildCls(view_object,node,getClass(elt))

        frame=Gtk.Frame(label="others")
        view_object.add(frame)
        box=Gtk.ListBox()
        frame.add(box)
        for k,v in node.getNonAttributeDict():
            self.text_line(box,k,v)
            

   #---------------------------------------------------
    def onBuildCls(self,view_object,node,cls):
        doc=cls.__help__()
        if "fields" in doc.keys():

            frame=Gtk.Frame(label=cls.__name__)
            #view_object.add(frame)
            view_object.pack_start(frame, False, False, 12)

            box=Gtk.ListBox()
            frame.add(box)



            for k in doc["fields"].keys():
                self.text_line(box,k,node[k])


   #---------------------------------------------------
    def text_line(self,view_object,k,v):

        string=k
        sep=" - "
        if isinstance(v,Frame) :
            string+=sep+"Frame("+str(len(v))+")"
        elif type(v) in [list,]:
            string+=sep+"list("+str(len(v))+")"
        elif type(v) in [dict,]:
            string+=sep+"dict("+str(len(v))+")"
        elif isinstance(v,Model) :
            string+=sep+"Model("+v.path()+")"
        else:
            t=str(type(v))
            string+=sep+str(v)

        listboxrow = Gtk.ListBoxRow()
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                  spacing=6)

        label_ = Gtk.Label(label=string)
        label_.get_style_context().add_class("config-list-box-label")
        box.pack_start(label_, False, False, 12)
        listboxrow.add(box)
        view_object.add(listboxrow)

#==========================================================

class GtkNodeHelp(GtkObject):
    
#==========================================================
 
    VIEW_CLASS=Gtk.VBox

   #---------------------------------------------------
    def onBuild(self,view_object,node):




        data=node.__class__.__help__()
        #console.add(data)

        self.onBuildClass(view_object,node.__class__)
        for cls in data["bases"]:
            self.onBuildClass(view_object,getClass(cls))
            


   #---------------------------------------------------
    def onBuildClass(self,view_object,cls):


        data=cls.__help__()

        frame=Gtk.Frame(label=cls.__name__)
        view_object.add(frame)
        #self. text_line(view_object,cls.__name__)
        self. text_line(frame,data["doc"])
        self.text_line(view_object,"")
        if "fields" in data.keys():
            for k,v in data["fields"].items():
                self.text_line(view_object,"       * var *    "+k)

        if "functions" in data.keys():
            for k,v in data["functions"].items():
                self.text_line(view_object,"       * f()  *    "+k)

        if "callbacks" in data.keys():
            for k,v in data["callbacks"].items():
                self.text_line(view_object,"       * call *    "+k)

        self.text_line(view_object,"")
   #---------------------------------------------------
    def onBuildAttr(self,view_object,data):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                  spacing=6)

        #widget.set_valign(Gtk.Align.CENTER)
        label_ = Gtk.Label(label=cls.__name__)
        label_.get_style_context().add_class("config-list-box-label")
        box.pack_start(label_, False, False, 12)
        #box.pack_end(widget, False, False, 12)
        listboxrow = Gtk.ListBoxRow()
        listboxrow.get_style_context().add_class("config-list-box-row")
        listboxrow.add(box)
        view_object.add(listboxrow)

   #---------------------------------------------------
    def text_listbox(self,view_object):
        lst = Gtk.ListBox() 
        lst.set_selection_mode(Gtk.SelectionMode.NONE)
        view_object.add(lst)
        return lst

   #---------------------------------------------------
    def text_line(self,view_object,data):
        listboxrow = Gtk.ListBoxRow()
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                  spacing=6)

        label_ = Gtk.Label(label=data)
        label_.get_style_context().add_class("config-list-box-label")
        box.pack_start(label_, False, False, 12)
        listboxrow.add(box)
        view_object.add(listboxrow)

   #---------------------------------------------------



#==========================================================
