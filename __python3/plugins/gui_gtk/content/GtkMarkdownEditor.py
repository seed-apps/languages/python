from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from ..GtkObject import GtkObject
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0') 
from gi.repository import Gtk,Gio
from gi.repository.WebKit2 import WebView, Settings
import markdown
from core import text_parser

#==========================================================

class GtkMarkdownEditor(GtkObject):
    
#==========================================================
    """

    """

   #---------------------------------------------------
    def onBuildView(self):
        """
        """
        self.fill=True
        self.expand=True
        self.position="start"

        view_object = Gtk.ScrolledWindow() 

        return view_object

   #---------------------------------------------------
    def onBuild(self,view_object,node):



        box = Gtk.VBox(False) 



        tv = Gtk.TextView()
        text_buffer = tv.get_buffer() 

        bbox = Gtk.HBox() 
        #bbox.set_layout(Gtk.BUTTONBOX_START) 
        #we need buttons! 
        savebutton = Gtk.Button("Save") 
        savebutton.connect("clicked", self.save,node,text_buffer) 
        #markdownbutton.connect("clicked", self.markdown,text_buffer,web_view) 
        #add the buttons to the bbox 
        bbox.pack_start(savebutton,False,False,0) 
        #bbox.pack_start(markdownbutton,False,False,0) 
        #add the bbox to the box 
        box.pack_start(bbox,False,False,0) 




        input_scroll = Gtk.ScrolledWindow() 
        input_scroll.add_with_viewport(tv) 
        box.add(input_scroll) 
        view_object.add(box) 
        self.read_default_file(node,text_buffer) 


    def markdown(self,widget,text_buffer,web_view): 
        content = self.get_buffer_text(text_buffer)
        content=text_parser.parse(self.view_object,content) 
        content = markdown.markdown(content) 
        web_view.load_html(content,"file:///") 


    def read_default_file(self,node,text_buffer): 
        try: 
            f = open(node.text,"r") 
            text = f.read() 
            text_buffer.set_text(text) 
            f.close() 
        except: 
            pass 


    def save(self,widget,node,text_buffer): 
        #get the text 
        text = self.get_buffer_text(text_buffer) 
        f = open(node.text,"w") 
        f.write(text) 
        f.close() 

    def get_buffer_text(self,text_buffer): 
        start_iter = text_buffer.get_start_iter() 
        end_iter = text_buffer.get_end_iter() 
        text=text_buffer.get_text(start_iter,end_iter,False) 
        return text 
   #---------------------------------------------------
#==========================================================

class GtkMarkdownEditor2(GtkObject):
    
#==========================================================
    """

    """

   #---------------------------------------------------
    def onBuildView(self):
        """
        """
        self.fill=True
        self.expand=True
        self.position="start"

        view_object = Gtk.ScrolledWindow() 

        return view_object

   #---------------------------------------------------
    def onBuild(self,view_object,node):



        box = Gtk.VBox(False) 

        pane = Gtk.HPaned() 


        web_view = WebView() 
        ws = web_view.get_settings() 
        ws.set_property('enable-plugins',False) 
        web_view.set_settings(ws) 

        out_scroll = Gtk.ScrolledWindow() 
        out_scroll.add(web_view) 

        tv = Gtk.TextView()
        text_buffer = tv.get_buffer() 

        bbox = Gtk.HBox() 
        #bbox.set_layout(Gtk.BUTTONBOX_START) 
        #we need buttons! 
        savebutton = Gtk.Button("Save") 
        savebutton.connect("clicked", self.save,node,text_buffer) 
        markdownbutton = Gtk.Button("_Markdown",use_underline=True) 
        markdownbutton.connect("clicked", self.markdown,text_buffer,web_view) 
        #add the buttons to the bbox 
        bbox.pack_start(savebutton,False,False,0) 
        bbox.pack_start(markdownbutton,False,False,0) 
        #add the bbox to the box 
        box.pack_start(bbox,False,False,0) 




        input_scroll = Gtk.ScrolledWindow() 
        input_scroll.add_with_viewport(tv) 



        pane.pack1(input_scroll,True) 

        pane.add(out_scroll) 
        box.add(pane) 

        view_object.add(box) 

        self.read_default_file(node,text_buffer) 


    def markdown(self,widget,text_buffer,web_view): 
        text = self.get_buffer_text(text_buffer) 
        mdtext = markdown.markdown(text) 
        web_view.load_html(mdtext,"file:///") 


    def read_default_file(self,node,text_buffer): 
        try: 
            f = open(node.text,"r") 
            text = f.read() 
            text_buffer.set_text(text) 
            f.close() 
        except: 
            pass 


    def save(self,widget,node,text_buffer): 
        #get the text 
        text = self.get_buffer_text(text_buffer) 
        f = open(node.text,"w") 
        f.write(text) 
        f.close() 

    def get_buffer_text(self,text_buffer): 
        start_iter = text_buffer.get_start_iter() 
        end_iter = text_buffer.get_end_iter() 
        text=text_buffer.get_text(start_iter,end_iter,False) 
        return text 
   #---------------------------------------------------
#==========================================================
