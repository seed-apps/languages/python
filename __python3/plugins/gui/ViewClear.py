from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewOperation import ViewOperation

#======================================================

class ViewClear(ViewOperation):

#======================================================


    #-------------------------------------------------        
    def onPreMake(self,data,output,**args):

        yield output.clear()
        yield "VIEW CLEAR "+output.path()



#======================================================

