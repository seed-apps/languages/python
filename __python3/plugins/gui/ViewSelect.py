from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewOperation import ViewOperation

#======================================================

class ViewSelect(ViewOperation):

#======================================================


    #-------------------------------------------------
    def onSetArgs(self,data,obj,**args):
        return data,obj,args
    #-------------------------------------------------        
    def onMakeChildren(self,data,obj,**args):
        yield "     SELECT on "+obj.path()
        for child in self.children.by_class(Run):
            if isinstance(child,Handler):
                child.set_selection([obj,])
                yield child.call()

            elif isinstance(child,Run):
                yield child.call()

   #---------------------------------------------------

#======================================================

