from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewOperation import ViewOperation
from .ViewInterface import ViewInterface
#======================================================

class ViewPart(ViewOperation):

#======================================================
    PART=String()
    PART_CLS=String()

    #-------------------------------------------------
    def onSetArgs(self,data,obj,**args):

        if self.part_cls:
            attrs=dict(self.get_attr(data))

            if not "name" in attrs.keys()and  self.part:
                attrs["name"]=self.part%data

            obj=getClass(self.part_cls)(parent=obj,data_object=data,**attrs)


        elif self.part and isinstance(obj,ViewInterface):
            obj=obj.get_part(self.part%data)
            obj.data_object=data
            attrs=dict(self.get_attr(data))
            obj.update(attrs)
        elif self.part:
            print("ERROR NO PART",self.part,self.select,self.part_cls)

        return data,obj,args

    #-------------------------------------------------
    def get_attr(self,data):
        for k,v in self.items():
            if k.startswith("node_"):
                yield k.replace("node_",""),v%data

#======================================================

