from dataModel.models import *
from core.program import *
from plugins.gui import ViewInterface
from core.data import Data
#==========================================================

class ViewObject(Data,ViewInterface):
    
#==========================================================
    """

    """

    VIEW_CLASS=None
    ATTACH_PARENT=True

    BUILD_DONE=Boolean(default=False)

    DATA_OBJECT=Field()
    VIEW_OBJECT=Field()

    BUILD=Function(ReturnMerge())
    REDRAW=Function(ReturnMerge())

    #-------------------------------------------------        
    def get_node(self):
        if self.data_object:
            return self.data_object
        elif self.parent and isinstance(self.parent,ViewObject):
            return self.parent.get_node()
        else:
            return self.root

    #-------------------------------------------------        

    def add(self,node):
        pass
    #-------------------------------------------------        
    def redraw(self):
        pass

    #-------------------------------------------------
    def build(self):

        if self.build_done==False:
            yield "    BUILD "+self.class_name+" : "+self.path()
            self.data_object=self.get_node()
            self.view_object=self.onBuildView()


            yield self.onBuild(self.view_object,self.data_object)
            yield self.onBuildAttachements(self.view_object,self.data_object)
            self.build_done=True

        yield self.onBuildChildren()

    #-------------------------------------------------        
    def onBuildView(self):
        return self.__class__.VIEW_CLASS()
    #-------------------------------------------------        
    def onBuild(self,view_object,node):
        return 
    #-------------------------------------------------        
    def onBuildChildren(self):

        return self.children.by_class(ViewObject).execute("build")
    #-------------------------------------------------        
    def onBuildAttachements(self,view_object,node):
        return 
#==========================================================




