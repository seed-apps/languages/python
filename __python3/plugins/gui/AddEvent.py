from dataModel.models import *
from core.program import *
from core.data import *
from dataModel.Store import Store
from .ViewOperation import ViewOperation
import console
#======================================================

class ViewEvent(Data):

#======================================================

    DATA_OBJECT=Field(record=False)
    VIEW=String(record=False)


    #-------------------------------------------------        
    def do(self,*args,**kwargs):

        """
        """

        return self.onDo(*args,**kwargs)
    #-------------------------------------------------        
    def onDo(self,*args,**kwargs):

        """

        """
        if self.data_object is None:
            self.data_object=self.parent.data_object
        result= self.parent.call_view(self.view,self.data_object,self.parent)
        self.parent.event_message(message="EVENT "+self.view,obj=self.data_object.path(),result=result)
   #---------------------------------------------------



#======================================================

class AddEvent(ViewOperation):

#======================================================
    Text=String()

    #-------------------------------------------------        
    def onPreMake(self,data,obj,**args):

        """
        """
        event=ViewEvent(parent=obj,data_object=data,name=self.name,view=self.text%data)

   #---------------------------------------------------




#======================================================

