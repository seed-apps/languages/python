from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewInterface import ViewInterface
import os

#==========================================================

class ViewSystem(Builder,ViewInterface):
    
#==========================================================
    """

    """
    MODEL_PATH=String()
    MAIN_VIEW=String()
    RESULTS=List()

    #-------------------------------------------------        
    def load_model(self,filename):

        print(name)
        content = self.ask(url=self.model_path+"/"+filename)
        content.parent=self
        return content.children[1]
        
    #-------------------------------------------------
    def event_message(self,**message):
        self.results.append(message)

    #-------------------------------------------------


    def onCall(self):
        """
        """
        yield "VIEWSYSTEM start"

        if self.root_obj is None:
            self.root_obj=self.get_new_obj()
            self.root_obj.parent=self

        node=self.find(self.main_view)
        node.root_obj=self.root_obj
        node.set_selection(self.get_selection())

        yield node.call()
        yield self.onBuild(self.root_obj)


    #-------------------------------------------------

    def onCallChildren(self):
        pass

    #-------------------------------------------------

    def onPostCleanup(self):
        return self.results

   #---------------------------------------------------

#==========================================================
