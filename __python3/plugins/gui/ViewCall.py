from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewPart import ViewPart
from .ViewOperation import ViewOperation
#======================================================

class ViewCall(ViewOperation):

#======================================================

    TEXT=String()

    def onPreMake(self,data,output,**args):
        yield "    VIEW CALL "+str(self.text%data)
        yield self.call_view(self.text%data,data,output)

#======================================================
