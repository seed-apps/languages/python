from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .ViewOperation import ViewOperation
#======================================================

class ViewRedraw(ViewOperation):

#======================================================


    def onPreMake(self,data,output,**args):
        yield "VIEW REDRAW "+output.path()
        yield output.build()
        output.redraw()
#======================================================


