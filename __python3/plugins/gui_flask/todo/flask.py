

import jinja2
from jinja2 import Environment, FileSystemLoader
from flask import Flask,render_template,request,redirect
from flask import Response
from flask import jsonify,send_from_directory
from flask_cors import CORS
from flask import Response


from dataModel import *
from mod_program import *
from IoNode import *
from xmlscript import tree_to_string




#============================================================

class IoFlask_Shutdown(IoFlask_Method):

#============================================================


    def onProcessRequest(self,request):

        func = request.flask_request.environ.get('werkzeug.server.shutdown')
        if func:
            func()
            #print("server stop")
        request.done=True
        request.response= self.redirection("/")



#===================================================================

class IoFlask_Node(Data):

#===================================================================

#===================================================================

class IoFlask_Module(IoServer_Module):




