from ..api import *
#============================================================

class Api_Shutdown(Api_Method):

#============================================================


    def onResponse(self,request,**args):

        from flask import Flask,render_template,request
        func = request.environ.get('werkzeug.server.shutdown')
        if func:
            func()
            #print("server stop")
        return self.redirection("/")

#============================================================

