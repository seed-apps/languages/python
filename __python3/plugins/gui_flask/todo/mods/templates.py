from ..api import *
from ..node import *
from ..modules import *
import os
#============================================================

class Template_Mod(Api_Module):

#============================================================
    FILE_PATH=String()
    #----------------------------------------------------------------
    def onSetup(self):
        self.__templates=Environment(loader=FileSystemLoader([self.file_path]),auto_reload=True)

    #----------------------------------------------------------------
    def test(self,template):
        return os.path.exists(os.path.join(self.file_path,template))
    #----------------------------------------------------------------
    def make_template(self,template,**args):
        #print(args)
        template_node = self.__templates.get_template(template)
        content= template_node.render(templates=self,
                                    template_name=template,**args)
        return content.replace(2*"\n","")
    #-------------------------------------------------------

#============================================================

class Api_Template(Api_Method):

#============================================================

    TEMPLATE=String(default=None)

    #-------------------------------------------------------
    def onResponse(self,request,module=None,**args):

        return module.make_template(self.template,request=request,**args)

    #-------------------------------------------------------
#============================================================

