from ..FlaskModule import FlaskModule
from dataModel.models import *
#===================================================================

class FlaskLocation(FlaskModule):

#===================================================================

    def onProcessRequest(self,request):
        if request.done==True:return
        path=self.get_path(request)
        if path=="":path="index"
        print(path)
        node=self.find(path)
        if node:
            if node.onTestRequest(request)==True:
                request.module=self
                node.onProcessRequest(request)
            else:
                return
        
#===================================================================

class FlaskTreePath(FlaskModule):

#===================================================================

    ROOT_PATH=String(default="/")

    def onProcessRequest(self,request):
        if request.done==True:return
        root=self.find(self.root_path)
        path=self.get_path(request)
        print(self.root_path,path)

        if path=="":
            request.tree_node=root
        else:
            request.tree_node=root.find(path)

        for method in self.children.by_class(IoFlask_Method):
            if method.onTestRequest(request):
                request.module=self
                method.onProcessRequest(request)
                if request.done==True:break

#============================================================
