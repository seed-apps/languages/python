from dataModel.models import *
from ..FlaskModule import FlaskModule
import os


from flask import send_from_directory


#===================================================================

class FlaskStatic(FlaskModule):

#============================================================

    FILE_PATH=String()

    #-------------------------------------------------------
    def onProcessRequest(self,request):
        path=self.get_path(request)

        if path !="":
            url_path=self.server_path+"/"+path+"/"
            fullpath=os.path.join(self.file_path,path)

        elif os.path.exists(self.file_path+"/index.html"):
            url_path=self.server_path
            fullpath=self.file_path+"/index.html"
            path="index.html"
        else:
            url_path=self.server_path+"/"
            fullpath=self.file_path
            path=""
        print(fullpath)
        if os.path.isfile(fullpath):
            request.response= send_from_directory(self.file_path,path)
            request.done=True

        elif os.path.isdir(fullpath):
            r=[]
            for elt in os.listdir(fullpath):
                r.append(url_path+elt)
            request.response= dict(result=r)
            request.done=True

    #-------------------------------------------------------

#============================================================
