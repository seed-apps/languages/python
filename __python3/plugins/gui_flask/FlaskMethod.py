from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .FlaskResponse import FlaskResponse

#===================================================================

class FlaskMethod(Object):

#===================================================================
    def onTestRequest(self,request):
        return request.url_method == self.name

    def onProcessRequest(self,request):

        if request.tree_node:
            node=request.tree_node
        else:
            node=self.root
            request.tree_node=self.root

        request.method=self
        for msg in self.onNextCall(node):
            msg.parent=request

        for response in self.children.by_class(FlaskResponse):
            if response.onTestRequest(request):
                response.onProcessRequest(request)
                request.done=True
                break

    def onNextCall(self,node):
        return self.children.by_class("Handler").execute("set_selection",[node,])
        return self.children.by_class("Run").execute("call")

#==========================================================
