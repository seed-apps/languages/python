from dataModel.models import *
from core.program import *
from dataModel.Store import Store

#==========================================================

class FlaskModule(Object):
    
#==========================================================
    SERVER_PATH=String()

    def onPreSetup(self):
        self.name=self.server_path[1:].replace("/","_")

    def get_path(self,request):

        if request.select==self.server_path:
            return ""

        path=request.select[len(self.server_path):]
        if path.startswith("/"):
            path=path[1:]
        return path

    def onTestRequest(self,request):
        return request.select.startswith(self.server_path)

    def onProcessRequest(self,request):
        pass
#==========================================================
