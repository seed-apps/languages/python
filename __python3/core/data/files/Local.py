from dataModel.models import *
from ..Resource import Resource
import os


#==============================================================
class Local(Resource):
#==============================================================

    RELATIVE_PATH=String()
    EXTENTION=String()
    BASENAME=String()
    #-----------------------------------------------------
    def onSetup(self):
        if isinstance(self.parent,Local):
            self.relative_path=self.parent.relative_path+"/"+self.name
        else:
            self.relative_path=""
        #lst = self.text.split("/")
        #self.name=lst[-1]
        if self.exists()==True:
            self.update(**self.infos())
        else:
            return "Local no exists "+self.text
    #-----------------------------------------------------
    def exists(self):
        return os.path.exists(self.text)
    #-----------------------------------------------------

    def infos(self):
        """
        renvoie les infos de la fonction os.stat


        - st_mode - protection bits,
        - st_ino - inode number,
        - st_dev - device,
        - st_nlink - number of hard links,
        - st_uid - user id of owner,
        - st_gid - group id of owner,
        - st_size - size of file, in bytes,
        - st_atime - time of most recent access,
        - st_mtime - time of most recent content modification,
        - st_ctime - platform dependent; time of most recent metadata change on Unix, or the time of creation on Windows)


        https://docs.python.org/2/library/os.html
        """

        headers=("mode", "ino", "dev", "nlink", "uid", "gid", "size", "atime", "mtime", "ctime")
        infos=os.stat(self.text)
        infos=dict(zip(headers,infos))
        return infos
    #-----------------------------------------------------

#==============================================================
