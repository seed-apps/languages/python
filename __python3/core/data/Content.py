from dataModel.models import *
from .Data import Data

#-----------------------------------------------------
def content(func):
#-----------------------------------------------------
    def content_action(self,*args,**kwargs):

        content=Content(mime_type=self.mime_type)
        result=func(self,*args,**kwargs)
        if type(result) in (str,bool,float):
            content.content=result

        elif hasattr(result,"__iter__"):
            for elt in result:
                elt.parent=content
        else:
            content.content=result
        return content
    return content_action
#-----------------------------------------------------


#==============================================================
class Content(Data):
#==============================================================

    MIME_TYPE=String()
    CONTENT=Field()

#==============================================================
