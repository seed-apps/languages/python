
from dataModel.models import *
from dataModel.structures.tree import NamedTree
from dataModel.structures.network import NetworkNode


#==========================================================

class Data(Model,NamedTree,NetworkNode):

#==========================================================
    """
    modèle de base pour décrire les données
    hérite des propriétés de :
        - NamedTree a la topologie d'un arbre
                peut etre manipulé comme un arbre 
                avec les chemins /a/b/
        - NetworkNode a la topologie d'un réseau
    """

    SETUP=Function(ReturnMerge())



    #-----------------------------------------------------
    def setup(self):
        """
        fonction d'initialisation
        """

        yield self.children.by_class('Preload').execute("do")
        yield self.onSetup()
        yield self.children.execute("setup")


    #-----------------------------------------------------
    def onSetup(self):
        """
        surcharger pour avoir un comportement
        """
        pass
    #-----------------------------------------------------

#==========================================================

class Url(Data):

#==========================================================

    TEXT=String()

#==========================================================

