from dataModel.models import *
from dataModel.structures.tree import *
from core.program import *
from dataModel.Store import Store

#==========================================================

class Processor(Model,NamedTree):
    
#==========================================================
    """

    """
    SEPARATOR=String(default="\n")
    #PROCESS=Function(ReturnMerge())

   #---------------------------------------------------
    def process(self,string):
        """
        """
        return self.onProcess(string)

   #---------------------------------------------------
    def onTest(self,string):
        return True
   #---------------------------------------------------
    def onSplit(self,string):
        return string.split(self.separator)
   #---------------------------------------------------
    def onMerge(self,nodes):
        return self.separator.join(nodes)

   #---------------------------------------------------
    def onProcess(self,string):
        """
        """
        if self.onTest(string) ==True:
            nodes=list()
            
            for elt in self.onSplit(string):
                nodes.append(self.onProcessChild(elt))

            return self.onMerge(nodes)
        else:
            return string
   #---------------------------------------------------
    def onProcessChild(self,string):
        """
        """
        for child in self.children.by_class(Processor):
            string= child.process(string)

        return string
   #---------------------------------------------------

#==========================================================

class StringTest(Processor):
    
#==========================================================

    TEXT=String(default="")
    TEST_FUNCTION=String()

   #---------------------------------------------------
    def onTest(self,string):

        if self.test_function:
            return getattr(string,self.test_function)(self.text)
        else:
            return self.text in string
   #---------------------------------------------------

#==========================================================

class Worlds(Processor):
    
#==========================================================
    """

    """
   #---------------------------------------------------
    def onSplit(self,string):
        return string.split(" ")
   #---------------------------------------------------
    def onMerge(self,nodes):
        return " ".join(nodes)

   #---------------------------------------------------


#==========================================================

class Replace(Processor):
    
#==========================================================
    """

    """
    KEYWORD=String()
    TEXT=String()

   #---------------------------------------------------
    def onSplit(self,string):
        if self.keyword in string:
            string=string.replace(self.keyword,"")
            string=self.text.format(string)

        return [string,]
   #---------------------------------------------------
    def onMerge(self,nodes):
        return " ".join(nodes)

   #---------------------------------------------------

#==========================================================
