from dataModel.models import *
from core.program import *
from .Processor import *
from dataModel.structures.tree import TreeSelector
import os

#==========================================================

class TreeProcessor(Processor,TreeSelector):
    
#==========================================================
    """

    """
    TEXT=String()
    PATH_INDICATOR=String(default="$")

   #---------------------------------------------------
    def onTest(self,string):
        return string.startswith(self.path_indicator)

   #---------------------------------------------------
    def onSplit(self,string):
        """
        """
        string=string[1:]
        self.select=string
        
        for elt in self.get_selection():
            yield text.format(elt)
            
   #---------------------------------------------------
#==========================================================

class TreeLineProcessor(TreeProcessor):
    
#==========================================================
    """

    """
    TEXT_SEPARATOR=String(default=" -> ")

   #---------------------------------------------------
    def onSplit(self,string):
        """
        """
        string=string[1:]
        if self.text_separator in string:
            string,text=string.split(self.text_separator)
        else:
            text=self.text

        self.select=string
        
        for elt in self.get_selection():
            yield text.format(elt)
            
   #---------------------------------------------------
#==========================================================

class TreeAttrProcessor(TreeProcessor):
    
#==========================================================
    """

    """

   #---------------------------------------------------
    def onSplit(self,string):
        """
        """
        attr=None
        string=string[1:]
        if "." in string:
            attr=string.split(".")[-1]
            string=string[:-(len(attr)+1)]
            text="{0["+attr+"]}"
        else:
            text=self.text

        self.select=string
        print(attr,text)
        for elt in self.get_selection():
            if attr and attr in elt.keys():
                yield text.format(elt)
            elif attr is None:
                yield text.format(elt)
            else:
                yield "ERROR /"+elt.path()+"attribute "+attr

   #---------------------------------------------------

#==========================================================


#==========================================================


