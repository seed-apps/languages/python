from dataModel import *
from .core import Node,Link,Data
from .main import Manager
from .classes import ClassManager,Class

#==============================================================
class PythonClass(Class):
#==============================================================
    
    CLS_OBJ=Field()

    #-----------------------------------------------------
    def get_instance(self,**args):

        node=self.cls_obj(**args)
        return node
    #-----------------------------------------------------

#==============================================================
class PythonClassManager(ClassManager):
#==============================================================

    #-----------------------------------------------------
    def onSetup(self):
        for name,cls in ALL_CLASSES.items():
            if issubclass(cls,Tree):
                PythonClass(parent=self,name=name,cls_obj=cls)
    #-----------------------------------------------------

#==============================================================

