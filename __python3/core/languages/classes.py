
from dataModel import *
from .core import Node,Link,Data
from .main import Manager

#==============================================================
class IsInstance(Link):
#==============================================================

    pass

#==============================================================
class Instance(Data):
#==============================================================

    pass

#==============================================================
class Attribute(Data):
#==============================================================
    
    DATA_TYPE=Field()

#==============================================================
class Class(Data):
#==============================================================
    
    #-----------------------------------------------------
    def get_instance(self,**args):

        node=Instance(**args)
        for k,v in args.items():
            attr=self.find(k)
            if not attr:
                Attribute(parent=self,name=k,data_type=type(v))
        return node
    #-----------------------------------------------------

#==============================================================
class ClassManager(Manager):
#==============================================================

    #-----------------------------------------------------
    def onSetup(self):
        pass
    #-----------------------------------------------------
    def onQuery(self,path=None,**args):
        cls=self.find(path)
        if not cls:
            cls=Class(parent=self,name=path)
        return cls
    #-----------------------------------------------------

#==============================================================
class InstanceManager(Manager):
#==============================================================

    CLASS_MANAGER=String()
    CLASS_NODE=Field(default=None)

    #-----------------------------------------------------
    def onSetup(self):
        pass
    #-----------------------------------------------------
    def onQuery(self,path=None,**args):

        if self.class_node:
            cls=self.class_node.query(path=path)
        else:
            cls=self.ask(url=self.class_manager+"://"+path)

        inst=cls.get_instance(**args)
        self.link(name="isInstance",source=inst,target=cls,cls=IsInstance)
        return inst
    #-----------------------------------------------------

#==============================================================

