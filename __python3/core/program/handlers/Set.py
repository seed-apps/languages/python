from dataModel.models import *
from ..Object import Object
from .Select import Select
from dataModel.Store import Store

#==========================================================

class Set(Select):
    
#==========================================================
    """
    permet de modifier un attribut de la ressource sélectionnée
    le nom de l'attribut est dans la voariable attr
    la valeur est dans la variable text
    la valeur est evaluée comme un code python !!!TODO: trop facile
    """
    ATTR=String(doc="attribut à modifier")
    TEXT=String(doc="valeur à attribuer")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        modifie la valeur de  l'attribut
        """
        try:
            data=eval(self.text%ressource)
        except:
            data=self.text%ressource
        ressource[self.attr]=data

   #---------------------------------------------------
#==========================================================
