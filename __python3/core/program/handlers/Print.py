from dataModel.models import *
from ..Object import Object
from .Select import Select


#==========================================================

class Print(Select):
    
#==========================================================
    """
    permet d'afficher la variable text

    la varible text permet d'insérer les données de la ressource dans le texte
        node.text="agrrr%(name)s" 
    voir documentation python
    """

    TEXT=String(doc="texte à afficher")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        prépare et renvoie le texte
        """
        command=self.text%ressource
        return command
   #---------------------------------------------------
#==========================================================
