from dataModel.models import *
from ..Object import Object
from .Select import Select
from dataModel.Store import Store

#==========================================================

class Code(Select):
    
#==========================================================
    """
    permet d'executer un code python
    écrit dans la variable text

    la varible text permet d'insérer les données de la ressource dans le texte
        node.text="agrrr%(name)s" 
    voir documentation python
    """

    TEXT=String(doc="code python à executer")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        appelle le code python spécifié dans text
        """
        command=self.text%ressource
        return eval(command)
   #---------------------------------------------------
#==========================================================
