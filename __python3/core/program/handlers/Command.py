from dataModel.models import *
from ..Object import Object
from .Select import Select
from dataModel.Store import Store
import subprocess,shlex
#==========================================================

class Command(Select):
    
#==========================================================
    """
    permet d'executer une commande bash
    écrit dans la variable text

    la varible text permet d'insérer les données de la ressource dans le texte
        node.text="agrrr%(name)s" 
    voir documentation python
    """

    TEXT=String(doc="commande bash à executer")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        appelle la commande bash spécifié dans text
        """
        command=self.text%ressource
        proc = subprocess.Popen(command, shell=True,stdin=subprocess.PIPE,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
        out,err=proc.communicate(input=str.encode(""))
        return dict(command=command,returncode=proc.returncode,out=out,err=err)

   #---------------------------------------------------
#==========================================================
