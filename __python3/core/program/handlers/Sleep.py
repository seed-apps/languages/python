from dataModel.models import *
from ..Object import Object
from .Select import Select
import time

#==========================================================

class Sleep(Select):
    
#==========================================================

    """
    permet d'd'attendre la durée de la variable text

    la variable text permet d'insérer les données de la ressource dans le texte
        node.text="agrrr%(name)s" 
    voir documentation python

    le texte est ensuite évalué comme un code python
        doit retourner un chiffre
        permet les opérations %(a)i+%(b)i
    """

    TEXT=String(doc="texte à afficher")

   #---------------------------------------------------
    def onCallNode(self,ressource):
        """
        attend le temps demandé 
        """
        dt=self.text%ressource
        dt=float(eval(dt))
        time.sleep(dt)
        return "SLEEP "+str(dt)
   #---------------------------------------------------
#==========================================================
