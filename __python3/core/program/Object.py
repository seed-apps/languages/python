
from dataModel.models import *

from .Preload import Preload
from dataModel.structures.tree import NamedTree
from dataModel.structures.network import NetworkNode

#==========================================================

class Object(Model,NamedTree,NetworkNode):

#==========================================================
    """
    arbre d'initialisation (setup) et de nettoyage (cleanup)
    permet de synchroniser les actions pour tout l'arbre
    """

    SETUP_DONE=Boolean(     default=False,
                            record=False,
                            doc="verifie si le setup est fait")

    CLEANUP_DONE=Boolean(   default=True,
                            record=False,
                            doc="verifie si le cleanup est fait")

    ACTIVE=Boolean(         default=True,
                            doc="active et désactive le noeud dans l'arbre d'execution")


    SETUP   =Function(ReturnMerge())
    CLEANUP =Function(ReturnMerge())
    RESET   =Function(ReturnMerge())


    #---------------------------------------------------
    def __need_setup(self):
        "teste si besoin de setup"
        return self.active==True and self.cleanup_done==True and self.setup_done==False

    #---------------------------------------------------
    def __need_cleanup(self):
        "teste si besoin de cleanup"
        return self.active==True and self.cleanup_done==False and self.setup_done==True

   #---------------------------------------------------
    def reset(self):
        """
        remise à zero du noeud et de ses enfants. appele dans l'ordre :
            - cleanup
            - setup
        """
        yield self.cleanup()
        yield self.setup()

   #---------------------------------------------------
    def setup(self):
        """
        appele dans l'ordre:
            - onPreSetup
            - children setup pour tous les enfant ayant la fonction setup
            - onPostSetup
        """


        if self.__need_setup()==True :
            yield self.children.by_class(Preload).execute("do")
            yield self.onPreSetup()
            yield self.children.execute("setup")
            yield self.onPostSetup()

            self.setup_done=True
            self.cleanup_done=False


   #---------------------------------------------------

    def cleanup(self):
        """
        nettoyage de l'arbre
        appele dans l'ordre:
            - onPreCleanup
            - children cleanup pour tous les enfant ayant la fonction cleanup
            - onPostCleanup
        """

        if self.__need_cleanup() ==True:

            yield self.onPreCleanup()
            yield self.children.execute("cleanup")
            yield self.onPostCleanup()

            self.setup_done=False
            self.cleanup_done=True
    
    #---------------------------------------------------
    def onPreSetup(self):
        """
        actions a realiser avant le setup
        surcharger la méthode pour avoir un comportement
        """
        pass
   #---------------------------------------------------


    def onPostSetup(self):
        """
        actions a realiser après le setup
        surcharger la méthode pour avoir un comportement
        """
        pass
   #---------------------------------------------------


    def onPreCleanup(self):
        """
        actions a realiser avant le cleanup
        surcharger la méthode pour avoir un comportement
        """
        pass
    
   #---------------------------------------------------


    def onPostCleanup(self):
        """
        actions a realiser après le cleanup
        surcharger la méthode pour avoir un comportement
        """
        pass

#==========================================================

