import yaml
from dataModel.models import *
from core.data import Data
from .DictReader import DictReader



#==============================================================

class YamlReader(DictReader):

#==============================================================
    """
    lecteur pour les textes bruts
    """
    #-----------------------------------------------------
    def onReadDict(self,content):
        """
        
        """
        return yaml.load(content)

    #----------------------------------------------------------------

#==============================================================
