from dataModel.models import *
from core.data import Data
from .Reader import Reader

#==============================================================    
class Line(Data):
#==============================================================

    TEXT=String()

#==============================================================
class TextReader(Reader):
#==============================================================
    """
    lecteur pour les textes bruts
    """

    #-----------------------------------------------------
    def onRead(self,node):
        """
        attache les lignes Line à la ressource
        """
        content=node.get_content()
        i=0
        for elt in content.split("\n"):
            Line(parent=node,name=i,text=elt)
            i+=1
        return "READ "+node.text
    #-----------------------------------------------------


#==============================================================
