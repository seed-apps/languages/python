from dataModel.models import *
from core.program import Select,Relation
from core.data import Resource,content



#==============================================================

class Reader(Select):

#==============================================================
    """
    essaye de lire la sélection de Ressources
        teste l'extention si renseignée
        ou lee mime type si renseigné
    """
    
    SELECTION_PASSIVE=Boolean(default=False)
    EXTENTIONS=Field(doc="liste d'extentions à prendre en compte")
    MIME_TYPE=String(doc="mimetype à prendre en compte")

    #---------------------------------------------------
    def onSelect(self):
        if self.selection_passive==False:
            Select.onSelect(self) 

    #---------------------------------------------------
    def onCallNode(self,node):
        """
        pour chaque noeud de la sélection execute la fonction onRead
        si 
            est une Ressource
            ressource existe
            test_res est ok
        """

        if isinstance(node,Resource) and node.exists() == True and self.test_res(node)==True:

            return self.onRead(node)
           # Relation(parent=self,source=self,target=node)
   #---------------------------------------------------
    def onCallChildren(self):
        """
        bloque la propagation de call dans les enfants
        """
        pass

    #-----------------------------------------------------
    def test_res(self,res):
        """
        teste la ressoure
            - si extentions : teste l'extention de la ressource
            - si mimetype : teste le mimetype
            - si aucun des deux renvoie vrai !!!TODO

        """
        ext=res.extention
        if ext and self.extentions:
            if ext in self.extentions:
                return True
            return False

        if self.mime_type:
            if res.get_type() == self.mime_type:
                return True
            else:
                return False
       
        return True

    #---------------------------------------------------
    def onRead(self,node):
        """
        lire la ressource et soit
            - la moddifier
            - renvoyer un résultat
        fonction à surcharger pour avoir un comportement
        """
        return node.text
#==============================================================
