from dataModel.models import *
from core.data import Data
from .Reader import Reader

import xml.dom.minidom
        



#========================================================================

class XmlDom(Data):

#========================================================================
    XML_CLASS=String()
    XML_NS=String()
    #----------------------------------------------------------------

#========================================================================

class XmlNs(Data):

#========================================================================

    SHORT=String()
    URI=String()
    NAMESPACE=Field()

#==============================================================
class XmlReader(Reader):
#==============================================================
    """
    lecteur pour les données xml
    """


    #-----------------------------------------------------
    def onRead(self,node):
        """
        crée l'arbre Xml sur la ressource
        """

        xmlnode = xml.dom.minidom.parseString(content)

        #namespaces
        if xmlnode.firstChild.attributes:
            for k,namespace in xmlnode.firstChild.attributes.itemsNS():
                #print(k,namespace)
                uri,short=k
                XmlNs(parent=node,name=short,short=short,uri=uri,namespace=namespace)
        self.parse(xmlnode,node)

        return "READ "+node.text


    #-----------------------------------------------------
    def parse_xml(self,xmlnode,node):


        data=dict()

        if xmlnode.localName:
            data["xml_class"]=self.xmlnode.localName
        else:
            data["xml_class"]=self.xmlnode.nodeName

        if xmlnode.namespaceURI:
           data["self.xml_ns"]=xmlnode.namespaceURI
        
        if xmlnode.attributes:
            data.update(**self.xmlnode.attributes)
        

        text=""
        for xmlchild in self.xmlnode.childNodes:

            if xmlchild.nodeName =="#text":
                text+=xmlchild.data

        text=text.replace("\n","")
        text=text.strip()
        if text !="":
            data["text"]=text

        child=XmlDom(parent=node,**data)

        for xmlchild in self.xmlnode.childNodes:
            if xmlchild.nodeName !="#text":
            else:
                self.parse(xmlnode=xmlchild,parent=child)




    #-----------------------------------------------------

#==============================================================
