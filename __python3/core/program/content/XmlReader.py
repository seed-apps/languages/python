from dataModel.models import *
from core.data import Data
from .Reader import Reader

import xml.etree.ElementTree as ET
#==============================================================    
class Xml(Data):
#==============================================================
    pass

#==============================================================
class XmlReader(Reader):
#==============================================================
    """
    lecteur pour les données xml
    """


    #-----------------------------------------------------
    def onRead(self,node):
        """
        crée l'arbre Xml sur la ressource
        """

        xmlnode=ET.fromstring(node.get_content())

        node=self.parse_xml(xmlnode,parent=node)
        return "READ "+node.text

    #-----------------------------------------------------
    def parse_xml(self,xmlnode,**kwargs):

        if hasattr( xmlnode , 'attrib'): 
            attrib=dict(xmlnode.attrib )
            #attrib=dict(convert_dict(xmlnode.attrib ))
        else:
            attrib=None

        #tag
        if hasattr( xmlnode , 'tag'): 
            tag=xmlnode.tag
            #attrib["__cls__"]=tag
        else:
            tag=None

        #text
        if hasattr( xmlnode , 'text'):

            if xmlnode.text is not None:    
                text=xmlnode.text.strip()
                if text !="":
                    attrib["text"]=text

        #convert attributes
        attrib.update(kwargs)

        for k,v in attrib.items():
            try:
                attrib[k]=v#eval(v)
            except:
                pass

        node=Xml(tag=tag,**attrib)

        for xmlchild in xmlnode:
            self.parse_xml(xmlchild,parent=node)

        return node
    #-----------------------------------------------------

#==============================================================
