from dataModel.models import *
from dataModel.Store import Store


#=======================================================================

class TreeSelector(metaclass=ModelMeta):

#=======================================================================

    """
    permet de sélectionner des ressources grace à un descripteur
    dans la variable text

    langage de selection :
        /a/b/c chemin
        . identité
        .. parent

        * children
        ** descendants
        [a:b] arbre entre les niveaux a et b
       
        [a=1] attribut egal
        [a!=1] attribut different
        [a<1] attribut inferieur
        [a>1] attribut supperieur
        [+a] attribut existe
        [-a] attribut n'existe pas

        [Class] isinstance
        [!Class] not isinstance


    """
    SELECT=String(default=".")
    PARENT_SELECTION=Boolean(default=False)
    #---------------------------------------------------
    def __iter__(self):
        return self.selection

    #---------------------------------------------------
    def onInit(self):
        """
        crée un Store selection
        """
        self.selection=Store()



   #---------------------------------------------------

    def set_selection(self,lst):
        """
        assigne une sélection
        efface la sélection précédente et ajoute la nouvelle
        """
        self.selection.clear()
        self.selection.extend(lst)


   #---------------------------------------------------


    def get_selection(self):
        """
        selectionner des noeud par rapport au descripteur et à la selection
        """

        if self.select is not None and self.select.startswith("/"):
            nodes=[self.root,] 

        elif len(self.selection)>0:
            nodes=self.selection


        elif self.parent_selection==True and self.parent and isinstance(self.parent,TreeSelector):
            nodes=self.parent.selection

        else:
            nodes=[self,]


        if self.select is not None: 

            lst=self.select.split("/")

            for elt in lst:
                nodes=list(self.get_selection_level(nodes,elt))

        return nodes
    #-------------------------------------------------        

    def get_selection_level(self,nodes,selector):

        if selector in (".","",None):
            for elt in nodes:
                yield elt

        elif selector=="..":
            for elt in nodes:
                yield elt.parent

        elif selector=="*":
            for child1 in nodes:
                if child1 is None:
                    print(self.path()+" - None - "+str(nodes))
                    break
                for child in child1.children:
                    yield child

        elif selector=="**":
            for child1 in nodes:
                for child in child1.descendants:
                    yield child

        elif ":" in selector:
            i,j=selector.split(":")
            i=int(i)
            j=int(j)
            for child1 in nodes:
                for child in child1.getRange(i,j):
                    yield child


        elif selector.startswith("[+"):
            s=selector[2:-1]
            for child1 in nodes:
                if s in child1.keys():
                    yield child1

        elif selector.startswith("[-"):
            s=selector[2:-1]
            for child1 in nodes:
                if not s in child1.keys():
                    yield child1

        elif selector.startswith("[") and "!=" in selector:
            s=selector[1:-1]
            k,v=s.split("!=")
            for child1 in nodes:
                if k in child1.keys() and str(child1[k])!=v:
                    yield child1

        elif selector.startswith("[") and "=" in selector:
            s=selector[1:-1]
            k,v=s.split("=")
            for child1 in nodes:
                if k in child1.keys() and str(child1[k])==v:
                    yield child1



        elif selector.startswith("[") and "<" in selector:
            s=selector[1:-1]
            k,v=s.split("<")
            for child1 in nodes:
                if k in child1.keys() and str(child1[k])<v:
                    yield child1

        elif selector.startswith("[") and ">" in selector:
            s=selector[1:-1]
            k,v=s.split(">")
            for child1 in nodes:
                if k in child1.keys() and str(child1[k])>v:
                    yield child1

        elif selector.startswith("[!"):
            cls=selector[2:-1]
            for child1 in nodes:
                if not child1.is_instance(cls):
                    yield child1


        elif selector.startswith("["):
            cls=selector[1:-1]
            for child1 in nodes:
                if child1.is_instance(cls):
                    yield child1

        else:
            for child1 in nodes:
                elt=child1.find(selector)
                if elt is not None:
                    yield elt

    #-------------------------------------------------


#=======================================================================


