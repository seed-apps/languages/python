from dataModel.models import *
from .Tree import Tree



#=======================================================================

class NamedTree(Tree):

#=======================================================================

    #---------------------------------------------------

    NAME=String(eval_expr=False)
    """
    un identifiant pour construire les chemins
    """
    #---------------------------------------------------



    I=0

    TREE_PATH=String(get_f="onGetPath")


    #-----------------------------------------------------
    def onGetPath(self,value):
        return self.path()
   #---------------------------------------------------

    def onInit(self):

        if self.name in [None,"None",""]:
            self.name=self.__class__.__name__+"_"+str(NamedTree.I)
            NamedTree.I+=1
        elif type(self.name) !=str:
            self.name=str(self.name)

    #---------------------------------------------------

    def json_tree(self,**args):


        nodes=list()

        for elt in self.children:
            result=dict()
            result.update(args)
            result["text"]=elt.name
            result["nodes"]=elt.json_tree()
            nodes.append( result )


        return nodes


    #---------------------------------------------------

    def move(self,path=None):
        node=self.find(path)
        if node not in [self,self.parent,None]:
            if node not in self.all():
                self.parent=node


    #---------------------------------------------------
    @frame(Frame)
    def sorted_nodes(self):
        lst=list()
        for elt in self.children:
            lst.append((elt.name,elt))
        lst.sort()
        for name,elt in lst:
            yield elt
    #---------------------------------------------------


    def path(self,separator=u"/",reverse=False,root=None):

        if self.parent is None:
            return ""

        lst=[ elt.name for elt in self.reverse_ancestors]
        lst=lst[1:]
        lst.append(self.name)


        if reverse ==True:
            lst.reverse()

        if root:
            return root+separator.join(lst)
        else:
            return separator.join(lst)
    #-----------------------------------------------------  
    def append(self,elt_path,cls=None,separator="/",reverse=False,**args):
        #print(elt_path)
        if cls is None:
            cls=Tree
        elif type(cls)==str:
            cls=self.getClass(cls)
        node=self
        
        lst=elt_path.split(separator)

        if reverse==True:
            lst.reverse()
        
        for elt in lst:
            #print(elt_path,elt,list(node.children.keys()))
            if elt == "" :
                pass
            elif  node.find(elt) :
                node=node.find(elt)
            else:
                #print("new cls")
                node=cls(parent=node,name=elt)
        node.update(**args)
        return node
    
    #-----------------------------------------------------------  

    def find(self,path,separator=u"/",reverse=False,cls=None,error=True,**args):

        lst=path.split(separator)
        if reverse == True:
            lst=lst.reverse()

        if path.strip().startswith("/"):
            __root=self.root
        else:
            __root=self
            

        

        for name in lst:

            found=False
            if name=='..':
                __root=__root.parent
                found=True

            elif name=='.':
                found=True
                
            elif name=='':
                found=True

            else:

                for child in __root.children:

                    if str(child.name).strip() == name.strip():
                        __root=child
                        found=True
                        break

            if found == False:
                if cls is None:
                    if error == True:
                        return
                        #print("XXX no path %s in %s:%s"%(path,self.__class__.__name__,self.path()))
                        #raise Exception("no path %s in %s:%s"%(path,self.__class__.__name__,self.path()))

                    return None
                else:
                    __root=cls(parent=__root,name=name,**args)


        return __root
    #-----------------------------------------------------------  


#=======================================================================


