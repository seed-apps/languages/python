# -*- coding: utf-8 -*-
"""
le modèle Tree_Node doit etre utilisé avec la classe Model
pour fonctionner ::

   class XXX(Model,Tree):pass
"""


from dataModel.models import *
from .Parent import Parent


#=======================================================================

class Tree(metaclass=ModelMeta):

#=======================================================================
    #__metaclass__ = ModelMeta


    #---------------------------------------------------
    # DOCUMENTATION
    #---------------------------------------------------

    """
    les noeuds sont liés par des relations parents/enfants
    qui permettent de naviguer dans la structure.
    """
    #---------------------------------------------------

    # INIT
    #==================================================

    #---------------------------------------------------

    PARENT=Parent(record=False)

    """
    relation au noeud supperieur, peut etre nul (None) 

    recupérer la variable ::

       node = self.parent

    assigner un autre parent::

        self.parent=node

    détacher du parent ::

        self.parent=None
    """

    #---------------------------------------------------

    CHILDREN=Field(cls=Frame,record=False,create=True)
    """
    liste des enfants ::

       for child in node.children:
           ...

    accès à toutes les fonctions de Frame ::

       node.children.by_class(CLS)
       node.children.by_attr(name="XXX")
       node.children.compare(lst)

    .. warning: restrictions sur append,extend,remove,delete
    """
    #---------------------------------------------------

    DETACH=Function()
    MOVE=Function(path=String())
    #ALL=Function()
    TREE=Function()
    SHOW=Function()
    CLEAR=Function()

    #---------------------------------------------------
    def stats(self):



        data=dict()
        data["descendants"]=len(self.descendants)
        data["leaves"]=len(self.leaves)
        data["deph"]=self.deph


        for i,level in self.getLevels():
            data["level_"+str(i)]=len(level)

        return data

    # DESTROY
    #==================================================

    #---------------------------------------------------

    def onDestroy(self):
        "destroy node and detach it from tree"

        self.parent=None

        for child in list(self.children):
            child.destroy()


    #---------------------------------------------------

    def clear(self):
        "destroy all children and keep node"

        for child in list(self.children):
            child.destroy()

    #---------------------------------------------------

    def detach(self):
        self.parent=None

    #---------------------------------------------------

    def move(self,node):
        if node not in [self,self.parent,None]:
            if node not in self.all():
                self.parent=node

    #---------------------------------------------------



    # ROOT
    #==================================================

    #---------------------------------------------------

    @property
    def root(self):
        """
        accéder à la racine de l'arbre ::

           root=node.root
        """
        if self.parent is None:
            return self

        node = self
        while node.parent:
            node=node.parent

        return node

    #---------------------------------------------------

    @property
    @frame(Frame)
    def ancestors(self):
        """
        accéder aux ancetres du noeud ::

           for ancestor in node.ancestors:
               ...
        """
        node = self
        while node.parent:
            node=node.parent
            yield node

    #---------------------------------------------------
    @property
    def reverse_ancestors(self):
        """
        accéder aux ancetres du noeud ::

           for ancestor in node.ancestors:
               ...
        """
        r=list(self.ancestors)
        r.reverse()
        return r

    #---------------------------------------------------

    def is_inside(self,node):

        return node in self.ancestors

    #---------------------------------------------------

    # TREE RELATIONS
    #==================================================



    #---------------------------------------------------

    @property
    @frame(Frame)
    def leaves(self):
        """
        accéder aux feuilles de l'arbre ::

           for leaf in node.leaves:
               ...
        """
        for node in self.descendants:
            if node.is_leaf:
                yield node


    #---------------------------------------------------

    @property
    @frame(Frame)
    def descendants(self):
        """
        accéder aux descendants ::

           for descendant in node.descendants:
               ...
        """
        for child in self.children:
            yield child
            for elt in child.descendants:
                yield elt

    #-----------------------------------------------------------  


    #SITUATION
    #==================================================


    #---------------------------------------------------
    @property
    def deph(self):
        "renvoie la distance max entre le noeud et la feuille la plus lointaine"

        maximum=0
        level=len(self.ancestors)
        for leaf in self.leaves:
            maximum=max(len(leaf.ancestors)-level,maximum)
        return maximum


    #---------------------------------------------------
    @property
    def is_root(self):
        "retourne True si pas de parent"

        return (self.parent == None)

    #---------------------------------------------------
    @property
    def is_leaf(self):
        "retourne True si pas d'enfants"

        return (len(self.children)==0)



    #ITERATORS
    #==================================================

    #-----------------------------------------------------------  
    @frame(Frame)
    def getLevel(self,level_max,level=0):

        if level_max==level:
            yield self
        else:
            for child in self.children:
                for elt in child.getLevel(level_max,level+1):
                    yield elt

    #-----------------------------------------------------------  

    def getLevels(self):

        for level in range(self.deph+1):
            yield level,list(self.getLevel(level))

    #-----------------------------------------------------------  
    @frame(Frame)
    def getDephArea(self,level_max,level=0):

        if level_max>=level:
            yield self
            for child in self.children:
                for elt in child.getDephArea(level_max,level+1):
                    yield elt

    #----------------------------------------------------------- 
    @frame(Frame)
    def getRange(self,deph_min,deph_max,level=0):

        if level>=deph_min and level<=deph_max:
            yield self

        if level<deph_max:
            for child in self.children:
                for elt2 in child.getRange(deph_min,deph_max,level+1):
                    yield elt2


    #-----------------------------------------------------------  

    def getIntervals(self,divisions):

        depth=self.maxdepth
        level=0

        while level<depth:
            yield level,self.getRange(level,level+divisions-1)
            level+=divisions


    #CONSOLE
    #==================================================

    #---------------------------------------------------
    def tree(self,level=0,cls=None,limit=None):

        if type(cls) == list:
            test=False
            for elt in cls:
                
                if isinstance(self,elt):
                    test=True
            if test == False:
                return        
        elif cls:
            if cls and not isinstance(self,cls):
                return

        if type(limit) == str:
            limit=int(limit)


        if ( limit is not None and level<limit ) or (limit is None):

            if "name" in self.keys():
                name=self.name
            else:
                name=self.__class__.__name__

            string="#"+"    | "*level+" *  "+str(name)#+" - "+self.__class__.__name__
            print( string )
            level+=1

            for elt in self.children:

                elt.tree(level=level,limit=limit,cls=cls)

    #---------------------------------------------------
    def show(self,depth=0,limit=None):

        if type(limit)==str:limit=int(limit)
        if limit is not None and depth >= limit :
            return

        LINE_LEN=70
        CHAR_V="|"
        CHAR_H="_"

        SPACE=5
        
        SPACE_STRING=u" "*SPACE+CHAR_V
        SPACE_LINE=CHAR_H*LINE_LEN


        if "name" in self.keys():
            name=self.name
        else:
            name=self.__class__.__name__


        #print( SPACE_STRING*(deph) )
        print( SPACE_STRING*(depth)+" "*(SPACE+1)+SPACE_LINE )
        print( SPACE_STRING*(depth+1))

        print( SPACE_STRING*depth+SPACE_STRING,self.name,self.__class__.__name__ )
        print( SPACE_STRING*(depth+1)+SPACE_LINE )


        print( SPACE_STRING*depth+SPACE_STRING )

        DICT=dict(self.get_dict())


        for k,v in DICT.items():

            print( SPACE_STRING*depth+SPACE_STRING+"     ",k,v )
        #print( SPACE_STRING*(deph+1)+SPACE_LINE )


        for child in self.children:
            child.show(depth=depth+1,limit=limit)
            
        if len(self.children)>0:print( SPACE_STRING*(depth+1)+SPACE_LINE )
   #---------------------------------------------------

    def children_tree(self,level_max=3,cls=None):
        if cls is None:
            cls=Tree
        else:
            cls=getClass(cls)

        root=cls()
        for elt in self.children.by_class(cls):
            root.append(elt.name,separator=".",node=elt,cls=cls)

        return root


   #---------------------------------------------------
    def get_firsts(self,cls=None,exception=None,root=True):


        if root==True:
            if self.get_firsts_test(self,cls=cls,exception=exception)==True:
                yield self

            else:
                for elt in self.children:
                    for e in elt.get_firsts(cls=cls,exception=exception):
                        yield e
        else:
            for elt in self.children:
                for e in elt.get_firsts(cls=cls,exception=exception):
                    yield e

   #---------------------------------------------------
    def get_firsts_test(self,elt,cls=None,exception=None):

        if elt.is_instance(cls)==True:
            if exception is None:
                return True
            elif  elt.is_instance(exception)!=True:
                return True

        return

    #-------------------------------------------------
    def is_instance(self,cls):
        cls=self.getClass(cls)
        return isinstance(self,cls)
 
    #---------------------------------------------------



#=======================================================================


