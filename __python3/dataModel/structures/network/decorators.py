#===============================================================================

def nodes(func):

#===============================================================================



    def wrapper(*args, **kwargs):
        from .NodeStore import NodeStore
        lst=func(*args,**kwargs)
        lst= NodeStore(lst)
        lst.remove_doublons()
        return lst

    return wrapper

#===============================================================================

def links(func):

#===============================================================================



    def wrapper(*args, **kwargs):
        from .LinkStore import LinkStore
        lst=func(*args,**kwargs)
        lst= LinkStore(lst)
        lst.remove_doublons()
        return lst
    return wrapper

#===============================================================================
