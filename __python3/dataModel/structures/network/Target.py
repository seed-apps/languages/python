from dataModel.models import *
from .NetworkNode import NetworkNode
#=============================================================================

class TargetAttribute(Property):

#=============================================================================


    #--------------------------------------------------------------------------

    def modify(self,value):

        self.delete()

        if type(value) in [str]:
            value=self.node.onAttachTarget(value)

        # et node existe
        if isinstance(value,NetworkNode):
            #nouvel attachement
            value.inputs.append(self.node)


        self.value=value

    #--------------------------------------------------------------------------

    def delete(self):

        # si attache, detacher
        if isinstance(self.value,NetworkNode):
            self.value.inputs.remove(self.node)
        self.value=None

    #--------------------------------------------------------------------------


        
#=============================================================================


class Target(Field):Attribute=TargetAttribute
