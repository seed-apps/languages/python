from dataModel.models import *
from .Source import Source
from .Target import Target
#=======================================================================

class NetworkLink(metaclass=ModelMeta):

#=======================================================================

    #---------------------------------------------------
    # DOCUMENTATION
    #---------------------------------------------------

    """

    """
    #---------------------------------------------------

    # INIT
    #==================================================

    #---------------------------------------------------

    SOURCE=Source()
    """

    """

    #---------------------------------------------------

    TARGET=Target()
    """

    """


    #-----------------------------------------------------------

    def destroy(self):

        self.target=None
        self.source=None

    def onAttachSource(self,value):return value
    def onAttachTarget(self,value):return value

    #QUESTIONS
    #-----------------------------------------------------------

    def same_nodes(self,link):
        self.source=link.source
        self.target=link.target
          
    def is_self_loop(self):
        return self.source == self.target
    
    #TODO: pertinence des questions suivantes? utilisation a voir
    def has_same_source(self,link):
        return link.source == self.source
  
    def has_same_target(self,link):
        return link.target == self.target

    def is_similar(self,link):
        return self.has_same_source(link) and self.has_same_target(link)


#=======================================================================
