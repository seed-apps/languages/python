from dataModel.models import *
from .LinkStore import LinkStore
from .NodeStore import NodeStore


#=======================================================================

class NetworkNode(metaclass=ModelMeta):

#=======================================================================

    """

    """

    INPUTS=Field(cls=LinkStore,record=False,create=True)
    """

    """

    OUTPUTS=Field(cls=LinkStore,record=False,create=True)
    """

    """
    

    def onDestroy(self):


        for elt in list(self.inputs):
            elt.destroy()

        for elt in list(self.outputs):
            elt.destroy()
            
        #del self.inputs
        #del self.outputs

    #-----------------------------------------------------------
    @frame(LinkStore)
    def links(self,name=None,cls=None,inputs=False,outputs=False):
        cls=getClass(cls)
        if outputs==True:
            for link in self.outputs:
                if self.__testlink(link,name=name,cls=cls)==True:
                    yield link

        if inputs==True:
            for link in self.inputs:
                if self.__testlink(link,name=name,cls=cls)==True:
                    yield link

    #-----------------------------------------------------------
    def __testlink(self,link,name=None,cls=None):

        if name is not None:
            if link.name !=name:
                return False
                
        if cls is not None:
            if not isinstance(link,cls):
                return False

        return True
    #-----------------------------------------------------------
    @frame(NodeStore)
    def subjects(self):
        for elt in self.inputs:
            yield elt.source
    #-----------------------------------------------------------
    @frame(NodeStore)
    def objects(self):
        for elt in self.outputs:
            yield elt.target

    #-----------------------------------------------------------
    def get_network(self,recursive=0,result=None,i=0,**args):

        print(i)
        if not result:
            result=dict()
            nodes_ids=dict()
            result["i"]=i
            result["nodes"]=dict()
            result["links"]=list()
            result["nodes_ids"]=dict()
            result["links_ids"]=list()



        for link in self.links(**args):

            if (link) in result["links_ids"]:
                pass

            else:
                link_id=self.get_node_id(link,result)
                source_id=self.get_node_id(link.source,result)
                target_id=self.get_node_id(link.target,result)
                result["links"].append((source_id,link_id,'green'))
                result["links"].append((link_id,target_id,'green'))
        
        if recursive >0:
            self.get_network_tree(result=result)
            for elt in self.children.by_class(Node):

                elt.get_network(recursive=recursive-1,result=result,**args)


        #print(result["links"])
        return result

    #-----------------------------------------------------------
    def get_network_tree(self,result=None,**args):


        source_id=self.get_node_id(self,result)

        for elt in self.children:

            target_id=self.get_node_id(elt,result,color="#B38481")
            result["links"].append((source_id,target_id,'#B38481'))

    #-----------------------------------------------------------
    def get_node_id(self,node,result,color="#659EC7"):
        if (node) in result["nodes_ids"].keys():
            target_id=result["nodes_ids"][(node)]
        else:
            target_id=result["i"]
            result["nodes"][str(result["i"])]=(node.name,color)#path()
            result["nodes_ids"][(node)]=result["i"]
            result["i"]+=1
        return target_id
#=============================================================================

