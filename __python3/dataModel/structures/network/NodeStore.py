from dataModel.models import *
from .decorators import *
#=======================================================================

class NodeStore(Frame):

#=======================================================================

    #STORE
    #=========================================================

    #----------------------------------------------------------
    @nodes
    def by_class(self,cls):
        return Store.by_class(self,cls)

    #----------------------------------------------------------
    @nodes
    def by_attribute(self,attribute):
        return NodeStore(Store.by_attribute(self,attribute))

    #----------------------------------------------------------

    #LINKS
    #=========================================================

    #GENERAL

    #----------------------------------------------------------
    @links
    def all_links(self):
        return self.inputs()+self.outputs()

    #----------------------------------------------------------
    @links
    def inputs(self):

        for node in self:
            for link in node.inputs:
                yield link


    #----------------------------------------------------------
    @links
    def outputs(self):
        for node in self:
            for link in node.outputs:
                yield link

    #----------------------------------------------------------
    @links
    def internals(self):

        for node in self:

            for link in node.outputs:
                if link.target in self:
                    yield link

    #----------------------------------------------------------
    @links
    def borders(self):
        return self.all_links()-self.internals()

    #----------------------------------------------------------
    @links
    def sources_links(self):
        return self.inputs()-self.internals()

    #----------------------------------------------------------
    @links
    def target_links(self):
        return self.outputs()-self.internals()

    #----------------------------------------------------------


#=======================================================================
