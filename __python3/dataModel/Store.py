# -*- coding: utf-8 -*-
"""


"""


#===============================================================================

def output():

#===============================================================================

    def execute(function):

        def execute2(self,*args,**kwargs):

            return Store(function(self,*args,**kwargs))

        return execute2

    return execute

#===============================================================================

class Store(object):

#===============================================================================
    """
    les structures sont des ensembles d'éléments.
    elles  ont plusieurs roles :

       - construire les noeuds 
       - construire les relations
       - gèrer les attributs et les actions

    elles animent les noeuds.

    Les structures Frame ajoutent et modifient les attributs des noeuds
    suivant des règles différentes.

    Une structure se comporte comme une liste de noeuds.
    Elle posséde l'interface permetant de le manipuler comme une liste.

    Les operations de comparaison et de combinaison entre structures.
    """
    def __str__(self):
        return str(self.__nodes)

    #CONSTRUCTEUR / DESTRUCTEUR
    #==============================================================
  
    #--------------------------------------------------------------------------
    def __init__(self,*args):

        """
        créer une structure

           - sans données ::

              node=Frame()

           - avec une liste de noeuds ::

              node=Frame(n1,n2,...)

           - changer le type de liste ::

              node=Frame(l=OtherListClass(n1,n2,...))
        """

        self.__nodes=[]
        if len(args)>0 and hasattr(args[0],"__iter__"):
            lst=args[0]
        else:
            lst=args
            
        self.extend(lst)

    #--------------------------------------------------------------------------
                
    def destroy(self):
        """ 
        supprimer  ::

           store.destroy()
        """
        del self.__nodes
    
    #--------------------------------------------------------------------------

    #ELEMENTS
    #==============================================================

    #--------------------------------------------------------------------------
    #Parcourir la liste
    #--------------------------------------------------------------------------

    def __len__(self):
        """
        renvoie le nombre d'éléments de la liste ::

           n=len(frame)

        """
        i=0
        for elt in self:
            i+=1
        return i

    #--------------------------------------------------------------------------  
    def __iter__(self): 
        """
        iterateur sur la liste ::

           for elt in frame:
                do_something(elt)

        """
        for elt in list(self.__nodes):
            yield elt

    #--------------------------------------------------------------------------
    def __getitem__(self,i):
        """
        renvoie un élément par son indice (position) dans la liste ::

           for i in range(len(frame)):
                do_something(frame[i])
        """
        return self.__nodes[i]

    #--------------------------------------------------------------------------
    #modifier la liste
    #--------------------------------------------------------------------------

    def clear(self):
        """
        vide la liste de tous ses éléments ::

           frame.clear()
        """
        del self.__nodes
        self.__nodes=[]

    #--------------------------------------------------------------------------

    def remove_doublons(self):
        """
        supprime tous les doublons dans la liste ::

           frame.remove_doublons()
        """
        doublons=[]
        ok=[]
        for elt in self:
            if not elt in ok:
                ok.append(elt)
            else:
                doublons.append(elt)
        self.clear()
        self.extend(ok)
        return doublons

    #--------------------------------------------------------------------------
    #avec une liste
    #--------------------------------------------------------------------------

    def extend(self,group):
        """
        étendre la liste avec une autre
        ajoute tout sans tester
        """
        [self.append(node) for node in group]

    #--------------------------------------------------------------------------
    def delete(self,group):
        """
        supprime la liste de la frame
        Exception si un noeud n'est pas dans la liste
        """
        [self.remove(node) for node in group]      

    #--------------------------------------------------------------------------
    #acces aux noeuds
    #--------------------------------------------------------------------------

    def empty(self):
        """
        renvoie vrai si la liste est vide ::

           if frame.empty():
               do
        """
        return len(self)<1
    #-------------------------------------------------------------------------- 

    def first(self):
        """
        renvoie le premier element de la liste, Exception si vide
        """
        return self.__nodes[0]

    #-------------------------------------------------------------------------- 

    def last(self):
        """
        renvoie le dernier element de la liste, Exception si vide
        """
        return self.__nodes[-1]

    #--------------------------------------------------------------------------      
    def __contains__(self,node):
        """
        renvoie True si l'élément est dans la liste
        """
        return self.__nodes.__contains__(node)

    #--------------------------------------------------------------------------
    def append(self,node):
        """
        ajoute un élément à la liste ::

           frame.append(node)

        """
        self.__nodes.append(node)

    #--------------------------------------------------------------------------
    def remove(self,node):
        """
        supprime un élément de la liste ::

           frame.remove(node)

        retourne True s'il a été supprimé
        False s'il n'était pas dans la liste

        """
        if node in self.__nodes:
            self.__nodes.remove(node)
            return True
        return False

    #SEARCH
    #==============================================================

    #--------------------------------------------------------------------------

    def classes(self):
        """
        renvoie la liste des classes des différents éléments
        """
        result=[]
        for node in self.__nodes:
            if type(node) not in result:
                #yield node.__class__
                result.append(type(node))
        return result
        
    #--------------------------------------------------------------------------

    def by_classes(self,**args):  
        """
        renvoie la liste des classes des différents éléments ainsi que la liste des instances
        associées ::

           for cls,lst in frame.by_classes():
               for instance in lst:
                   print isinstance(instance,cls)

        il peut y avoir des doublons entre les liste à cause de l'héritage
        """              
        for cls in self.classes():
               yield cls,self.by_class(cls,**args)

    #--------------------------------------------------------------------------
    @output()
    def by_class(self,cls,strict=False,cls_except=None):
        """
        trier les elements par classe
        retourne la liste des éléments appartenant à la classe cls ::

           for elt in frame.by_class(Class):
               ...
        """

        if strict == False:
            if cls_except is None:
                for elt in self:
                    if isinstance(elt,cls):
                        yield elt
            else:
                for elt in self:
                    #print(isinstance(elt,cls), not isinstance(elt,cls_except))
                    if isinstance(elt,cls) and not isinstance(elt,cls_except):
                            yield elt
        else:
            for elt in self:
                if elt.__class__==cls:
                    yield elt

    #OPERATIONS
    #==============================================================

    #--------------------------------------------------------------------------
    @output()
    def execute(self,command,*args,**kwargs):
        """
        essaye d'executer la methode pour toutes les instances ::

           for elt,result in frame.execute("method"):
               ...
        """

        for elt in self:
            if hasattr(elt,command):
                yield getattr(elt,command)(*args,**kwargs)
            else:
                yield None


    #--------------------------------------------------------------------------
    #OPERATIONS SUR LES LISTES
    #--------------------------------------------------------------------------

    def copy(self):
        """
        crée une nouvelle structure avec les memes noeuds ::

            self.copy() == self -> True
        """
        return Store([elt for elt in self.__nodes])


    #--------------------------------------------------------------------------
    def __add__(self,group):
        """
        créer une nvl structure en ajoutant une liste ::

           frame =  self + lst_of_elts
        """
        result=self.copy()
        result.extend(group)
        return result

    #-------------------------------------------------------------------------- 
    def __sub__(self,group):
        """
        créer une nvl structure en retirant une liste ::

           frame = self - lst_of_elts
        """
        result=self.copy()
        [result.remove(node) for node in group]
        return result
  
    #--------------------------------------------------------------------------

    #OPERATIONS SUR LE GROUPE
    #==============================================================
      
    #--------------------------------------------------------------------------
    def __eq__(self,group):
        """
        deux groupes sont égaux si leurs elements sont les meme ::

            if frame == frame2 :
                ...
        """
    
        for node in group:
            if not node in self:
                return False
      
        for node in self:
            if not node in group:
                return False
      
        return True

    #--------------------------------------------------------------------------
    def compare(self,group):
        """
        comparer deux groupes ::

            same,add,remove=frame.compare(frame2)

        """
    
        same=self.intersection(group)
        remove=self.difference(group)
        add=Store(group).difference(self)
        return (same,add,remove)

    #--------------------------------------------------------------------------

    @output()
    def intersection(self,group):
        "renvoie une liste des éléments commun aux deux listes"
        for node in group:
            if node in self:
                yield node

    #--------------------------------------------------------------------------
    @output()
    def union(self,group):
        "renvoie une liste de tous les éléments des deux listes, sans doublons"
        for node in self:
            yield node
        for node in group:
            if node not in self:
                yield node 

    #--------------------------------------------------------------------------
    @output()
    def difference(self,group):
        "renvoie une liste des élmément de self qui ne sont pas dans group"

        for node in list(self):
            if node not in group:
                yield node 

    #--------------------------------------------------------------------------
    def symetric_difference(self,group):
        "renvoie une liste des éléments n'appartenant qu'à un seul des deux groupes"
        union=self.union(group)
        inter=list(self.intersection(group))
        result=union.difference(inter)
        return result


    #--------------------------------------------------------------------------
    @output()
    def carthesian_product(self,group):
        "renvoie une liste des couples (self,group)"

        group=list(group)

        for node1 in self:
            for node2 in group:
                yield (node1,node2)

    #--------------------------------------------------------------------------


#=============================================================================


  
