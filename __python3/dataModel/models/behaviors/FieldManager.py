# -*- coding: utf-8 -*-
"""
La classe Node est un dictionnaire de données stockée dans une classe et
accessible grace aux fonctions dictionnaire de python.

Il s'agit de stocker des objects et structurer les relation.
La classe abstraite sert de base.


"""

from ..ModelMeta import ModelMeta
from ..Function import Function
from ..instances.Property import Property
#=============================================================================

class FieldManager(metaclass=ModelMeta):

#=============================================================================

    """
    élément de base du système.
    gère les données, les relations et les évènements
    les noeuds stockent des données python de tous types.
    Les données sont dans un dictionnaire associé au noeud.

    """

    GET_DATA=Function()
    GET_DICT=Function()
    GET_RECORDS=Function()
    COPY=Function()


    def __len__(self):
        return self.__data.__len__()

    #--------------------------------------------------------------------------
    #       CREATION/DESTRUCTION
    #--------------------------------------------------------------------------

    def onInitFields(self):

        """
        créer un noeud

           - sans données ::

              node=Node()

           - avec des attributs ::

              node=Node(a=1,b=2)

           - changer le type de dictionnaire ::

              node=Node(data=OrderedDict())

        """

        self.__data = dict()

    #--------------------------------------------------------------------------
    def onDestroyFields(self):

        """ 
        supprimer le noeud et ses relations.
        supprime les données ::

           node.destroy()
        """
        del self.__data



    #--------------------------------------------------------------------------
    #       PATH DICT
    #--------------------------------------------------------------------------
    def keys(self):

        """ 
        liste les chemins ::

           for elt in  node.keys():
               ...      
        """
        return self.__data.keys()

    #--------------------------------------------------------------------------

    def values(self):

        """ 
        liste les valeurs des attributs ::

           for elt in  node.values():
               ...
        """
        return self.__data.values()

    #--------------------------------------------------------------------------

    def items(self):

        """ 
        liste des tuples (chemins,noeud) ::

           for key,elt in  node.items():
               ...   
        """
        return self.__data.items()



    #--------------------------------------------------------------------------

    def __getitem__(self,key):

        """
        lire une donnée ::

            a = node["a"]
         """
        
        return self.__data.__getitem__(key)

    #--------------------------------------------------------------------------

    def __setitem__(self,key,value):

        """
        modifier une donnée ::

           node["a"] = 2
        """
        return self.__data.__setitem__(key,value)

    #--------------------------------------------------------------------------

    def __delitem__(self,key):

        """
        supprimer une donnée ::

            del node["a"]
        """
        self.__data.__delitem__(key)


    #--------------------------------------------------------------------------
    #       REALTIONS WITH OTHER DICTS
    #--------------------------------------------------------------------------

    def copy(self):

        """ 
        copier un noeud ::

           node2=node.copy()

        """
        return self.__class__(**self)

    #--------------------------------------------------------------------------

    def update(self,*args,**kwargs):

        """ 
        mettre à jour le dictionnaire ::

           node.update(node1)
        """
        for d in args:
            FieldManager.update(self,**d)

        for k,v in kwargs.items():
            self[k]=v

    #--------------------------------------------------------------------------

    def compare(self,node):

        """ 
        comparer deux dictionnaires ::

           eq,in1,in2=node.compare(node1)
        """
        pass

    #--------------------------------------------------------------------------

    def get_data(self):

        """
        renvoie le dictionnaire
        """
        return self.__data
    #--------------------------------------------------------------------------

    def get_dict(self,**args):

        """
        renvoie le dictionnaire mis à jour avec les argument args
        """
        data=dict(self)
        data.update(args)

        return data

    #--------------------------------------------------------------------------

    def get_records(self):

        """
        renvoie le dictionnaire mis à jour avec les argument args
        """

        r=dict()
        for k,v in self.__data.items():
            if isinstance(v,Property):
                if v.field.record==True:
                    r[k]=v.access()
            else:
                r[k]=v
        return r

    #-----------------------------------------------------------
    def getInfos(self,*args):
        """
        met a jour le dictionnaire avec les argument args
        """
        lst=[]
        for elt in args:
            lst.append(self[elt])

        if len(lst)==1:
            return lst[0]
        return tuple(lst)


    #-----------------------------------------------------------
    def delInfos(self,*args):
        """
        supprime la liste des clés données par les argument args
        """
        for elt in args:
            del self.__data[elt]

    #-----------------------------------------------------------
#=============================================================================


