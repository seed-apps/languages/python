
from ..ModelMeta import ModelMeta
from ..settings import getClass
from ..Field import Field
from ..Function import Function
#=====================================================================

class ModelManager(metaclass=ModelMeta):

#=====================================================================
    """
    un meta modèle pour gerer les MetaModel
    contient les méthodes de classes pour gérer les modèles:

        - cls.models()
        - cls.getFields()
        - cls.getFunctions()
        - cls.help()
    """


    #--------------------------------------------------------------------------
    @classmethod
    def models(cls,parent=None,interfaces=False):
        """
        liste des modèles utilisées par la classe::

           for model in CLS.models():
              ...
        """
        if parent is not None:
            parent=getClass(parent)
         
            for elt in cls.__mro__:
                if isinstance(elt,ModelMeta) and issubclass(elt,parent):
                    yield elt
        elif interfaces==True:
            from ..model import Model
            for elt in cls.__mro__:
                if isinstance(elt,ModelMeta) and elt != Model and not isinstance(elt,Model):
                    yield elt
        else:
            for elt in cls.__mro__:
                if isinstance(elt,ModelMeta):
                    yield elt
    #--------------------------------------------------------------------------
    
    @classmethod
    def getFields(cls):

        """
        liste des champs utilisées ::

           for name,field in CLS.getFields():
              ...
        """

        for model in cls.models():
            if hasattr(model,"_fields"):
                for k,v in model._fields.items():
                    yield k,v


    #--------------------------------------------------------------------------
    
    @classmethod
    def getFunctions(cls):

        """
        """

        for model in cls.models():
            if hasattr(model,"_functions"):
                for k,v in model._functions.items():
                    yield k,v
    #--------------------------------------------------------------------------
    @classmethod
    def help(cls):

        """
        retourne une aide pour l'objet sous forme de dictionnaires
        imbriqués
        """

        models={}

        for model in cls.models():
            models[model.__name__]=model.__help__()

        return models
    #--------------------------------------------------------------------------



#=====================================================================
