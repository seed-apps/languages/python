import os
from ..ModelMeta import ModelMeta
from ..Function import Function
from ..fields.basics import String
#==========================================================

class FunctionManager(metaclass=ModelMeta):
    
#==========================================================
    """
    MetaModel pour l'execution des fonctions de l'instance
        - parcourir les fonctions
        - executer les fonctions
    """


    ACTIONS=Function()
    QUERY=Function(action=String())
  
    #-------------------------------------------------------
    def actions(self):
        """
        liste les actions de l'instance
        renvoie l'aide pour chaque méthode sour forme de dictionaire
        """
        result=dict()
        for k,v in self.__class__.getFunctions():        
            result[k]=v.__help__()
        return result

    #-------------------------------------------------------
    def query(self,action=None,**kwargs):

        """
        execute l'action demandée et renvoie la réponse
        """

        if hasattr(self,action):
            f=getattr(self,action)
            return f(**kwargs)


   #---------------------------------------------------
#==========================================================

