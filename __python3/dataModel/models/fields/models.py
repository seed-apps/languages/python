from ..Model import Model
from ..Field import Field
from ..Frame import Frame
from ..settings import getClass
import console

#=============================================================================

class ModelField(Field):

#=============================================================================

    #--------------------------------------------------------------------------

    def get_value(self,value):
        return getClass(value)

    #--------------------------------------------------------------------------
#=============================================================================

class NodeField(Field):
#=============================================================================
    CLASS=Model
    #--------------------------------------------------------------------------

    def get_value(self,value):

        if isinstance(value,Model):
            return value

        elif type(value) is dict:

            return getClass(self.cls)(**value)

        console.error(text="value must be a model",attr=self.name)
        raise Exception("ERROR "+node.__class__.__name__+" in function "+self.name)

#=============================================================================
class NodeList(Field):
#=============================================================================
    CLASS=Frame

#=============================================================================

