from .instances.Property import Property

#=============================================================================

class Field(object):

#=============================================================================

    """
    gestion des données et des types.

        comportement des attibuts

            - valeur par défault
            - type
            - fonctions attachées pour
                - accès
                - modification
                - suppression
    """
    CLASS=None
    Attribute=Property
    #--------------------------------------------------------------------------

    def __init__(self,doc=None,cls=None,default=None,
                set_f=None,get_f=None,del_f=None,
                record=True,
                create=False,eval_expr=False):
        """

        """

        if cls is not None:
            self.cls = cls

        elif self.CLASS is not None:
            self.cls=self.CLASS

        else:
            self.cls=None

        self.name=None

        self.set_f=set_f
        self.get_f=get_f
        self.del_f=del_f

        self.create=create
        self.record=record
        self.eval_expr=eval_expr

       
        self.default=default
        self.doc=doc
    #--------------------------------------------------------------------------

    def get_value(self,value):

        """
        récuperer la bonne valeur pour l'attibut.

        plusieurs cas :

            valeur nulle
                et valeur par défaut -> default
                et cls ->  cls()
                sinon -> None

            si valeur
                et cls ->  valeur
                sinon eval 
                    retest cls-> valeur
        """
        #print("modify ",value,self.cls,self.default)

        if value is None and self.default is not None:
            return self.default

        elif self.create == True:
            if value is not None:
                return self.cls(value)
            else:
                return self.cls()

        elif value is None:
            return None

        elif type(value) in [str] and self.eval_expr == True:
            return eval(value)

        elif self.cls and type(self.cls)==list:
            for c in self.field.cls:
                if isinstance(value, c)==True:
                    return value

        elif self.cls and isinstance(value, self.cls):
            return value


        elif self.cls:
            if self.cls==bool:
                return bool(eval(value))
            return self.cls(value)
            #except:
            #    print("ERROR converion ",self.cls,value)

        else:
            return value



    #--------------------------------------------------------------------------

    def new_attribute(self,node):

        """
        crée un nouvel attribut pour le noeud node avec la valeur transmise
        au constructeur. Cette valeur peut être nulle
        """

        #la première fois le dictionnaire stocke l'objet
        elt= self.Attribute(node,self)
        elt.init()
        node[self.name] =elt
        #ensuite, il met en place les callback


    #--------------------------------------------------------------------------

    def __help__(self,funct=None):


        if self.cls is None:
            t=str(self.__class__.__name__)
        else:
            t=str(self.cls.__name__)


        if funct:
            return dict(cls=t,default=self.default,doc=self.doc)
        else:
            return dict(record=self.record,cls=t,default=self.default,doc=self.doc)

    #--------------------------------------------------------------------------

#=============================================================================

