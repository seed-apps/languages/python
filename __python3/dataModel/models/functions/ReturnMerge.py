from .Decorator import Decorator
from ..Frame import Frame
from dataModel.Store import Store
import types
import inspect,sys,traceback
import console
#=============================================================================

class ReturnMerge(Decorator):

#=============================================================================
    """
    scanne les listes et itérateurs pour ne renvoyer qu'une liste unique
    supprime les None
    renvoie une liste
    """

    #-----------------------------------------------------------------

    def onCall(self,function,args,kwargs,Class=Frame,**options):
        node=Class()

        try:
            self.returnResult(node,function(*args, **kwargs))
            return node
        except:

            exc_type, exc_value, exc_traceback = sys.exc_info()
            tb=traceback.extract_tb(exc_traceback )
            node=dict(path=self.node.path(),exc_type=exc_type, exc_value=exc_value)

            
            if tb is not None:
                lst=[]
                for line in tb:
                    elt=dict(filename=line[0],line=line[1],function_name=line[2],text=line[3])
                    lst.append(elt)
                node["details"]=lst

            console.title("ERROR")
            console.add(node)
            console.show_data(node)
            return node


    #-----------------------------------------------------------------
    def returnResult(self,node,result):


        if type(result) is list or isinstance(result,Store) or isinstance(result, types.GeneratorType):
            for elt in result:
                self.returnResult(node,elt)

        elif result is None:
            pass

        else:
            node.append(result)

    #-----------------------------------------------------------------

#=============================================================================




