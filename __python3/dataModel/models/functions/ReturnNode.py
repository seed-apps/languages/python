from .Decorator import Decorator

#=============================================================================

class ReturnNode(Decorator):

#=============================================================================


    def onCall(self,function,args,kwargs,Class=None,attr=None,**options):

        node=Class(**function(*args,**kwargs))
        return node

#=============================================================================
